#!/bin/bash

testdir=testResults
rm -r $testdir
mkdir $testdir

folders="condensation doc femath inputlib outputlib modal framefea static transform"

for i in $folders ; do
    echo
    echo $i
    echo ------------------------------------------------------------
    echo
    cd $i
    coveragefile=../$testdir/${i}_coverage
    go test -covermode=count -coverprofile=${coveragefile} 2>/dev/null
    go tool cover -func=${coveragefile}
    if (( `wc -l ${coveragefile} | awk '{print $1;}'` > 1 )); then  
        go tool cover -html=${coveragefile} -o ${coveragefile}.html
        # xdg-open ${coveragefile}.html
    fi
    go vet
    golint
    cd .. 
done


