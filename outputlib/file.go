/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package outputlib

import "bitbucket.org/srosencrantz/framefea/inputlib"

// FileSuffix specifies the filename suffix to be used for files of this type
const FileSuffix = ".ffout"

// Output describes shotlines to examine
type Output struct {
	Filename           string
	Docs               []*Doc                        `csvrecRec:"Doc"`
	StaticSummaries    []*StaticSummary              `csvrecRec:"StaticSummary"`
	NodeLoads          []*NodeLoad                   `csvrecRec:"NodeLoad"`
	NodalDisplacements []*inputlib.NodalDisplacement `csvrecRec:"NodalDisplacement"` /*Ndl*/
	NodalVelocities    []*inputlib.NodalVelocity     `csvrecRec:"NodalVelocity"`
	NodalAccelerations []*inputlib.NodalAcceleration `csvrecRec:"NodalAcceleration"`
	ReactionVecs       []*ReactionVec                `csvrecRec:"ReactionVec"`
	EndForces          []*EndForce                   `csvrecRec:"EndForce"`
	ModalSummaries     []*ModalSummary               `csvrecRec:"ModalSummary"`
	NaturalFreqs       []*NaturalFreq                `csvrecRec:"NaturalFreq"`
	ModeShapes         []*ModeShape                  `csvrecRec:"ModeShape"`
	MassMats           []*MassMat                    `csvrecRec:"Mass"`
	DynStiffMats       []*DynStiffMat                `csvrecRec:"DynStiff"`
	DynMassMats        []*DynMassMat                 `csvrecRec:"DynMass"`
	CondStiffMats      []*CondStiffMat               `csvrecRec:"condStiff"`
	CondMassMats       []*CondMassMat                `csvrecRec:"condMass"`
}

//Doc describes a Documentation record.
type Doc struct {
	Text string `csvrecField:"cstr"` // Doc name
}

// StaticSummary combines eqerr, rmsresid, and ok into a struct for single line printing in the output file.
// These are quality measures from the static analysis.
type StaticSummary struct {
	EqErr    float64 `csvrecField:"flt"`
	RmsResID float64 `csvrecField:"flt"`
	Ok       int     `csvrecField:"int"`
}

// NodeLoad are the nodal loads for each degree of freedom
type NodeLoad struct {
	NID int     `csvrecField:"int"`
	Fx  float64 `csvrecField:"flt"`
	Fy  float64 `csvrecField:"flt"`
	Fz  float64 `csvrecField:"flt"`
	Mxx float64 `csvrecField:"flt"`
	Myy float64 `csvrecField:"flt"`
	Mzz float64 `csvrecField:"flt"`
}

// NodeLoadList is a list of NodeLoad pointers
type NodeLoadList []*NodeLoad

func (a NodeLoadList) Len() int           { return len(a) }
func (a NodeLoadList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NodeLoadList) Less(i, j int) bool { return a[i].NID < a[j].NID }

// NodeDispList is a list of NodeDisp pointers
type NodeDispList []*inputlib.NodalDisplacement

func (a NodeDispList) Len() int           { return len(a) }
func (a NodeDispList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NodeDispList) Less(i, j int) bool { return a[i].Nid < a[j].Nid }

// NodeVelList is a list of NodeVel pointers
type NodeVelList []*inputlib.NodalVelocity

func (a NodeVelList) Len() int           { return len(a) }
func (a NodeVelList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NodeVelList) Less(i, j int) bool { return a[i].Nid < a[j].Nid }

// NodeAccelList is a list of NodeAccel pointers
type NodeAccelList []*inputlib.NodalAcceleration

func (a NodeAccelList) Len() int           { return len(a) }
func (a NodeAccelList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NodeAccelList) Less(i, j int) bool { return a[i].Nid < a[j].Nid }

// ReactionVec is the reaction vector with reactions for each reacted degree of freedom
type ReactionVec struct {
	NID int     `csvrecField:"int"`
	Fx  float64 `csvrecField:"flt"`
	Fy  float64 `csvrecField:"flt"`
	Fz  float64 `csvrecField:"flt"`
	Mxx float64 `csvrecField:"flt"`
	Myy float64 `csvrecField:"flt"`
	Mzz float64 `csvrecField:"flt"`
}

// ReactionVecList is a list of ReactionVec pointers
type ReactionVecList []*ReactionVec

func (a ReactionVecList) Len() int           { return len(a) }
func (a ReactionVecList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ReactionVecList) Less(i, j int) bool { return a[i].NID < a[j].NID }

// EndForce describes the element end forces in local coordinates
type EndForce struct {
	EID int     `csvrecField:"int"`
	NID int     `csvrecField:"int"`
	Nx  float64 `csvrecField:"flt"`
	Vy  float64 `csvrecField:"flt"`
	Vz  float64 `csvrecField:"flt"`
	Txx float64 `csvrecField:"flt"`
	Myy float64 `csvrecField:"flt"`
	Mzz float64 `csvrecField:"flt"`
}

// EndForceList is a list of EndForce pointers
type EndForceList []*EndForce

func (a EndForceList) Len() int      { return len(a) }
func (a EndForceList) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a EndForceList) Less(i, j int) bool {
	if a[i].EID == a[j].EID {
		return a[i].NID < a[j].NID
	}
	return a[i].EID < a[j].EID
}

// ModalSummary describes the modal analysis
type ModalSummary struct {
	Iter int `csvrecField:"int"`
}

// NaturalFreq is the displacement vector with displacements for each degree of freedom
type NaturalFreq struct {
	ModeNum          int     `csvrecField:"int"`
	NaturalFrequency float64 `csvrecField:"flt"`
}

// ModeShape describes the natural frequency and shape information for a single mode shape.
type ModeShape struct {
	ModeNum          int       `csvrecField:"int"`
	NaturalFrequency float64   `csvrecField:"flt"`
	ModeShape        []float64 `csvrecField:"fltlist"`
}

// StiffMat describes the mass matrix
type StiffMat struct {
	StiffMatrix [][]float64 `csvrecField:"fmatrix"`
}

// MassMat describes the mass matrix
type MassMat struct {
	MassMatrix [][]float64 `csvrecField:"fmatrix"`
}

// DynStiffMat describes the dynamic stiffness matrix
type DynStiffMat struct {
	DynStiffMatrix [][]float64 `csvrecField:"fmatrix"`
}

// DynMassMat describes the dynamic mass matrix
type DynMassMat struct {
	DynMassMatrix [][]float64 `csvrecField:"fmatrix"`
}

// CondStiffMat describes the condensed stiffness matrix
type CondStiffMat struct {
	CondStiffMatrix [][]float64 `csvrecField:"fmatrix"`
}

// CondMassMat describes the condensed mass matrix
type CondMassMat struct {
	CondMassMatrix [][]float64 `csvrecField:"fmatrix"`
}
