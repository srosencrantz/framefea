/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package outputlib

import (
	"sort"

	"bitbucket.org/srosencrantz/framefea/inputlib"
)

// AddStatic simplifies adding static results to the output structure
func (output *Output) AddStatic(F, D, R []float64, Q [][]float64,
	eqerr, rmsresid float64, ok int, fd inputlib.FrameData) {

	loadList := make([]*NodeLoad, 0)
	dispList := make([]*inputlib.NodalDisplacement, 0)
	reacList := make([]*ReactionVec, 0)

	for nID, dof := range fd.DofMap {
		load := &NodeLoad{
			NID: nID,
			Fx:  (F)[dof],
			Fy:  (F)[dof+1],
			Fz:  (F)[dof+2],
			Mxx: (F)[dof+3],
			Myy: (F)[dof+4],
			Mzz: (F)[dof+5]}
		loadList = append(loadList, load)

		disp := &inputlib.NodalDisplacement{
			Nid: nID,
			Dx:  (D)[dof],
			Dy:  (D)[dof+1],
			Dz:  (D)[dof+2],
			Dxx: (D)[dof+3],
			Dyy: (D)[dof+4],
			Dzz: (D)[dof+5]}
		dispList = append(dispList, disp)

		reac := &ReactionVec{
			NID: nID,
			Fx:  (R)[dof],
			Fy:  (R)[dof+1],
			Fz:  (R)[dof+2],
			Mxx: (R)[dof+3],
			Myy: (R)[dof+4],
			Mzz: (R)[dof+5]}
		reacList = append(reacList, reac)
	}

	forcList := make([]*EndForce, 0)
	for ellabel, ordernum := range fd.ElemMap {
		force1 := &EndForce{
			EID: ellabel,
			NID: fd.INodeMap[fd.N1[ordernum]],
			Nx:  (Q)[ordernum][0],
			Vy:  (Q)[ordernum][1],
			Vz:  (Q)[ordernum][2],
			Txx: (Q)[ordernum][3],
			Myy: (Q)[ordernum][4],
			Mzz: (Q)[ordernum][5]}

		force2 := &EndForce{
			EID: ellabel,
			NID: fd.INodeMap[fd.N2[ordernum]],
			Nx:  (Q)[ordernum][6],
			Vy:  (Q)[ordernum][7],
			Vz:  (Q)[ordernum][8],
			Txx: (Q)[ordernum][9],
			Myy: (Q)[ordernum][10],
			Mzz: (Q)[ordernum][11]}
		forcList = append(forcList, force1, force2)
	}

	sort.Sort(NodeLoadList(loadList))
	sort.Sort(NodeDispList(dispList))
	for i, disp := range dispList {
		disp.ID = i + 1
	}
	sort.Sort(ReactionVecList(reacList))
	sort.Sort(EndForceList(forcList))
	output.NodeLoads = loadList
	output.NodalDisplacements = dispList
	output.ReactionVecs = reacList
	output.EndForces = forcList
	output.StaticSummaries = []*StaticSummary{&StaticSummary{EqErr: eqerr, RmsResID: rmsresid, Ok: ok}}
}

// AddModal simplifies adding modal results to the output structure
func (output *Output) AddModal(Kd, Md, M, V [][]float64, f []float64, iter int) {

	modeList := make([]*ModeShape, 0)
	nfreqList := make([]*NaturalFreq, 0)

	for mode := 0; mode < len(V[0]); mode++ {
		modeShape := &ModeShape{
			ModeNum:          mode + 1,
			NaturalFrequency: f[mode],
			ModeShape:        V[mode]}
		modeList = append(modeList, modeShape)

		nfreq := &NaturalFreq{
			ModeNum:          mode + 1,
			NaturalFrequency: f[mode]}
		nfreqList = append(nfreqList, nfreq)
	}

	output.DynStiffMats = []*DynStiffMat{&DynStiffMat{DynStiffMatrix: M}}
	output.DynMassMats = []*DynMassMat{&DynMassMat{DynMassMatrix: M}}
	output.MassMats = []*MassMat{&MassMat{MassMatrix: M}}
	output.ModeShapes = modeList
	output.NaturalFreqs = nfreqList
	output.ModalSummaries = []*ModalSummary{&ModalSummary{Iter: iter}}
}

// AddCondensation simplifies adding condensation results to the output structure
func (output *Output) AddCondensation(Kc, Mc [][]float64) {
	output.CondStiffMats = []*CondStiffMat{&CondStiffMat{CondStiffMatrix: Kc}}
	output.CondMassMats = []*CondMassMat{&CondMassMat{CondMassMatrix: Mc}}
}

// AddNewmarkBeta simplifies adding newmark-beta time integration results to the output structure
func (output *Output) AddNewmarkBeta(Q1 [][]float64, Fhat, U1, V1, A1, R1 []float64, eqErr, rmsResid float64, ok int, fd *inputlib.FrameData) {
	output.StaticSummaries = []*StaticSummary{&StaticSummary{EqErr: eqErr, RmsResID: rmsResid, Ok: ok}}

	output.NodeLoads = make([]*NodeLoad, 0)
	output.NodalDisplacements = make([]*inputlib.NodalDisplacement, 0)
	output.NodalVelocities = make([]*inputlib.NodalVelocity, 0)
	output.NodalAccelerations = make([]*inputlib.NodalAcceleration, 0)
	output.ReactionVecs = make([]*ReactionVec, 0)

	for nID, dof := range fd.DofMap {
		load := &NodeLoad{NID: nID, Fx: (Fhat)[dof], Fy: (Fhat)[dof+1], Fz: (Fhat)[dof+2],
			Mxx: (Fhat)[dof+3], Myy: (Fhat)[dof+4], Mzz: (Fhat)[dof+5]}
		output.NodeLoads = append(output.NodeLoads, load)

		disp := &inputlib.NodalDisplacement{Nid: nID, Dx: (U1)[dof], Dy: (U1)[dof+1], Dz: (U1)[dof+2],
			Dxx: (U1)[dof+3], Dyy: (U1)[dof+4], Dzz: (U1)[dof+5]}
		output.NodalDisplacements = append(output.NodalDisplacements, disp)

		vel := &inputlib.NodalVelocity{Nid: nID, Vx: (V1)[dof], Vy: (V1)[dof+1], Vz: (V1)[dof+2],
			Vxx: (V1)[dof+3], Vyy: (V1)[dof+4], Vzz: (V1)[dof+5]}
		output.NodalVelocities = append(output.NodalVelocities, vel)

		acc := &inputlib.NodalAcceleration{Nid: nID, Ax: (A1)[dof], Ay: (A1)[dof+1], Az: (A1)[dof+2],
			Axx: (A1)[dof+3], Ayy: (A1)[dof+4], Azz: (A1)[dof+5]}
		output.NodalAccelerations = append(output.NodalAccelerations, acc)

		reac := &ReactionVec{NID: nID, Fx: (R1)[dof], Fy: (R1)[dof+1], Fz: (R1)[dof+2],
			Mxx: (R1)[dof+3], Myy: (R1)[dof+4], Mzz: (R1)[dof+5]}
		output.ReactionVecs = append(output.ReactionVecs, reac)
	}

	sort.Sort(NodeLoadList(output.NodeLoads))
	sort.Sort(NodeDispList(output.NodalDisplacements))
	sort.Sort(NodeVelList(output.NodalVelocities))
	sort.Sort(NodeAccelList(output.NodalAccelerations))
	for i := 0; i < len(output.NodalDisplacements); i++ {
		output.NodalDisplacements[i].ID = i + 1
		output.NodalVelocities[i].ID = i + 1
		output.NodalAccelerations[i].ID = i + 1
	}
	sort.Sort(ReactionVecList(output.ReactionVecs))

	output.EndForces = make([]*EndForce, 0)
	for label, order := range fd.ElemMap {
		force1 := &EndForce{EID: label, NID: fd.INodeMap[fd.N1[order]],
			Nx: (Q1)[order][0], Vy: (Q1)[order][1], Vz: (Q1)[order][2],
			Txx: (Q1)[order][3], Myy: (Q1)[order][4], Mzz: (Q1)[order][5]}

		force2 := &EndForce{EID: label, NID: fd.INodeMap[fd.N2[order]],
			Nx: (Q1)[order][6], Vy: (Q1)[order][7], Vz: (Q1)[order][8],
			Txx: (Q1)[order][9], Myy: (Q1)[order][10], Mzz: (Q1)[order][11]}
		output.EndForces = append(output.EndForces, force1, force2)
	}

	sort.Sort(EndForceList(output.EndForces))

}
