/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package outputlib

import (
	"fmt"
	"time"

	"bitbucket.org/srosencrantz/csvrec"
	"bitbucket.org/srosencrantz/framefea/doc"
)

// ReadFile reads an framefea output file into an Output Struct.
func ReadFile(fileName string) (outDataPtr *Output, err error) {
	outData := csvrec.ParseFile(fileName, Output{}, &err).(Output)
	if err != nil {
		return &outData, err
	}
	outData.Filename = fileName

	return &outData, err
}

// WriteFile writes a Description struct to a file.
func (outData *Output) WriteFile(fileName, toolName string, verbose int) error {
	if outData == nil {
		return nil
	}

	outData.Docs = append(outData.Docs,
		&Doc{Text: fmt.Sprintf("Written by: %s on %s from %s",
			toolName, time.Now().Format("2 Jan 2006, 15:04:05 -MST"), doc.GetVersion())})

	switch verbose {
	case 1:
		return csvrec.CreateFile(fileName, FileSuffix, *outData)
	default:
		newOut := &Output{
			Docs:               outData.Docs,
			NodeLoads:          outData.NodeLoads,
			NodalDisplacements: outData.NodalDisplacements,
			NodalVelocities:    outData.NodalVelocities,
			NodalAccelerations: outData.NodalAccelerations,
			ReactionVecs:       outData.ReactionVecs,
			EndForces:          outData.EndForces,
			StaticSummaries:    outData.StaticSummaries,
			ModeShapes:         outData.ModeShapes,
			NaturalFreqs:       outData.NaturalFreqs,
			ModalSummaries:     outData.ModalSummaries,
			CondStiffMats:      outData.CondStiffMats,
			CondMassMats:       outData.CondMassMats}

		return csvrec.CreateFile(fileName, FileSuffix, *newOut)
	}
}
