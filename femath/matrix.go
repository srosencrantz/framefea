/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package femath

import (
	"bitbucket.org/srosencrantz/errcat"
	"fmt"
	"math"
)

// Float3d creates a 3d array of floats with various lengths for each dimension
func Float3d(a, b, c int) [][][]float64 {
	output := make([][][]float64, a)
	for i := range output {
		output[i] = make([][]float64, b)
		for j := range output[i] {
			output[i][j] = make([]float64, c)
		}
	}
	return output
}

// Float2d creates a 2d array of floats with various lengths for each dimension
func Float2d(a, b int) [][]float64 {
	output := make([][]float64, a)
	for i := range output {
		output[i] = make([]float64, b)
	}
	return output
}

// Int2d creates a 2d array of ints with various lengths for each dimension
func Int2d(a, b int) [][]int {
	output := make([][]int, a)
	for i := range output {
		output[i] = make([]int, b)
	}
	return output
}

// LuDcmp solves [A]{x} = {b} simply and efficiently by performing an
// LU - decomposition of [A].  No pivoting is performed.
// [a] is a diagonally dominant matrix of dimension [1..n][1..n].
// {b} is a r.h.s. vector of dimension [1..n].
// {b} is updated using [LU] and then back-substitution is done to obtain {x}.
// {b} is replaced by {x} and [A] is replaced by the LU - reduction of itself.
//
//  usage:  double **A, *b;
//  int   n, reduce, solve, pd;
//  luDcmp ( A, n, b, reduce, solve, &pd );     5may98
//
// a, the system matrix, and it's LU- reduction
// b, the right hand side vector, and the solution vector
// reduce, true: do a forward reduction; false: don't do the reduction
// solve, true: do a back substitution for {x};  false: do no bk-sub'n
func LuDcmp(a [][]float64, b []float64, reduce, solve bool) (err error) {
	n := len(a)
	if reduce { /* forward reduction of [A]*/
		for k := 0; k < n; k++ {
			pivot := a[k][k]
			if 0.0 == pivot {
				return errcat.AppendErrStr(err, fmt.Sprintf("LuDcmp, zero found on the diagonal A[%d][%d] = %11.4e", k, k, a[k][k]))
			}
			for i := k + 1; i < n; i++ {
				a[i][k] /= pivot
				for j := k + 1; j < n; j++ {
					a[i][j] -= a[i][k] * a[k][j]
				}
			}
		}
	} /* the forward reduction of [A] is now complete*/

	if solve { /* back substitution to solve for {x}*/
		/* {b} is run through the same forward reduction as was [A]*/
		for k := 0; k < n; k++ {
			for i := k + 1; i < n; i++ {
				b[i] -= a[i][k] * b[k]
			}
		}

		/* now back substitution is conducted on {b};  [A] is preserved */
		for j := n - 1; j > 0; j-- {
			for i := 0; i < j; i++ {
				b[i] -= b[j] * a[i][j] / a[j][j]
			}
		}

		/* finally we solve for the {x} vector*/
		for i := 0; i < n; i++ {
			b[i] /= a[i][i]
		}
	}
	// /* TAA DAAAAAAAA! {b} is now {x} and is ready to be returned*/
	return nil
}

// LdlDcmp solves [A]{x} = {b} simply and efficiently by performing an
// L D L' - decomposition of [A].  No pivoting is performed.
// [A] is a symmetric diagonally-dominant matrix of dimension [1..n][1..n].
// {b} is a r.h.s. vector of dimension [1..n].
// {b} is updated using L D L' and then back-substitution is done to obtain {x}
// {b} is returned unchanged.  ldlDcmp(A,n,d,x,x,1,1,&pd) is valid.
//     The lower triangle of [A] is replaced by the lower triangle L of the
//     L D L' reduction.  The diagonal of D is returned in the vector {d}
//
// usage: double **A, *d, *b, *x;
// int   n, reduce, solve, pd;
// ldlDcmp ( A, n, d, b, x, reduce, solve, &pd );
//
// H.P. Gavin, Civil Engineering, Duke University, hpgavin@duke.edu  9 Oct 2001
// Bathe, Finite Element Procecures in Engineering Analysis, Prentice Hall, 1982
// 	a:  the system matrix, and L of the L D L' decomp.
// 	b: the right hand side vector
// 	x:  the solution vector
// 	reduce: = true, do a forward reduction of A; = false don't
// 	solve: = true, do a back substitution for {x}; = false: don't
// n size of a
// 	err:  = nil, definite matrix and successful L D L' decomp'n
// 	d:  diagonal of D in the  L D L' - decomp'n
func LdlDcmp(a [][]float64, n int, d, b, x []float64, reduce, solve bool) (pd int, err error) {
	var i, j, k, m int
	pd = 0

	if reduce { /* forward column-wise reduction of [A]	*/
		for j = 0; j < n; j++ {
			m = 0
			for i = 0; i < j; i++ { /* scan the sky-line	*/
				if a[i][j] == 0.0 {
					m++
				} else {
					break
				}
			}
			for i = m; i < j; i++ {
				a[j][i] = a[i][j]
				for k = m; k < i; k++ {
					a[j][i] -= a[j][k] * a[i][k]
				}
			}

			d[j] = a[j][j]
			for i = m; i < j; i++ {
				d[j] -= a[j][i] * a[j][i] / d[i]
			}
			for i = m; i < j; i++ {
				a[j][i] /= d[i]
			}
			if d[j] == 0.0 {
				return pd, errcat.AppendErrStr(err, fmt.Sprintf("LdlDcmp, zero found on diagonal d[%d] = %11.4e", j, d[j]))
			}
			if d[j] < 0.0 {
				pd--
			}

		}
	} // the forward reduction of [A] is now complete

	if solve { // back substitution to solve for {x}
		//{x} is run through the same forward reduction as was [A]
		for i = 0; i < n; i++ {
			x[i] = b[i]
			for j = 0; j < i; j++ {
				x[i] -= a[i][j] * x[j]
			}
		}

		for i = 0; i < n; i++ {
			x[i] /= d[i]
		}
		// now back substitution is conducted on {x};  [A] is preserved
		for i = n - 1; i > 0; i-- {
			for j = 0; j < i; j++ {
				x[j] -= a[i][j] * x[i]
			}
		}
	}
	return pd, nil
}

// LdlMprove improves a solution vector x[1..n] of the linear set of equations
// [A]{x} = {b}.  The matrix A[1..n][1..n], and the vectors b[1..n] and x[1..n]
// are input, as is the dimension n.   The matrix [A] is the L D L'
// decomposition of the original system matrix, as returned by ldlDcmp().
// Also input is the diagonal vector, {d} of [D] of the L D L' decompositon.
// On output, only {x} is modified to an improved set of values.
// usage: double **A, *d, *b, *x, rmsResid;
// int   n, ok;
func LdlMprove(a [][]float64, n int, d, b, x []float64, rmsResID *float64) (ok bool, finalErr error) {
	// unless you have already run LdlMprove (in which case the rmsResID should resent) rmsResID should be 1.0 (I think)
	var sdp float64             // accumulate the r.h.s. in double precision
	var rmsResIDNew float64     // the RMS error of the mprvd solution
	resid := make([]float64, n) // the residual error
	var j, i int

	for i = 0; i < n; i++ { // calculate the r.h.s. of
		sdp = b[i]              // [A]{r} = {b} - [A]{x+r}
		for j = 0; j < n; j++ { // A in upper triangle only
			if i <= j {
				sdp -= a[i][j] * x[j]
			} else {
				sdp -= a[j][i] * x[j]
			}
		}
		resid[i] = sdp
	}

	// solve for the error term
	_, err := LdlDcmp(a, n, d, resid, resid, false, true)
	if err != nil {
		return ok, errcat.AppendErr(finalErr, err)
	}

	for i = 0; i < n; i++ {
		rmsResIDNew += resid[i] * resid[i]
	}

	rmsResIDNew = math.Sqrt(rmsResIDNew / float64(n))

	ok = false
	if rmsResIDNew / *rmsResID < 0.90 { // good improvement
		// 	subtract the error from the old solution
		for i = 0; i < n; i++ {
			x[i] += resid[i]
		}
		*rmsResID = rmsResIDNew
		ok = true // the solution has improved
	}
	return ok, nil
}

// LdlDcmpPm solves partitioned matrix equations
//
//           [A_qq]{x_q} + [A_qr]{xR} = {b_q}
//           [ARq]{x_q} + [ARr]{xR} = {bR}+{cR}
//           where {b_q}, {bR}, and {xR} are known and
//           where {x_q} and {cR} are unknown
//
// via L D L' - decomposition of [A_qq].  No pivoting is performed.
// [A] is a symmetric diagonally-dominant matrix of dimension [1..n][1..n].
// {b} is a r.h.s. vector of dimension [1..n].
// {b} is updated using L D L' and then back-substitution is done to obtain {x}
// {b_q} and {bR}  are returned unchanged.
// {cR} is returned as a vector of [1..n] with {c_q}=0.
// {q} is a vector of the indexes of known values {b_q}
// {r} is a vector of the indexes of known values {xR}
//     The lower triangle of [A_qq] is replaced by the lower triangle L of its
//     L D L' reduction.  The diagonal of D is returned in the vector {d}
//
// usage: double **A, *d, *b, *x;
//      int   n, reduce, solve, pd;
//      ldlDcmp_pm ( A, n, d, b, x, c, q, r, reduce, solve, &pd );
//
// H.P. Gavin, Civil Engineering, Duke University, hpgavin@duke.edu
// Bathe, Finite Element Procecures in Engineering Analysis, Prentice Hall, 1982
// 2014-05-14
//
// double **A,     /**< the system matrix, and L of the L D L' decomp.*/
// int n,          /**< the dimension of the matrix                */
// double *d,      /**< diagonal of D in the  L D L' - decomp'n    */
// double *b,      /**< the right hand side vector                 */
// double *x,      /**< part of the solution vector                */
// double *c,      /**< the part of the solution vector in the rhs */
// int *q,         /**< q[j]=1 if  b[j] is known; q[j]=0 otherwise */
// int *r,         /**< r[j]=1 if  x[j] is known; r[j]=0 otherwise */
// int reduce,     /**< 1: do a forward reduction of A; 0: don't   */
// int solve,      /**< 1: do a back substitution for {x}; 0: don't */
// int *pd         /**< 1: definite matrix and successful L D L' decomp'n*/
func LdlDcmpPm(a [][]float64, n int, d, b, x, c []float64, q, r []bool, reduce, solve bool, pd *int) (err error) {
	var i, j, k, m int
	*pd = 0 /* number of negative elements on the diagonal of D */

	if reduce { /* forward column-wise reduction of [A]	*/
		for j = 0; j < n; j++ {
			d[j] = 0.0
			if q[j] { /* reduce column j, except where q[i]==0	*/
				for m, i = 0, 0; i < j; i++ { /* scan the sky-line	*/
					if a[i][j] == 0.0 {
						m++
					} else {
						break
					}
				}

				for i = m; i < j; i++ {
					if q[i] {
						a[j][i] = a[i][j]
						for k = m; k < i; k++ {
							if q[k] {
								a[j][i] -= a[j][k] * a[i][k]
							}
						}
					}
				}

				d[j] = a[j][j]
				for i = m; i < j; i++ {
					if q[i] {
						d[j] -= a[j][i] * a[j][i] / d[i]
					}
				}
				for i = m; i < j; i++ {
					if q[i] {
						a[j][i] /= d[i]
					}
				}
				if d[j] == 0.0 {
					return errcat.AppendErrStr(err, fmt.Sprintf("LdlDcmpPm, zero found on diagonal d[%d] = %11.4e", j, d[j]))
				}
				if d[j] < 0.0 {
					(*pd)--
				}
			}
		}
	} /* the forward reduction of [a] is now complete	*/

	if solve { /* back substitution to solve for {x}   */
		for i = 0; i < n; i++ {
			if q[i] {
				x[i] = b[i]
				for j = 0; j < n; j++ {
					if r[j] {
						x[i] -= a[i][j] * x[j]
					}
				}
			}
		}

		/* {x} is run through the same forward reduction as was [a] */
		for i = 0; i < n; i++ {
			if q[i] {
				for j = 0; j < i; j++ {
					if q[j] {
						x[i] -= a[i][j] * x[j]
					}
				}
			}
		}

		for i = 0; i < n; i++ {
			if q[i] {
				x[i] /= d[i]
			}
		}

		/* now back substitution is conducted on {x};  [a] is preserved */
		for i = n - 1; i > 0; i-- {
			if q[i] {
				for j = 0; j < i; j++ {
					if q[j] {
						x[j] -= a[i][j] * x[i]
					}
				}
			}
		}

		/* finally, evaluate cR	*/
		for i = 0; i < n; i++ {
			c[i] = 0.0
			if r[i] {
				c[i] = -b[i] // changed from 0.0 to -b[i]; 2014-05-14
				for j = 0; j < n; j++ {
					c[i] += a[i][j] * x[j]
				}
			}
		}

	}
	return nil
}

// LdlMprovePm improves a solution vector x[1..n] of the partitioned set of linear equations
//           [A_qq]{x_q} + [A_qr]{xR} = {b_q}
//           [ARq]{x_q} + [ARr]{xR} = {bR}+{cR}
//           where {b_q}, {bR}, and {xR} are known and
//           where {x_q} and {cR} are unknown
// by reducing the residual r_q
//           A_qq r_q = {b_q} - [A_qq]{x_q+r_q} + [A_qr]{xR}
// The matrix A[1..n][1..n], and the vectors b[1..n] and x[1..n]
// are input, as is the dimension n.   The matrix [A] is the L D L'
// decomposition of the original system matrix, as returned by ldlDcmp_pm().
// Also input is the diagonal vector, {d} of [D] of the L D L' decompositon.
// On output, only {x} is modified to an improved set of values.
// The calculations in ldlMprove_pm do not involve bR.
//
// usage: double **A, *d, *b, *x, rmsResid;
//      int   n, ok, *q, *r;
//      ldlMprove_pm ( A, n, d, b, x, q, r, &rmsResid, &ok );
//
// double **A,     /**< the system matrix, and L of the L D L' decomp.*/
// int n,          /**< the dimension of the matrix                */
// double *d,      /**< diagonal of D in the  L D L' - decomp'n    */
// double *b,      /**< the right hand side vector                 */
// double *x,      /**< part of the solution vector		*/
// double *c,      /**< the part of the solution vector in the rhs */
// int *q,         /**< q[j]=1 if  b[j] is known; q[j]=0 otherwise */
// int *r,         /**< r[j]=1 if  x[j] is known; r[j]=0 otherwise */
// double *rmsResid, /**< root-mean-square of residual error      */
// int *ok );      /**< 1: >10% reduction in rmsResid; 0: not     */
func LdlMprovePm(a [][]float64, n int, d, b, x, c []float64, q, r []bool, rmsResID *float64) (ok bool, finalErr error) {
	var sdp float64             // accumulate the r.h.s. in double precision
	dx := make([]float64, n)    // the residual error
	dc := make([]float64, n)    // update to partial r.h.s. vector, c
	rmsResIDNew := float64(0.0) // the RMS error of the mprvd solution
	var j, i, pd int

	// calculate the r.h.s. of ...
	//  [a_qq]{dx_q} = {b_q} - [a_qq]*{x_q} - [a_qr]*{xR}
	//  {dxR} is left unchanged at 0.0;
	for i = 0; i < n; i++ {
		if q[i] {
			sdp = b[i]
			for j = 0; j < n; j++ {
				if q[j] { // a_qq in upper triangle only
					if i <= j {
						sdp -= a[i][j] * x[j]
					} else {
						sdp -= a[j][i] * x[j]
					}
				}
			}
			for j = 0; j < n; j++ {
				if r[j] {
					sdp -= a[i][j] * x[j]
				}
			}
			dx[i] = sdp
		} // else dx[i] = 0.0; // x[i];
	}

	// solve for the residual error term, a is already factored
	err := LdlDcmpPm(a, n, d, dx, dx, dc, q, r, false, true, &pd)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return false, finalErr
		}
	}

	for i = 0; i < n; i++ {
		if q[i] {
			rmsResIDNew += dx[i] * dx[i]
		}
	}

	rmsResIDNew = math.Sqrt(rmsResIDNew / float64(n))
	ok = false
	if rmsResIDNew / *rmsResID < 0.90 { /*  enough improvement    */
		for i = 0; i < n; i++ { /*  update the solution 2014-05-14   */
			if q[i] {
				x[i] += dx[i]
			}
			if r[i] {
				c[i] += dc[i]
			}
		}
		*rmsResID = rmsResIDNew /* return the new residual   */
		ok = true               /* the solution has improved */
	}
	return ok, nil
}

// PseudoInv calculates the pseudo-inverse of A.
//  Ai = inv ( A'*A + beta * trace(A'*A) * I ) * A'
//  beta is a regularization factor, which should be small (1e-10)
//  A is m by n      Ai is m by n
// 	double **A,	/**< an n-by-m matrix				*/
// 	double **Ai,	/**< the pseudo-inverse of A			*/
// 	int n, int m,	/**< matrix dimensions				*/
// 	double beta,	/**< regularization factor			*/
func PseudoInv(A, Ai [][]float64, n int, m int, beta float64) (finalErr error) {

	diag := make([]float64, n)
	b := make([]float64, n)
	x := make([]float64, n)
	AtA := Float2d(n, n)
	AtAi := Float2d(n, n)
	var err error

	var tmp, trAtA float64

	if beta > 1.0 {
		finalErr = errcat.AppendWarnStr(finalErr, fmt.Sprintf("PseudoInv, beta = %10.6f", beta))
	}

	for i := 0; i < n; i++ { /* compute A' * A */
		for j := 0; j < n; j++ {
			tmp = 0.0
			for k := 0; k < m; k++ {
				tmp += A[k][i] * A[k][j]
			}
			AtA[i][j] = tmp
		}
	}
	for i := 0; i < n; i++ { /* make symmetric */
		for j := i; j < n; j++ {
			AtA[j][i] = 0.5 * (AtA[i][j] + AtA[j][i])
			AtA[i][j] = AtA[j][i]
		}
	}
	trAtA = 0.0
	for i := 0; i < n; i++ {
		trAtA += AtA[i][i] //sums up all of the diagonal ATA terms
	} /* trace of AtA */
	for i := 0; i < n; i++ {
		AtA[i][i] += beta * trAtA
	} /* add beta I */

	_, err = LdlDcmp(AtA, n, diag, b, x, true, false) /*  L D L'  decomp */
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}
	for j := 0; j < n; j++ { /* compute inv(AtA) */
		for k := 0; k < n; k++ {
			b[k] = 0.0
		}
		b[j] = 1.0
		_, err = LdlDcmp(AtA, n, diag, b, x, false, true) /* L D L' bksbtn */
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return finalErr
			}
		}

		theErr := float64(1.0)
		ok := true
		for ok {
			ok, err = LdlMprove(AtA, n, diag, b, x, &theErr)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}
		}
		for k := 0; k < n; k++ {
			AtAi[k][j] = x[k]
		} /* save inv(AtA) */
	}

	for i := 0; i < n; i++ { /* make symmetric */
		for j := i; j < n; j++ {
			AtAi[j][i] = 0.5 * (AtAi[i][j] + AtAi[j][i])
			AtAi[i][j] = AtAi[j][i]
		}
	}
	for i := 0; i < n; i++ { /* compute inv(A'*A)*A'	*/
		for j := 0; j < m; j++ {
			tmp = 0.0
			for k := 0; k < n; k++ {
				tmp += AtAi[i][k] * A[j][k]
			}
			Ai[i][j] = tmp
		}
	}
	return nil
}

// ProdABj performs matrix-matrix multiplication for symmetric A
// u = A * B(:,j)
// 27apr01
func ProdABj(A, B [][]float64, n, j int, u []float64) {
	for i := 0; i < n; i++ {
		u[i] = 0
		for k := 0; k < n; k++ {
			if i <= k {
				u[i] += A[i][k] * B[k][j]
			} else {
				u[i] += A[k][i] * B[k][j]
			}
		}
	}
}

// ProdAB performs matrix-matrix multiplication
// C = A * B
// 27apr01
func ProdAB(a, b, c [][]float64, I, J, K int) {
	for i := 0; i < I; i++ {
		for k := 0; k < K; k++ {
			c[i][k] = float64(0.0)
		}
	}
	for i := 0; i < I; i++ {
		for k := 0; k < K; k++ {
			for j := 0; j < J; j++ {
				c[i][k] += a[i][j] * b[j][k]
			}
		}
	}
	return
}

// InvAB calculates the product inv(A) * B
//  where A is n by n
//        B is n by m
// 6jun07
func InvAB(A, B [][]float64, n, m int, AiB [][]float64, ok *bool) (finalErr error) {
	diag := make([]float64, n)
	b := make([]float64, n)
	x := make([]float64, n)
	var err error

	_, err = LdlDcmp(A, n, diag, b, x, true, false) /*   L D L'  decomp */
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}
	for j := 0; j < m; j++ {
		for k := 0; k < n; k++ {
			b[k] = B[k][j]
		}
		_, err = LdlDcmp(A, n, diag, b, x, false, true) /*   L D L'  bksbtn */
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return finalErr
			}
		}
		theErr := float64(1.0)
		*ok = true
		for *ok { /* improve the solution*/
			*ok, err = LdlMprove(A, n, diag, b, x, &theErr)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}
		}

		for i := 0; i < n; i++ {
			AiB[i][j] = x[i]
		}
	}
	return nil
}

// XtInvAy calculates the quadratic form with inverse matrix
// X' * inv(A) * Y
//  where A is n by n
//        X is n by m
//        Y is n by m
// 15sep01
func XtInvAy(X, A, Y [][]float64, n, m int, Ac [][]float64) (finalErr error) {
	diag := make([]float64, n)
	x := make([]float64, n)
	y := make([]float64, n)
	var err error

	_, err = LdlDcmp(A, n, diag, y, x, true, false) /*   L D L'  decomp */
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	for j := 0; j < m; j++ {

		for k := 0; k < n; k++ {
			y[k] = Y[k][j]
		}
		_, err = LdlDcmp(A, n, diag, y, x, false, true) /*   L D L'  bksbtn */
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return finalErr
			}
		}

		ok := true
		theErr := float64(1.0)
		for ok { /* improve the solution*/
			ok, err = LdlMprove(A, n, diag, y, x, &theErr)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}

		}

		for i := 0; i < m; i++ {
			Ac[i][j] = 0.0
			for k := 0; k < n; k++ {
				Ac[i][j] += X[k][i] * x[k]
			}
		}
	}
	return nil
}

// XtAx carries out matrix-matrix-matrix multiplication for symmetric A
// C = X' A X
// where C is J by J
//       X is N by J
//       A is N by N
// 7nov02
func XtAx(A, X, C [][]float64, N, J int) {
	AX := Float2d(N, J)

	for i := 0; i < N; i++ { /*  use upper triangle of A */
		for j := 0; j < J; j++ {
			for k := 0; k < N; k++ {
				if i <= k {
					AX[i][j] += A[i][k] * X[k][j]
				} else {
					AX[i][j] += A[k][i] * X[k][j]
				}
			}
		}
	}

	for i := 0; i < J; i++ {
		for j := 0; j < J; j++ {
			C[i][j] = 0.0
			for k := 0; k < N; k++ {
				C[i][j] += X[k][i] * AX[k][j]
			}
		}

	}

	for i := 0; i < J; i++ { /*  make  C  symmetric */
		for j := i; j < J; j++ {
			C[j][i] = 0.5 * (C[i][j] + C[j][i])
			C[i][j] = C[j][i]
		}
	}
}

// XtAy carries out vector-matrix-vector multiplication for symmetric A
// 7apr9
func XtAy(x []float64, a [][]float64, y []float64, n int, d []float64) (out float64) {
	out = 0.0
	for i := 0; i < n; i++ { /*  d = A y  */
		d[i] = 0.0
		for j := 0; j < n; j++ { //  A in upper triangle only
			if i <= j {
				d[i] += a[i][j] * y[j]
			} else {
				d[i] += a[j][i] * y[j]
			}
		}
	}
	for i := 0; i < n; i++ { /*  xAy = x' A y  */
		out += x[i] * d[i]
	}
	return out
}
