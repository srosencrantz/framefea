/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package femath

import (
	"math"
)

const pideg float64 = math.Pi / float64(180.0)

// Sind returns sin(x) in degrees.
func Sind(x float64) float64 {
	return math.Sin(math.Mod(x, 360) * pideg)
}

// Cosd returns cos(x) in degrees.
func Cosd(x float64) float64 {
	return math.Cos(math.Mod(x, 360) * pideg)
}
