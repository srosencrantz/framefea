/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package femath

import (
	"math"
)

// Vec3 represents a 3D coordinate
type Vec3 struct {
	X, Y, Z float64
}

// ElemLength calcualtes the distance between two nodes a and b
func ElemLength(a, b Vec3) float64 {
	return math.Sqrt((b.X-a.X)*(b.X-a.X) + (b.Y-a.Y)*(b.Y-a.Y) + (b.Z-a.Z)*(b.Z-a.Z))
}

// Vect3d converts a Vec to a 3 element float array for compatibility with bitbucket.org/srosencrantz/vect3d
func (v Vec3) Vect3d() [3]float64 {
	return [3]float64{v.X, v.Y, v.Z}
}
