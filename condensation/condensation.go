/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package condensation

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
)

// Input contains the model information necessary to perform a matrix condensation.
type Input struct {

	// Node Info
	NumNodes   int    // number of Nodes
	ReactedDof []bool // {Dof} Dof's with reactions formally "r"

	// Dynamic Analysis Data
	NumModes int // number of desired modes

	// matrix Condensation Data
	Cdof               int   // number of condensed degrees of freedom
	CondensationMethod int   // matrix condensation method: 0, 1, 2, or 3
	CondensationData   []int // {DoF} matrix condensation data
	CondensedModeNums  []int // {DoF} vector of condensed mode numbers	*/

	// internal variables
	dof int // degrees of freedom

}

// DoF returns the number of degrees of freedom represented by the CondensationInput (6*NumNodes).
func (inData *Input) DoF() int {
	if inData.dof == 0 {
		inData.dof = inData.NumNodes * 6
	}
	return inData.dof
}

// Run condenses the stiffeness and mass matricies K and M respectivly return ing Kc and Mc.
func (inData *Input) Run(K, M, V [][]float64, f []float64) (Kc, Mc [][]float64, finalErr error) {

	Kc = femath.Float2d(inData.Cdof, inData.Cdof) // condensed stiffness matrix
	Mc = femath.Float2d(inData.Cdof, inData.Cdof) // condensed mass matrix
	var err error

	switch inData.CondensationMethod {
	case 1:
		// static condensation only
		err = staticCondensation(K, inData.DoF(), inData.CondensationData, inData.Cdof, Kc)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Kc, Mc, finalErr
			}
		}
	case 2:
		// dynamic condensation
		Cfreq := float64(0.0) // frequency used for Guyan condensation
		if inData.CondensedModeNums[0] > -1 && inData.NumModes > 0 {
			Cfreq = f[inData.CondensedModeNums[0]]
		}
		err = pazCondensation(M, K, inData.DoF(), inData.CondensationData, inData.Cdof, Mc, Kc, Cfreq)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Kc, Mc, finalErr
			}
		}
	case 3:
		// modal condensation
		if inData.NumModes <= 0 {
			return Kc, Mc, errcat.AppendErrStr(finalErr, fmt.Sprintf("PerformMatrixCondensation, Must have NumModes > 0 "+
				"to use Modal Condensation"))
		}
		if inData.Cdof > inData.NumModes && inData.CondensationMethod == 3 {
			return Kc, Mc, errcat.AppendErrStr(finalErr, fmt.Sprintf("PerformMatrixCondensation, Cdof > nM ... Cdof = %d  nM = %d "+
				"The number of condensed degrees of freedom may not exceed the number of computed modes when using "+
				"dynamic condensation.", inData.Cdof, inData.NumModes))
		}
		err = modalCondensation(M, K, inData.DoF(), inData.ReactedDof, inData.CondensationData, inData.Cdof, Mc, Kc, V, f,
			inData.CondensedModeNums)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Kc, Mc, finalErr
			}
		}
	}
	return Kc, Mc, finalErr
}

// staticCondensation of stiffness matrix from NxN to nxn
// A, a square matrix
// N, the dimension of the matrix
// c, list of matrix indices to retain
// n, the dimension of the condensed matrix
// Ac, the condensed matrix
func staticCondensation(A [][]float64, N int, c []int, n int, Ac [][]float64) (finalErr error) {

	r := make([]int, N-n)
	k := 0
	for i := 0; i < N; i++ {
		ok := true
		for j := 0; j < n; j++ {
			if c[j] == i {
				ok = false
				break
			}
		}
		if ok {
			r[k] = i
			k++
		}
	}

	var ri, rj int
	Arr := femath.Float2d(N-n, N-n)
	for i := 0; i < N-n; i++ {
		for j := i; j < N-n; j++ { /* use only upper triangle of A */
			ri = r[i]
			rj = r[j]
			if ri <= rj {
				Arr[i][j] = A[ri][rj]
				Arr[j][i] = Arr[i][j]
			}
		}
	}

	var cj int
	Arc := femath.Float2d(N-n, n)
	for i := 0; i < N-n; i++ {
		for j := 0; j < n; j++ { /* use only upper triangle of A */
			ri = r[i]
			cj = c[j]
			if ri < cj {
				Arc[i][j] = A[ri][cj]
			} else {
				Arc[i][j] = A[cj][ri]
			}
		}
	}

	err := femath.XtInvAy(Arc, Arr, Arc, N-n, n, Ac)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	var ci int
	for i := 0; i < n; i++ {
		for j := i; j < n; j++ { /* use only upper triangle of A */
			ci = c[i]
			cj = c[j]
			if ci <= cj {
				Ac[i][j] = A[ci][cj] - Ac[i][j]
				Ac[j][i] = Ac[i][j]
			}
		}
	}
	return finalErr
}

// pazCondensation of mass and stiffness matrices
// 	matches the response at a particular frequency, sqrt(L)/2/pi
//         Paz M. Dynamic condensation. AIAA J 1984;22(5):724-727.
// 	double **M, double **K,	/**< mass and stiffness matrices	*/
// 	int N,			/**< dimension of the matrices, DoF	*/
// 	int *q,			/**< list of degrees of freedom to retain */
// 	int n,			/**< dimension of the condensed matrices */
// 	double **Mc, double **Kc,	/**< the condensed matrices	*/
// 	double w2,		/**< matched value of frequency squared	*/
func pazCondensation(M, K [][]float64, N int, c []int, n int, Mc, Kc [][]float64, w2 float64) (finalErr error) {
	var i, j, k, ri, rj, cj int
	var ok bool
	//fmt.Println("N: ", N, " n: ", n)
	r := make([]int, N-n)
	Drr := femath.Float2d(N-n, N-n)
	Drc := femath.Float2d(N-n, n)
	invDrrDrc := femath.Float2d(N-n, n) /* inv(Drr) * Drc	*/
	T := femath.Float2d(N, n)           /* coordinate transformation matrix	*/

	w2 = 4.0 * math.Pi * math.Pi * w2 * w2 /* eigen-value ... omega^2 	*/

	/* find "remaining" (r) degrees of freedom, not "condensed" (c)	*/
	k = 0
	for i = 0; i < N; i++ {
		ok = true
		for j = 0; j < n; j++ {
			if c[j] == i {
				ok = false
				break
			}
		}
		if ok {
			r[k] = i
			k++
		}
	}

	for i = 0; i < N-n; i++ {
		for j = 0; j < N-n; j++ { /* use only upper triangle of K,M */
			ri = r[i]
			rj = r[j]
			if ri <= rj {
				Drr[i][j] = K[ri][rj] - w2*M[ri][rj]
				Drr[j][i] = Drr[i][j]
			} else {
				Drr[i][j] = K[rj][ri] - w2*M[rj][ri]
				Drr[j][i] = Drr[i][j]
			}
		}
	}

	for i = 0; i < N-n; i++ {
		for j = 0; j < n; j++ { /* use only upper triangle of K,M */
			ri = r[i]
			cj = c[j]
			if ri < cj {
				Drc[i][j] = K[ri][cj] - w2*M[ri][cj]
			} else {
				Drc[i][j] = K[cj][ri] - w2*M[cj][ri]
			}
		}
	}

	err := femath.InvAB(Drr, Drc, N-n, n, invDrrDrc, &ok) /* inv(Drr) * Drc */
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	/* coordinate transformation matrix	*/
	for i = 0; i < n; i++ {
		for j = 0; j < n; j++ {
			T[c[i]][j] = 0.0
			T[c[i]][i] = 1.0
		}
	}
	for i = 0; i < N-n; i++ {
		for j = 0; j < n; j++ {
			T[r[i]][j] = -invDrrDrc[i][j]
		}
	}

	femath.XtAx(K, T, Kc, N, n) /* Kc = T' * K * T	*/
	femath.XtAx(M, T, Mc, N, n) /* Mc = T' * M * T	*/

	return finalErr
}

// modalCondensation
// 	dynamic condensation of mass and stiffness matrices
// 	matches the response at a set of frequencies
// 	@NOTE Kc and Mc may be ill-conditioned, and xyzsibly non-positive def.
// 	double **M, double **K,	/**< mass and stiffness matrices	*/
// 	int N,			/**< dimension of the matrices, DoF	*/
// 	int *R,		/**< R[i]=1: DoF i is fixed, R[i]=0: DoF i is free */
// 	int *p,		/**< list of primary degrees of freedom		*/
// 	int n,		/**< the dimension of the condensed matrix	*/
// 	double **Mc, double **Kc,	/**< the condensed matrices	*/
// 	double **V, double *f,	/**< mode shapes and natural frequencies*/
// 	int *m,		/**< list of modes to match in the condensed model */
func modalCondensation(M, K [][]float64, N int, R []bool, p []int, n int, Mc, Kc, V [][]float64, f []float64, m []int) (finalErr error) {
	var i, j, k int
	var traceM, traceMc, Aij float64
	P := make([][]float64, n)
	invP := make([][]float64, n)
	for i = range P {
		P[i] = make([]float64, n)
		invP[i] = make([]float64, n)
	}

	for i = 0; i < n; i++ { /* first n modal vectors at primary DoF's */
		for j = 0; j < n; j++ {
			P[i][j] = V[p[i]][m[j]]
		}
	}

	err := femath.PseudoInv(P, invP, n, n, 1e-9)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	for i = 0; i < N; i++ { //i up to dimension of the Matricies DOF
		if !R[i] { //if dof is fixed
			traceM += M[i][i] //Summing up diagonal Terms of fixed Dof
		}
	}

	for i = 0; i < n; i++ { /* compute inv(P)' * I * inv(P)	*/
		for j = 0; j < n; j++ {
			Aij = 0.0
			for k = 0; k < n; k++ {
				Aij += invP[k][i] * invP[k][j]
			}
			Mc[i][j] = Aij
		}
	}
	for i = 0; i < n; i++ {
		traceMc += Mc[i][i]
	}

	for i = 0; i < n; i++ { /* compute inv(P)' * W^2 * inv(P) */
		for j = 0; j < n; j++ {
			Aij = 0.0
			for k = 0; k < n; k++ {
				Aij += invP[k][i] * 4.0 * math.Pi * math.Pi * f[m[k]] * f[m[k]] * invP[k][j]
			}
			Kc[i][j] = Aij
		}
	}
	for i = 0; i < n; i++ {
		for j = 0; j < n; j++ {
			Mc[i][j] *= (traceM / traceMc)
		}
	}
	for i = 0; i < n; i++ {
		for j = 0; j < n; j++ {
			Kc[i][j] *= (traceM / traceMc)
		}
	}
	return finalErr
}
