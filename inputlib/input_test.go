/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"fmt"
	"testing"

	"bitbucket.org/srosencrantz/testhelp"
)

func TestIO(t *testing.T) {
	filename := "sample.ffin"
	tempname := "sampletemp.ffin"
	frameData, inData, err := ReadFile(filename)
	if err != nil {
		t.Fatalf("Couldn't read %s, err:\n %s", filename, err)
	}
	err = inData.WriteFile(tempname, "test")
	if err != nil {
		t.Fatalf("Couldn't write %s, err:\n %s", tempname, err)
	}
	frameData2, inData2, err := ReadFile(tempname)
	if err != nil {
		t.Fatalf("Couldn't read %s, err:\n %s", tempname, err)
	}

	for i, rec := range inData.StaticParams {
		testhelp.CompareData(t, fmt.Sprintf("StaticParams %d", i), rec, inData2.StaticParams[i])
	}

	for i, rec := range inData.Nodes {
		testhelp.CompareData(t, fmt.Sprintf("Nodes %d", i), rec, inData2.Nodes[i])
	}

	for i, rec := range inData.Reactions {
		testhelp.CompareData(t, fmt.Sprintf("Reactions %d", i), rec, inData2.Reactions[i])
	}

	for i, rec := range inData.Elements {
		testhelp.CompareData(t, fmt.Sprintf("Elements %d", i), rec, inData2.Elements[i])
	}

	for i, rec := range inData.Materials {
		testhelp.CompareData(t, fmt.Sprintf("Materials %d", i), rec, inData2.Materials[i])
	}

	for i, rec := range inData.Curves {
		testhelp.CompareData(t, fmt.Sprintf("Curves %d", i), rec, inData2.Curves[i])
	}

	for i, rec := range inData.BodyLoads {
		testhelp.CompareData(t, fmt.Sprintf("BodyLoads %d", i), rec, inData2.BodyLoads[i])
	}

	for i, rec := range inData.ElementPointLoads {
		testhelp.CompareData(t, fmt.Sprintf("ElementPointLoads %d", i), rec, inData2.ElementPointLoads[i])
	}

	for i, rec := range inData.NodalDisplacements {
		testhelp.CompareData(t, fmt.Sprintf("NodalDisplacements %d", i), rec, inData2.NodalDisplacements[i])
	}

	for i, rec := range inData.NodalLoads {
		testhelp.CompareData(t, fmt.Sprintf("NodalLoads %d", i), rec, inData2.NodalLoads[i])
	}

	for i, rec := range inData.TemperatureLoads {
		testhelp.CompareData(t, fmt.Sprintf("TemperatureLoads %d", i), rec, inData2.TemperatureLoads[i])
	}

	for i, rec := range inData.TrapezoidLoads {
		testhelp.CompareData(t, fmt.Sprintf("TrapezoidLoads %d", i), rec, inData2.TrapezoidLoads[i])
	}

	for i, rec := range inData.UniformLoads {
		testhelp.CompareData(t, fmt.Sprintf("UniformLoads %d", i), rec, inData2.UniformLoads[i])
	}

	for i, rec := range inData.ModalParams {
		testhelp.CompareData(t, fmt.Sprintf("ModalParams %d", i), rec, inData2.ModalParams[i])
	}

	for i, rec := range inData.NodalMasses {
		testhelp.CompareData(t, fmt.Sprintf("NodalMasses %d", i), rec, inData2.NodalMasses[i])
	}

	for i, rec := range inData.ElementMasses {
		testhelp.CompareData(t, fmt.Sprintf("ElementMasses %d", i), rec, inData2.ElementMasses[i])
	}

	for i, rec := range inData.CondensationParams {
		testhelp.CompareData(t, fmt.Sprintf("CondensationParams %d", i), rec, inData2.CondensationParams[i])
	}

	for i, rec := range inData.CondensedNodes {
		testhelp.CompareData(t, fmt.Sprintf("CondensedNodes %d", i), rec, inData2.CondensedNodes[i])
	}

	for i, rec := range inData.CondensedModes {
		testhelp.CompareData(t, fmt.Sprintf("CondensedModes %d", i), rec, inData2.CondensedModes[i])
	}

	testhelp.CompareData(t, "FrameData", frameData, frameData2)

}
