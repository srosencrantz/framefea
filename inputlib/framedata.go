/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/condensation"
	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/framefea/modal"
	"bitbucket.org/srosencrantz/framefea/newmarkbeta"
	"bitbucket.org/srosencrantz/framefea/static"
)

/*FrameData contains all the input data in a form that can be used directly
to run analyses (Assembled loads, nodes and elements that just use index id's etc...)*/
type FrameData struct {
	// Run Info

	Shear bool // indicates shear deformation
	Geom  bool // indicates  geometric nonlinearity

	// Node Info
	NumNodes   int           // number of Nodes
	Xyz        []femath.Vec3 // {NumNodes} X,Y,Z node coordinates (global)
	NodeRadius []float64     // {NumNodes} node size radius, for finite sizes

	// Reaction Info
	ReactedDof   []bool // {Dof} Dof's with reactions formally "r"
	UnreactedDof []bool // {Dof} Dof's without reactions formally "q" (calculated)

	// Element Info
	NumElems     int       // Number of Frame Elements
	N1, N2       []int     // {NumElems} begin and end node numbers
	Ax, Asy, Asz []float64 // {NumElems} cross section areas, incl. shear
	Jx, Iy, Iz   []float64 // {NumElems} section inertias
	E, G         []float64 // {NumElems} elastic modulus, shear modulus
	ElemRoll     []float64 // {NumElems} roll of each member, radians
	ElemDensity  []float64 // {NumElems} member densities

	L  []float64 //  node-to-node length of each element (calculated)
	Le []float64 //  effective length, accounts for node size (calculated)

	// Load Info
	PerformStaticAnalysis bool
	StaticTol             float64   // Convergence tolerance for the static analysis when Geom == true
	AnalyzeLoadCase       bool      //  Are there mechanical loads in Mechanical Loads
	MechanicalLoads       []float64 // {Dof} mechanical load vectors, all load cases
	Dp                    []float64 // {Dof} prescribed node displacements

	TempLoads    bool      // Are there temperature loads in the load case
	ThermalLoads []float64 // {Dof} thermal load vectors, all load cases

	EqFMech [][]float64 //  {numElems}{12} equivalent end forces from mech loads global (calculated)
	EqFTemp [][]float64 //  {numElems}{12} equivalent end forces from temp loads global (calculated)

	// Dynamic Analysis Data
	PerformModalAnalysis bool
	NumModes             int       // number of desired modes
	ModalMethod          int       // modal analysis method: 1=Jacobi-Subspace or 2=Stodola
	LumpFlag             bool      // true: lumped mass matrix or false: consistent mass matrix
	ModalTol             float64   // convergence tolerance for modal analysis
	ModalFreqShift       float64   // modal frequency shift for unrestrained structures, shift-factor for rigid-body-modes
	NMs                  []float64 // {NumNodes} node mass for each node
	NMx                  []float64 // {NumNodes} node inertia about global X axis
	NMy                  []float64 // {NumNodes} node inertia about global Y axis
	NMz                  []float64 // {NumNodes} node inertia about global Z axis
	EMs                  []float64 // {NumElems} lumped mass for each frame element

	// matrix Condensation Data
	PerformCondensation bool
	Cdof                int   // number of condensed degrees of freedom
	CondensationMethod  int   // matrix condensation method: 0, 1, 2, or 3
	CondensationData    []int // {DoF} matrix condensation data
	CondensedModeNums   []int // {DoF} vector of condensed mode numbers	*/

	// Newmark-beta Data
	PerformNewmarkBetaAnalysis bool
	NBAlpha                    float64
	NBBeta                     float64
	NBDt                       float64
	NBTermTime                 float64
	Vi                         []float64 // {Dof} initial nodal velocities
	Ai                         []float64 // {Dof} initial nodal accelerations

	// convenience maps
	NodeMap  map[int]int // map of external node number to internal node number
	INodeMap map[int]int // inverse of NodeMap. map of internal to external node number
	DofMap   map[int]int // map of external node ID to lowest degree of freedom (the x dir)
	ElemMap  map[int]int // map of external to internal element number
	IElemMap map[int]int // inverse of ElemMap. map of internal to external element number

	// internal variables
	dof int // degrees of freedom
}

// IMapInt returns the inverse of type map[int]int (map[1]2 -> map[2]1)
func IMapInt(OrigMap map[int]int) (InvMap map[int]int) {
	if len(OrigMap) == 0 {
		return nil
	}

	InvMap = make(map[int]int)

	for key, val := range OrigMap {
		InvMap[val] = key
	}
	return InvMap
}

// DoF returns the number of degrees of freedom described by a FrameInput2 structure (6*NumNodes).
func (fd *FrameData) DoF() int {
	if fd.dof == 0 {
		fd.dof = fd.NumNodes * 6
	}
	return fd.dof
}

// NewStaticInput converts a FrameData structure to the StaticInput structure needed for a static structural analysis.
func (fd *FrameData) NewStaticInput() (si *static.Input) {
	return &static.Input{Shear: fd.Shear, Geom: fd.Geom, NumNodes: fd.NumNodes, Xyz: fd.Xyz, NodeRadius: fd.NodeRadius, ReactedDof: fd.ReactedDof, UnreactedDof: fd.UnreactedDof, NumElems: fd.NumElems, N1: fd.N1, N2: fd.N2, Ax: fd.Ax, Asy: fd.Asy, Asz: fd.Asz, Jx: fd.Jx, Iy: fd.Iy, Iz: fd.Iz, E: fd.E, G: fd.G, ElemRoll: fd.ElemRoll, L: fd.L, Le: fd.Le, StaticTol: fd.StaticTol, AnalyzeLoadCase: fd.AnalyzeLoadCase, MechanicalLoads: fd.MechanicalLoads, Dp: fd.Dp, TempLoads: fd.TempLoads, ThermalLoads: fd.ThermalLoads, EqFMech: fd.EqFMech, EqFTemp: fd.EqFTemp}
}

// NewModalInput converts a FrameData structure to the ModalInput structure needed for a modal analysis.
func (fd *FrameData) NewModalInput() (mi *modal.Input) {
	return &modal.Input{NumNodes: fd.NumNodes, Xyz: fd.Xyz, NodeRadius: fd.NodeRadius, ReactedDof: fd.ReactedDof, NumElems: fd.NumElems, N1: fd.N1, N2: fd.N2, Ax: fd.Ax, Jx: fd.Jx, Iy: fd.Iy, Iz: fd.Iz, ElemRoll: fd.ElemRoll, ElemDensity: fd.ElemDensity, L: fd.L, NumModes: fd.NumModes, ModalMethod: fd.ModalMethod, LumpFlag: fd.LumpFlag, ModalTol: fd.ModalTol, ModalFreqShift: fd.ModalFreqShift, NMs: fd.NMs, NMx: fd.NMx, NMy: fd.NMy, NMz: fd.NMz, EMs: fd.EMs}
}

// NewCondensationInput converts a FrameData structure to the CondensationInput structure needed for matrix condensation.
func (fd *FrameData) NewCondensationInput() (mi *condensation.Input) {
	return &condensation.Input{NumNodes: fd.NumNodes, ReactedDof: fd.ReactedDof, NumModes: fd.NumModes, Cdof: fd.Cdof, CondensationMethod: fd.CondensationMethod, CondensationData: fd.CondensationData, CondensedModeNums: fd.CondensedModeNums}
}

// NewNewmarkBetaInput converts a FrameData structure to the NewmarkBetaInput structure needed for NewmarkBeta time integration analysis.
func (fd *FrameData) NewNewmarkBetaInput() (si *newmarkbeta.Input) {
	return &newmarkbeta.Input{Shear: fd.Shear, Geom: fd.Geom, NumNodes: fd.NumNodes, Xyz: fd.Xyz, NodeRadius: fd.NodeRadius, ReactedDof: fd.ReactedDof, UnreactedDof: fd.UnreactedDof, NumElems: fd.NumElems, N1: fd.N1, N2: fd.N2, Ax: fd.Ax, Asy: fd.Asy, Asz: fd.Asz, Jx: fd.Jx, Iy: fd.Iy, Iz: fd.Iz, E: fd.E, G: fd.G, ElemRoll: fd.ElemRoll, ElemDensity: fd.ElemDensity, L: fd.L, Le: fd.Le, StaticTol: fd.StaticTol, AnalyzeLoadCase: fd.AnalyzeLoadCase, MechanicalLoads: fd.MechanicalLoads, TempLoads: fd.TempLoads, ThermalLoads: fd.ThermalLoads, EqFMech: fd.EqFMech, EqFTemp: fd.EqFTemp, LumpFlag: fd.LumpFlag, NMs: fd.NMs, NMx: fd.NMx, NMy: fd.NMy, NMz: fd.NMz, EMs: fd.EMs, Alpha: fd.NBAlpha, Beta: fd.NBBeta, Dt: fd.NBDt, U0: fd.Dp, V0: fd.Vi, A0: fd.Ai}
}

// CreateFrameData converts the Inputfile struct to a FrameData struct that can be used to run analyses.
func CreateFrameData(in *Description) (fd *FrameData, err error) {
	fd = &FrameData{
		NodeMap:  make(map[int]int),
		INodeMap: make(map[int]int),
		DofMap:   make(map[int]int),
		ElemMap:  make(map[int]int),
		IElemMap: make(map[int]int)}

	// make a map of nodes that are used in either n1 or n2 of the elements so we don't include any unconnected nodes
	connectedNodeMap := make(map[int]bool)
	unconNodeMap := make(map[int]int)

	for i := 0; i < len(in.Elements); i++ {
		connectedNodeMap[in.Elements[i].Nid1] = true
		connectedNodeMap[in.Elements[i].Nid2] = true
	}

	for j := 0; j < len(in.Nodes); j++ {
		id := in.Nodes[j].ID
		if _, ok := connectedNodeMap[id]; !ok {
			unconNodeMap[id] = j
		}
	}

	// What analysis is being performed?

	fd.AnalyzeLoadCase = len(in.NodalLoads) > 0 || len(in.UniformLoads) > 0 || len(in.TrapezoidLoads) > 0 || len(in.ElementPointLoads) > 0 || len(in.NodalDisplacements) > 0 || in.BodyLoads[0].Gx != 0 || in.BodyLoads[0].Gy != 0 || in.BodyLoads[0].Gz != 0

	fd.TempLoads = len(in.TemperatureLoads) > 0

	staticPresent := len(in.StaticParams) > 0
	modalPresent := len(in.ModalParams) > 0
	condPresent := len(in.CondensationParams) > 0
	nbPresent := len(in.NewmarkBetaParams) > 0

	switch {
	case nbPresent && !condPresent && modalPresent && staticPresent:
		fd.PerformNewmarkBetaAnalysis = true
	case condPresent && !nbPresent && modalPresent && staticPresent:
		fd.PerformCondensation = true
	case modalPresent && staticPresent && !nbPresent && !condPresent:
		fd.PerformModalAnalysis = true
	case staticPresent && !modalPresent && !nbPresent && !condPresent && (fd.AnalyzeLoadCase || fd.TempLoads):
		fd.PerformStaticAnalysis = true
	}

	// RunInfo
	defaultTemperature := fd.processStaticParams(in.StaticParams[0])

	// Nodes
	fd.NodeMap = fd.processNodes(in.Nodes, connectedNodeMap)
	fd.INodeMap = IMapInt(fd.NodeMap)

	// Create Dof Map
	for nID, nIdx := range fd.NodeMap {
		fd.DofMap[nID] = nIdx * 6
	}

	// Reactions
	err = fd.processReactions(in.Reactions)
	if err != nil {
		return nil, err
	}

	// Elements
	var elemMatMap map[int]int
	var elemTempMap map[int]float64
	fd.ElemMap, elemMatMap, elemTempMap, err = fd.processElements(in.Elements, in.Nodes, unconNodeMap)
	fd.IElemMap = IMapInt(fd.ElemMap)
	if err != nil {
		return nil, err
	}

	// Materials
	var matMap map[int]int
	matMap, err = fd.processMaterials(in.Materials, in.Elements, defaultTemperature)
	if err != nil {
		return nil, err
	}

	// Loads
	if fd.AnalyzeLoadCase {
		//process and assemble nodal loads, body loads, uniform loads, trapazoid loads, internal element point loads, and thermal loads.
		// this sets up fd.MechanicalLoads, fd.ThermalLoads, fd.EqFMech, and fd.EqFTemp
		fd.MechanicalLoads = make([]float64, fd.DoF())
		fd.EqFMech = femath.Float2d(fd.NumElems, 12) /* eqF due to mech loads */
		fd.EqFTemp = femath.Float2d(fd.NumElems, 12) /* eqF due to temp loads */

		// Prescribed displacements
		fd.processNodalDisplacements(in.NodalDisplacements)
		fd.processNodalVelocities(in.NodalVelocities)
		fd.processNodalAccelerations(in.NodalAccelerations)
		fd.processBodyLoads(in.BodyLoads)
		fd.processNodalLoads(in.NodalLoads)

		err = fd.processUniformLoads(in.UniformLoads)
		if err != nil {
			return fd, errcat.AppendErr(nil, err)
		}

		err = fd.processTrapezoidLoads(in.TrapezoidLoads)
		if err != nil {
			return fd, errcat.AppendErr(nil, err)
		}

		err = fd.processElementPointLoads(in.ElementPointLoads)
		if err != nil {
			return fd, errcat.AppendErr(nil, err)
		}

		err = fd.processTemperatureLoads(in.TemperatureLoads, in.Materials, matMap, elemMatMap, elemTempMap)
		if err != nil {
			return fd, errcat.AppendErr(nil, err)
		}

		// assemble all element equivalent loads into
		// separate load vectors for mechanical and thermal loading
		for n := 0; n < fd.NumElems; n++ {
			n1 := fd.N1[n]
			n2 := fd.N2[n]
			for i := 0; i < 6; i++ {
				fd.MechanicalLoads[6*n1+i] += fd.EqFMech[n][i]
			}
			for i := 6; i < 12; i++ {
				fd.MechanicalLoads[6*n2-6+i] += fd.EqFMech[n][i]
			}
			for i := 0; i < 6; i++ {
				fd.ThermalLoads[6*n1+i] += fd.EqFTemp[n][i]
			}
			for i := 6; i < 12; i++ {
				fd.ThermalLoads[6*n2-6+i] += fd.EqFTemp[n][i]
			}
		}

	}

	// modal stuff
	if fd.PerformModalAnalysis || fd.PerformNewmarkBetaAnalysis || fd.PerformCondensation {
		fd.processModalParams(in.ModalParams[0])
		fd.processNodalMasss(in.NodalMasses)
		fd.processElementMasss(in.ElementMasses)
	}

	// condensation stuff
	if fd.PerformCondensation {
		fd.processCondensationParams(in.CondensationParams[0])
		fd.processCondensedNodes(in.CondensedNodes)
		fd.processDynamicCondensationModes(in.CondensedModes[0])
	}

	// Newmark Beta Stuff
	if fd.PerformNewmarkBetaAnalysis {
		fd.processNewmarkBetaParams(in.NewmarkBetaParams[0])
		fd.processNodalVelocities(in.NodalVelocities)
		fd.processNodalAccelerations(in.NodalAccelerations)
	}

	return fd, nil
}
