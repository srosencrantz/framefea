/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"math"

	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/framefea/transform"
	"bitbucket.org/srosencrantz/utility"
	"bitbucket.org/srosencrantz/vect3d"
)

// calcRollAngle calculates the roll angle required by framefea to orient the s/t or yy, zz directions of the beam consistantly between dyna and framefea and returns the value in degrees.
func calcRollAngle(n1, n2, bp femath.Vec3) (angle float64) {
	axis := vect3d.Subtract(n2.Vect3d(), n1.Vect3d())
	bpR := vect3d.Subtract(bp.Vect3d(), n1.Vect3d()) // bp relative to n1
	bpNorm := vect3d.Normalize(bpR)
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64
	transform.CoordTrans([]femath.Vec3{n1, n2}, femath.ElemLength(n1, n2), 0, 1, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, 0.0)
	bptrans := smallAtma(t1, t2, t3, t4, t5, t6, t7, t8, t9, femath.Vec3{X: 0, Y: 0, Z: 1} /*localBeamPoint*/)

	return rad2deg(vect3d.GetSignedAngle(bptrans.Vect3d(), bpNorm, axis)) + float64(90.0)
}

func rad2deg(rad float64) float64 {
	return rad * (180 / math.Pi)
}

// smallAtma performs the inverse rotational transform on a vector
func smallAtma(t1, t2, t3, t4, t5, t6, t7, t8, t9 float64, xyz femath.Vec3) (newXyz femath.Vec3) {
	newXyz.X = t1*xyz.X + t4*xyz.Y + t7*xyz.Z
	newXyz.Y = t2*xyz.X + t5*xyz.Y + t8*xyz.Z
	newXyz.Z = t3*xyz.X + t6*xyz.Y + t9*xyz.Z
	return newXyz
}

// Roll2Node creates the third node for an element when the endpoints and roll are known
func Roll2Node(node1, node2 *Node, roll float64, id int) (node3 *Node) {
	n1 := femath.Vec3{X: node1.Loc[0], Y: node1.Loc[1], Z: node1.Loc[2]}
	n2 := femath.Vec3{X: node2.Loc[0], Y: node2.Loc[1], Z: node2.Loc[2]}
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64
	transform.CoordTrans([]femath.Vec3{n1, n2}, femath.ElemLength(n1, n2), 0, 1,
		&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, roll)
	bptrans := smallAtma(t1, t2, t3, t4, t5, t6, t7, t8, t9,
		femath.Vec3{X: 0, Y: 1, Z: 0})
	node3 = &Node{ID: id, Loc: vect3d.Add(node1.Loc, vect3d.Normalize(bptrans.Vect3d())), Rad: 0.0}
	return node3
}

type pointList [][2]float64

func (a pointList) Len() int           { return len(a) }
func (a pointList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a pointList) Less(i, j int) bool { return a[i][0] < a[j][0] }

// GetYVal returns the interpolated value from the Curve.
func (a *Curve) GetYVal(xval float64) (yval float64, err error) {
	x := make([]float64, len(a.Points))
	y := make([]float64, len(a.Points))
	for i, pt := range a.Points {
		x[i], y[i] = pt[0], pt[1]
	}
	yval, err = utility.LinearInterpolation(x, y, xval)
	return yval, err
}
