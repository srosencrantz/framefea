/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/transform"
)

// ProcessBodyLoads uses the input data and the load case number to populate eqFMech
func (fd *FrameData) processBodyLoads(bll []*BodyLoad) {
	for n := 0; n < fd.NumElems; n++ {
		n1 := fd.N1[n]
		n2 := fd.N2[n]
		var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* 3D coord Xfrm coeffs */
		transform.CoordTrans(fd.Xyz, fd.L[n], n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, fd.ElemRoll[n])

		fd.EqFMech[n][0] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gx / 2.0
		fd.EqFMech[n][1] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gy / 2.0
		fd.EqFMech[n][2] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gz / 2.0

		fd.EqFMech[n][3] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((-t4*t8+t5*t7)*bll[0].Gy + (-t4*t9+t6*t7)*bll[0].Gz)
		fd.EqFMech[n][4] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((-t5*t7+t4*t8)*bll[0].Gx + (-t5*t9+t6*t8)*bll[0].Gz)
		fd.EqFMech[n][5] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((-t6*t7+t4*t9)*bll[0].Gx + (-t6*t8+t5*t9)*bll[0].Gy)

		fd.EqFMech[n][6] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gx / 2.0
		fd.EqFMech[n][7] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gy / 2.0
		fd.EqFMech[n][8] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * bll[0].Gz / 2.0

		fd.EqFMech[n][9] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((t4*t8-t5*t7)*bll[0].Gy + (t4*t9-t6*t7)*bll[0].Gz)
		fd.EqFMech[n][10] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((t5*t7-t4*t8)*bll[0].Gx + (t5*t9-t6*t8)*bll[0].Gz)
		fd.EqFMech[n][11] = fd.ElemDensity[n] * fd.Ax[n] * fd.L[n] * fd.L[n] / 12.0 *
			((t6*t7-t4*t9)*bll[0].Gx + (t6*t8-t5*t9)*bll[0].Gy)
	}
}

// processElementPointLoads adds ElementPointLoad's data to a FrameData struct.
func (fd *FrameData) processElementPointLoads(epll []*ElementPointLoad) (err error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* 3D coord Xfrm coeffs */

	numConcPointLoads := len(epll)
	P := make([][]float64, numConcPointLoads)
	for i := 0; i < numConcPointLoads; i++ {
		P[i] = []float64{float64(fd.ElemMap[epll[i].Eid]), epll[i].X, epll[i].Y, epll[i].Z, epll[i].Pos}
	}

	for i := 0; i < numConcPointLoads; i++ { /* ! local element coordinates ! */

		a := P[i][4]
		n := int(P[i][0])
		b := fd.L[n] - a

		if a < 0 || fd.L[n] < a || b < 0 || fd.L[n] < b {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessInternalElementPointLoads, in point load data: "+
				"Point load coord. out of range Frame element number: %d  L: %g  load coord.: %g", n, fd.L[n], P[i][4]))
		}
		var Ksy, Ksz float64 /* shear deformatn coefficients	*/

		if fd.Shear {
			Ksy = (12.0 * fd.E[n] * fd.Iz[n]) / (fd.G[n] * fd.Asy[n] * fd.Le[n] * fd.Le[n])
			Ksz = (12.0 * fd.E[n] * fd.Iy[n]) / (fd.G[n] * fd.Asz[n] * fd.Le[n] * fd.Le[n])
		}

		Ln := fd.L[n]

		Nx1 := P[i][1] * a / Ln
		Nx2 := P[i][1] * b / Ln

		Vy1 := (1./(1.+Ksz))*P[i][2]*b*b*(3.*a+b)/(Ln*Ln*Ln) +
			(Ksz/(1.+Ksz))*P[i][2]*b/Ln
		Vy2 := (1./(1.+Ksz))*P[i][2]*a*a*(3.*b+a)/(Ln*Ln*Ln) +
			(Ksz/(1.+Ksz))*P[i][2]*a/Ln

		Vz1 := (1./(1.+Ksy))*P[i][3]*b*b*(3.*a+b)/(Ln*Ln*Ln) +
			(Ksy/(1.+Ksy))*P[i][3]*b/Ln
		Vz2 := (1./(1.+Ksy))*P[i][3]*a*a*(3.*b+a)/(Ln*Ln*Ln) +
			(Ksy/(1.+Ksy))*P[i][3]*a/Ln

		Mx1, Mx2 := 0.0, 0.0

		My1 := -(1./(1.+Ksy))*P[i][3]*a*b*b/(Ln*Ln) -
			(Ksy/(1.+Ksy))*P[i][3]*a*b/(2.*Ln)
		My2 := (1./(1.+Ksy))*P[i][3]*a*a*b/(Ln*Ln) +
			(Ksy/(1.+Ksy))*P[i][3]*a*b/(2.*Ln)

		Mz1 := (1./(1.+Ksz))*P[i][2]*a*b*b/(Ln*Ln) +
			(Ksz/(1.+Ksz))*P[i][2]*a*b/(2.*Ln)
		Mz2 := -(1./(1.+Ksz))*P[i][2]*a*a*b/(Ln*Ln) -
			(Ksz/(1.+Ksz))*P[i][2]*a*b/(2.*Ln)

		n1 := fd.N1[n]
		n2 := fd.N2[n]

		transform.CoordTrans(fd.Xyz, Ln, n1, n2,
			&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, fd.ElemRoll[n])

		/* {F} = [T]'{Q} */
		fd.EqFMech[n][0] += (Nx1*t1 + Vy1*t4 + Vz1*t7)
		fd.EqFMech[n][1] += (Nx1*t2 + Vy1*t5 + Vz1*t8)
		fd.EqFMech[n][2] += (Nx1*t3 + Vy1*t6 + Vz1*t9)
		fd.EqFMech[n][3] += (Mx1*t1 + My1*t4 + Mz1*t7)
		fd.EqFMech[n][4] += (Mx1*t2 + My1*t5 + Mz1*t8)
		fd.EqFMech[n][5] += (Mx1*t3 + My1*t6 + Mz1*t9)

		fd.EqFMech[n][6] += (Nx2*t1 + Vy2*t4 + Vz2*t7)
		fd.EqFMech[n][7] += (Nx2*t2 + Vy2*t5 + Vz2*t8)
		fd.EqFMech[n][8] += (Nx2*t3 + Vy2*t6 + Vz2*t9)
		fd.EqFMech[n][9] += (Mx2*t1 + My2*t4 + Mz2*t7)
		fd.EqFMech[n][10] += (Mx2*t2 + My2*t5 + Mz2*t8)
		fd.EqFMech[n][11] += (Mx2*t3 + My2*t6 + Mz2*t9)
	} /* end element point loads */
	return nil
}

// processNodalDisplacements adds NodalDisp's data to a FrameData struct.
func (fd *FrameData) processNodalDisplacements(ndl []*NodalDisplacement) (err error) {
	fd.Dp = make([]float64, fd.DoF())
	for i := 0; i < len(ndl); i++ {
		intNid := fd.NodeMap[ndl[i].Nid]
		fd.Dp[intNid*6+0] = ndl[i].Dx
		fd.Dp[intNid*6+1] = ndl[i].Dy
		fd.Dp[intNid*6+2] = ndl[i].Dz
		fd.Dp[intNid*6+3] = ndl[i].Dxx
		fd.Dp[intNid*6+4] = ndl[i].Dyy
		fd.Dp[intNid*6+5] = ndl[i].Dzz
	}

	if fd.PerformNewmarkBetaAnalysis {
		return nil
	}

	for j := 0; j < fd.DoF(); j++ {
		if !fd.ReactedDof[j] && fd.Dp[j] != 0.0 {
			return errcat.AppendErrStr(nil, fmt.Sprintf("CreateFrameData, Initial displacements can be "+
				"prescribed only at restrained coordinates node: %d  dof: %d  r: %v",
				int(math.Trunc(float64(j/6))), int(math.Mod(float64(j), 6)), fd.ReactedDof[j]))
		}
	}
	return nil
}

// processNodalLoads adds NodalLoad's data to a FrameData struct.
func (fd *FrameData) processNodalLoads(nll []*NodalLoad) {
	for i := 0; i < len(nll); i++ {
		intNid := fd.NodeMap[nll[i].Nid]
		fd.MechanicalLoads[intNid*6+0] = nll[i].X
		fd.MechanicalLoads[intNid*6+1] = nll[i].Y
		fd.MechanicalLoads[intNid*6+2] = nll[i].Z
		fd.MechanicalLoads[intNid*6+3] = nll[i].Xx
		fd.MechanicalLoads[intNid*6+4] = nll[i].Yy
		fd.MechanicalLoads[intNid*6+5] = nll[i].Zz
	}
}

// processTemperatureLoads adds TemperatureLoad's data to a FrameData struct.
func (fd *FrameData) processTemperatureLoads(templl []*TemperatureLoad, Ml []*Material, matMap, elemMatMap map[int]int, elemTempMap map[int]float64) (err error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* 3D coord Xfrm coeffs */

	fd.ThermalLoads = make([]float64, fd.DoF())
	numTempChanges := len(templl)
	T := make([][]float64, numTempChanges)
	for i := 0; i < numTempChanges; i++ {
		eid := fd.ElemMap[templl[i].Eid]
		thermcoef, err := Ml[matMap[elemMatMap[eid]]].GetThermCoef(elemTempMap[eid])
		if err != nil {
			return err
		}
		T[i] = []float64{float64(eid), thermcoef,
			templl[i].Hy, templl[i].Hz,
			templl[i].DtyPos, templl[i].DtyNeg,
			templl[i].DtzPos, templl[i].DtzNeg}
	}

	for i := 0; i < numTempChanges; i++ { /* ! local element coordinates ! */
		n := int(T[i][0])
		a := T[i][1]
		hy := T[i][2] // 	hy, hz;			/* section dimensions in local coords */
		hz := T[i][3]

		if hy < 0 || hz < 0 {
			return errcat.AppendErrStr(nil, fmt.Sprintf("ProcessThermalLoads, in thermal load data: "+
				"section dimension < 0 Frame element number: %d  hy: %f  hz: %f", n, hy, hz))
		}

		Nx2 := a * (1.0 / 4.0) * (T[i][4] + T[i][5] + T[i][6] + T[i][7]) * fd.E[n] * fd.Ax[n]
		Nx1 := -Nx2
		Vy1, Vy2, Vz1, Vz2 := 0.0, 0.0, 0.0, 0.0
		Mx1, Mx2 := 0.0, 0.0
		My1 := (a / hz) * (T[i][7] - T[i][6]) * fd.E[n] * fd.Iy[n]
		My2 := -My1
		Mz1 := (a / hy) * (T[i][4] - T[i][5]) * fd.E[n] * fd.Iz[n]
		Mz2 := -Mz1

		n1 := fd.N1[n]
		n2 := fd.N2[n]

		transform.CoordTrans(fd.Xyz, fd.L[n], n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, fd.ElemRoll[n])

		/* {F} = [T]'{Q} */
		fd.EqFTemp[n][0] += (Nx1*t1 + Vy1*t4 + Vz1*t7)
		fd.EqFTemp[n][1] += (Nx1*t2 + Vy1*t5 + Vz1*t8)
		fd.EqFTemp[n][2] += (Nx1*t3 + Vy1*t6 + Vz1*t9)
		fd.EqFTemp[n][3] += (Mx1*t1 + My1*t4 + Mz1*t7)
		fd.EqFTemp[n][4] += (Mx1*t2 + My1*t5 + Mz1*t8)
		fd.EqFTemp[n][5] += (Mx1*t3 + My1*t6 + Mz1*t9)

		fd.EqFTemp[n][6] += (Nx2*t1 + Vy2*t4 + Vz2*t7)
		fd.EqFTemp[n][7] += (Nx2*t2 + Vy2*t5 + Vz2*t8)
		fd.EqFTemp[n][8] += (Nx2*t3 + Vy2*t6 + Vz2*t9)
		fd.EqFTemp[n][9] += (Mx2*t1 + My2*t4 + Mz2*t7)
		fd.EqFTemp[n][10] += (Mx2*t2 + My2*t5 + Mz2*t8)
		fd.EqFTemp[n][11] += (Mx2*t3 + My2*t6 + Mz2*t9)
	} /* end thermal loads	*/
	return nil
}

// processTrapezoidLoads adds TrapezoidLoad's data to a FrameData struct.
func (fd *FrameData) processTrapezoidLoads(trapll []*TrapezoidLoad) (err error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* 3D coord Xfrm coeffs */

	numTrapazoidDistLoads := len(trapll)
	W := make([][]float64, numTrapazoidDistLoads)
	for i := 0; i < numTrapazoidDistLoads; i++ {
		W[i] = []float64{float64(fd.ElemMap[trapll[i].Eid]),
			trapll[i].X1, trapll[i].X2, trapll[i].Wx1, trapll[i].Wx2,
			trapll[i].Y1, trapll[i].Y2, trapll[i].Wy1, trapll[i].Wy2,
			trapll[i].Z1, trapll[i].Z2, trapll[i].Wz1, trapll[i].Wz2}
	}

	for i := 0; i < numTrapazoidDistLoads; i++ { /* ! local element coordinates ! */
		n := int(W[i][0])
		Ln := fd.L[n]

		/* error checking */
		if W[i][3] == 0 && W[i][4] == 0 &&
			W[i][7] == 0 && W[i][8] == 0 &&
			W[i][11] == 0 && W[i][12] == 0 {
			err = errcat.AppendWarnStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, All trapezoidal "+
				"loads applied to frame element are zero, element %d , load %d", n, i))
		}

		if W[i][1] < 0 {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in x-axis trapezoidal loads,"+
				"  element %d , load %d  starting location = %f < 0",
				n, i, W[i][1]))
		}
		if W[i][1] > W[i][2] {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in x-axis trapezoidal loads,"+
				" element %d , load %d  starting location = %f > ending location = %f ",
				n, i, W[i][1], W[i][2]))
		}
		if W[i][2] > Ln {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in x-axis trapezoidal loads,"+
				" element %d , load %d ending location = %f > L (%f) ",
				n, i, W[i][2], Ln))
		}
		if W[i][5] < 0 {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in y-axis trapezoidal loads,"+
				" element %d , load %d starting location = %f < 0",
				n, i, W[i][5]))
		}
		if W[i][5] > W[i][6] {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in y-axis trapezoidal loads,"+
				" element %d , load %d starting location = %f > ending location = %f ",
				n, i, W[i][5], W[i][6]))
		}
		if W[i][6] > Ln {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in y-axis trapezoidal loads,"+
				" element %d , load %d ending location = %f > L (%f) ",
				n, i, W[i][6], Ln))
		}
		if W[i][9] < 0 {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in z-axis trapezoidal loads,"+
				" element %d , load %d starting location = %f < 0",
				n, i, W[i][9]))
		}
		if W[i][9] > W[i][10] {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in z-axis trapezoidal loads,"+
				" element %d , load %d starting location = %f > ending location = %f ",
				n, i, W[i][9], W[i][10]))
		}
		if W[i][10] > Ln {
			return errcat.AppendErrStr(err, fmt.Sprintf("ProcessTrapezoidallyDistributedLoads, in z-axis trapezoidal loads,"+
				" element %d , load %d ending location = %f > L (%f)",
				n, i, W[i][10], Ln))
		}

		var Ksy, Ksz float64
		if fd.Shear {
			Ksy = (12.0 * fd.E[n] * fd.Iz[n]) / (fd.G[n] * fd.Asy[n] * fd.Le[n] * fd.Le[n])
			Ksz = (12.0 * fd.E[n] * fd.Iy[n]) / (fd.G[n] * fd.Asz[n] * fd.Le[n] * fd.Le[n])
		}

		/* x-axis trapezoidal loads (along the frame element length) */
		x1 := W[i][1]
		x2 := W[i][2]
		w1 := W[i][3]
		w2 := W[i][4]

		Nx1 := (3.0*(w1+w2)*Ln*(x2-x1) - (2.0*w2+w1)*x2*x2 + (w2-w1)*x2*x1 + (2.0*w1+w2)*x1*x1) / (6.0 * Ln)
		Nx2 := (-(2.0*w1+w2)*x1*x1 + (2.0*w2+w1)*x2*x2 - (w2-w1)*x1*x2) / (6.0 * Ln)

		/* y-axis trapezoidal loads (across the frame element length) */
		x1 = W[i][5]
		x2 = W[i][6]
		w1 = W[i][7]
		w2 = W[i][8]

		R1o := ((2.0*w1+w2)*x1*x1 - (w1+2.0*w2)*x2*x2 +
			3.0*(w1+w2)*Ln*(x2-x1) - (w1-w2)*x1*x2) / (6.0 * Ln)
		R2o := ((w1+2.0*w2)*x2*x2 + (w1-w2)*x1*x2 -
			(2.0*w1+w2)*x1*x1) / (6.0 * Ln)

		f01 := (3.0*(w2+4.0*w1)*x1*x1*x1*x1 - 3.0*(w1+4.0*w2)*x2*x2*x2*x2 -
			15.0*(w2+3.0*w1)*Ln*x1*x1*x1 + 15.0*(w1+3.0*w2)*Ln*x2*x2*x2 -
			3.0*(w1-w2)*x1*x2*(x1*x1+x2*x2) +
			20.0*(w2+2.0*w1)*Ln*Ln*x1*x1 - 20.0*(w1+2.0*w2)*Ln*Ln*x2*x2 +
			15.0*(w1-w2)*Ln*x1*x2*(x1+x2) -
			3.0*(w1-w2)*x1*x1*x2*x2 - 20.0*(w1-w2)*Ln*Ln*x1*x2) / 360.0

		f02 := (3.0*(w2+4.0*w1)*x1*x1*x1*x1 - 3.0*(w1+4.0*w2)*x2*x2*x2*x2 -
			3.0*(w1-w2)*x1*x2*(x1*x1+x2*x2) -
			10.0*(w2+2.0*w1)*Ln*Ln*x1*x1 + 10.0*(w1+2.0*w2)*Ln*Ln*x2*x2 -
			3.0*(w1-w2)*x1*x1*x2*x2 + 10.0*(w1-w2)*Ln*Ln*x1*x2) / 360.0

		Mz1 := -(4.0*f01 + 2.0*f02 + Ksy*(f01-f02)) / (Ln * Ln * (1.0 + Ksy))
		Mz2 := -(2.0*f01 + 4.0*f02 - Ksy*(f01-f02)) / (Ln * Ln * (1.0 + Ksy))

		Vy1 := R1o + Mz1/Ln + Mz2/Ln
		Vy2 := R2o - Mz1/Ln - Mz2/Ln

		/* z-axis trapezoidal loads (across the frame element length) */
		x1 = W[i][9]
		x2 = W[i][10]
		w1 = W[i][11]
		w2 = W[i][12]

		R1o = ((2.0*w1+w2)*x1*x1 - (w1+2.0*w2)*x2*x2 +
			3.0*(w1+w2)*Ln*(x2-x1) - (w1-w2)*x1*x2) / (6.0 * Ln)
		R2o = ((w1+2.0*w2)*x2*x2 + (w1-w2)*x1*x2 -
			(2.0*w1+w2)*x1*x1) / (6.0 * Ln)

		f01 = (3.0*(w2+4.0*w1)*x1*x1*x1*x1 - 3.0*(w1+4.0*w2)*x2*x2*x2*x2 -
			15.0*(w2+3.0*w1)*Ln*x1*x1*x1 + 15.0*(w1+3.0*w2)*Ln*x2*x2*x2 -
			3.0*(w1-w2)*x1*x2*(x1*x1+x2*x2) + 20.0*(w2+2.0*w1)*Ln*Ln*x1*x1 -
			20.0*(w1+2.0*w2)*Ln*Ln*x2*x2 + 15.0*(w1-w2)*Ln*x1*x2*(x1+x2) -
			3.0*(w1-w2)*x1*x1*x2*x2 - 20.0*(w1-w2)*Ln*Ln*x1*x2) / 360.0

		f02 = (3.0*(w2+4.0*w1)*x1*x1*x1*x1 - 3.0*(w1+4.0*w2)*x2*x2*x2*x2 -
			3.0*(w1-w2)*x1*x2*(x1*x1+x2*x2) - 10.0*(w2+2.0*w1)*Ln*Ln*x1*x1 +
			10.0*(w1+2.0*w2)*Ln*Ln*x2*x2 - 3.0*(w1-w2)*x1*x1*x2*x2 +
			10.0*(w1-w2)*Ln*Ln*x1*x2) / 360.0

		My1 := (4.0*f01 + 2.0*f02 + Ksz*(f01-f02)) / (Ln * Ln * (1.0 + Ksz))
		My2 := (2.0*f01 + 4.0*f02 - Ksz*(f01-f02)) / (Ln * Ln * (1.0 + Ksz))

		Vz1 := R1o - My1/Ln - My2/Ln
		Vz2 := R2o + My1/Ln + My2/Ln

		n1 := fd.N1[n]
		n2 := fd.N2[n]

		transform.CoordTrans(fd.Xyz, Ln, n1, n2,
			&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, fd.ElemRoll[n])

		Mx1, Mx2 := 0.0, 0.0
		/* {F} = [T]'{Q} */
		fd.EqFMech[n][0] += (Nx1*t1 + Vy1*t4 + Vz1*t7)
		fd.EqFMech[n][1] += (Nx1*t2 + Vy1*t5 + Vz1*t8)
		fd.EqFMech[n][2] += (Nx1*t3 + Vy1*t6 + Vz1*t9)
		fd.EqFMech[n][3] += (Mx1*t1 + My1*t4 + Mz1*t7)
		fd.EqFMech[n][4] += (Mx1*t2 + My1*t5 + Mz1*t8)
		fd.EqFMech[n][5] += (Mx1*t3 + My1*t6 + Mz1*t9)

		fd.EqFMech[n][6] += (Nx2*t1 + Vy2*t4 + Vz2*t7)
		fd.EqFMech[n][7] += (Nx2*t2 + Vy2*t5 + Vz2*t8)
		fd.EqFMech[n][8] += (Nx2*t3 + Vy2*t6 + Vz2*t9)
		fd.EqFMech[n][9] += (Mx2*t1 + My2*t4 + Mz2*t7)
		fd.EqFMech[n][10] += (Mx2*t2 + My2*t5 + Mz2*t8)
		fd.EqFMech[n][11] += (Mx2*t3 + My2*t6 + Mz2*t9)

	} /* end trapezoidally distributed loads */
	return nil
}

// processUniformLoads adds UniformLoad's data to a FrameData struct.
func (fd *FrameData) processUniformLoads(ull []*UniformLoad) (err error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* 3D coord Xfrm coeffs */

	numUniformDistLoads := len(ull)
	U := make([][]float64, numUniformDistLoads)
	for i := 0; i < numUniformDistLoads; i++ {
		U[i] = []float64{float64(fd.ElemMap[ull[i].Eid]), ull[i].Ux, ull[i].Uy, ull[i].Uz}
	}

	for i := 0; i < numUniformDistLoads; i++ { /* ! local element coordinates ! */
		if U[i][1] == 0 && U[i][2] == 0 && U[i][3] == 0 {
			return errcat.AppendWarnStr(nil, fmt.Sprintf("ProcessUniformlyDistributedLoads, All distributed loads "+
				"applied to frame element %d are zero", i))
		}
		n := int(U[i][0])

		Nx2 := U[i][1] * fd.Le[n] / 2.0
		Nx1 := Nx2
		Vy2 := U[i][2] * fd.Le[n] / 2.0
		Vy1 := Vy2
		Vz2 := U[i][3] * fd.Le[n] / 2.0
		Vz1 := Vz2
		Mx1, Mx2 := 0.0, 0.0
		My1 := -U[i][3] * fd.Le[n] * fd.Le[n] / 12.0
		My2 := -My1
		Mz1 := U[i][2] * fd.Le[n] * fd.Le[n] / 12.0
		Mz2 := -Mz1

		n1 := fd.N1[n]
		n2 := fd.N2[n]

		transform.CoordTrans(fd.Xyz, fd.L[n], n1, n2,
			&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, fd.ElemRoll[n])

		/* {F} = [T]'{Q} */
		fd.EqFMech[n][0] += (Nx1*t1 + Vy1*t4 + Vz1*t7)
		fd.EqFMech[n][1] += (Nx1*t2 + Vy1*t5 + Vz1*t8)
		fd.EqFMech[n][2] += (Nx1*t3 + Vy1*t6 + Vz1*t9)
		fd.EqFMech[n][3] += (Mx1*t1 + My1*t4 + Mz1*t7)
		fd.EqFMech[n][4] += (Mx1*t2 + My1*t5 + Mz1*t8)
		fd.EqFMech[n][5] += (Mx1*t3 + My1*t6 + Mz1*t9)

		fd.EqFMech[n][6] += (Nx2*t1 + Vy2*t4 + Vz2*t7)
		fd.EqFMech[n][7] += (Nx2*t2 + Vy2*t5 + Vz2*t8)
		fd.EqFMech[n][8] += (Nx2*t3 + Vy2*t6 + Vz2*t9)
		fd.EqFMech[n][9] += (Mx2*t1 + My2*t4 + Mz2*t7)
		fd.EqFMech[n][10] += (Mx2*t2 + My2*t5 + Mz2*t8)
		fd.EqFMech[n][11] += (Mx2*t3 + My2*t6 + Mz2*t9)

	} /* end uniformly distributed loads */
	return nil
}
