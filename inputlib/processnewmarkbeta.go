package inputlib

// processNewmarkBetaParams
func (fd *FrameData) processNewmarkBetaParams(params *NewmarkBetaParam) {
	fd.PerformNewmarkBetaAnalysis = true
	fd.NBAlpha = params.Alpha
	fd.NBBeta = params.Beta
	fd.NBDt = params.Dt
	fd.NBTermTime = params.TerminationTime
}

// processNodalVelocities adds nodal velocities data to a FrameData struct.
func (fd *FrameData) processNodalVelocities(vi []*NodalVelocity) (err error) {
	fd.Vi = make([]float64, fd.DoF())
	for i := 0; i < len(vi); i++ {
		intNid := fd.NodeMap[vi[i].Nid]
		fd.Vi[intNid*6+0] = vi[i].Vx
		fd.Vi[intNid*6+1] = vi[i].Vy
		fd.Vi[intNid*6+2] = vi[i].Vz
		fd.Vi[intNid*6+3] = vi[i].Vxx
		fd.Vi[intNid*6+4] = vi[i].Vyy
		fd.Vi[intNid*6+5] = vi[i].Vzz
	}
	return nil
}

// processNodalAccelerations adds nodal acceleration data to a FrameData struct.
func (fd *FrameData) processNodalAccelerations(ai []*NodalAcceleration) (err error) {
	fd.Ai = make([]float64, fd.DoF())
	for i := 0; i < len(ai); i++ {
		intNid := fd.NodeMap[ai[i].Nid]
		fd.Ai[intNid*6+0] = ai[i].Ax
		fd.Ai[intNid*6+1] = ai[i].Ay
		fd.Ai[intNid*6+2] = ai[i].Az
		fd.Ai[intNid*6+3] = ai[i].Axx
		fd.Ai[intNid*6+4] = ai[i].Ayy
		fd.Ai[intNid*6+5] = ai[i].Azz
	}
	return nil
}
