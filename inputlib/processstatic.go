/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/csvrec"
	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
)

// processStaticParams adds StaticParams data to a FrameData struct.
func (fd *FrameData) processStaticParams(sp *StaticParam) (defTemp float64) {
	fd.Shear = sp.Shear
	fd.Geom = sp.Geom
	fd.StaticTol = sp.Tol
	return sp.Temp
}

// processNodes adds Node's data to a FrameData struct.
func (fd *FrameData) processNodes(nl []*Node, connectedNodeMap map[int]bool) (nodeMap map[int]int) {
	fd.NumNodes = len(connectedNodeMap)
	fd.Xyz = make([]femath.Vec3, fd.NumNodes)
	fd.NodeRadius = make([]float64, fd.NumNodes)
	nodeMap = make(map[int]int)

	for i, j := 0, 0; j < len(nl); j++ {
		if _, ok := connectedNodeMap[nl[j].ID]; ok {
			fd.Xyz[i] = femath.Vec3{X: nl[j].Loc[0], Y: nl[j].Loc[1], Z: nl[j].Loc[2]}
			fd.NodeRadius[i] = nl[j].Rad
			nodeMap[nl[j].ID] = i
			i++
		}
	}
	return nodeMap
}

// processReactions adds Reaction's data to a FrameData struct.
func (fd *FrameData) processReactions(rl []*Reaction) (err error) {
	numRestrainedNodes := len(rl)
	fd.ReactedDof = make([]bool, fd.DoF())
	fd.UnreactedDof = make([]bool, fd.DoF())
	for i := 0; i < fd.DoF(); i++ {
		fd.UnreactedDof[i] = true
	}

	if fd.PerformStaticAnalysis && (numRestrainedNodes < 1 || numRestrainedNodes > fd.DoF()/6) {
		return errcat.AppendErrStr(err,
			fmt.Sprintf("ProcessReactions, number of restrained nodes = %d, should be between 1 and %d", numRestrainedNodes, fd.DoF()/6))
	}

	numReactions := 0
	for i := 0; i < numRestrainedNodes; i++ {
		reacts := []bool{rl[i].X, rl[i].Y, rl[i].Z, rl[i].Xx, rl[i].Yy, rl[i].Zz}
		intNid := fd.NodeMap[rl[i].Nid]
		for j := 0; j < 6; j++ {
			indx := intNid*6 + j
			fd.ReactedDof[indx] = reacts[j]
			fd.UnreactedDof[indx] = !reacts[j]
			if reacts[j] {
				numReactions++
			}
		}
	}

	if fd.PerformStaticAnalysis && (numReactions < 4) {
		return errcat.AppendWarnStr(err,
			fmt.Sprintf("ProcessReactions, Un-restrained structure: %d imposed reactions. "+
				"At least 4 reactions are required to support static loads.", numReactions))
	}

	return nil
}

// processElements adds Element's data to a FrameData struct.
func (fd *FrameData) processElements(el []*Element, Nl []*Node, unconNodeMap map[int]int) (elemMap, elemMatMap map[int]int, elemTempMap map[int]float64, err error) {
	fd.NumElems = len(el)
	fd.N1 = make([]int, fd.NumElems)
	fd.N2 = make([]int, fd.NumElems)
	fd.Ax = make([]float64, fd.NumElems)
	fd.Asy = make([]float64, fd.NumElems)
	fd.Asz = make([]float64, fd.NumElems)
	fd.Jx = make([]float64, fd.NumElems)
	fd.Iy = make([]float64, fd.NumElems)
	fd.Iz = make([]float64, fd.NumElems)
	fd.ElemRoll = make([]float64, fd.NumElems)
	elemMap = make(map[int]int)
	elemMatMap = make(map[int]int)
	elemTempMap = make(map[int]float64)

	for i := 0; i < fd.NumElems; i++ {
		fd.N1[i] = fd.NodeMap[el[i].Nid1]
		fd.N2[i] = fd.NodeMap[el[i].Nid2]
		fd.Ax[i] = el[i].Ax
		fd.Asy[i] = el[i].Asy
		fd.Asz[i] = el[i].Asz
		fd.Jx[i] = el[i].Jx
		fd.Iy[i] = el[i].Iy
		fd.Iz[i] = el[i].Iz
		N3 := unconNodeMap[el[i].Nid3]
		fd.ElemRoll[i] = (math.Pi / 180) * calcRollAngle(fd.Xyz[fd.N1[i]], fd.Xyz[fd.N2[i]], femath.Vec3{X: Nl[N3].Loc[0], Y: Nl[N3].Loc[1], Z: Nl[N3].Loc[2]})
		// E, G, and density are set in the material section
		elemMap[el[i].ID] = i
		elemMatMap[i] = el[i].Mid
		elemTempMap[i] = el[i].Temp
		if fd.Ax[i] < 0 || fd.Asy[i] < 0 || fd.Asz[i] < 0 ||
			fd.Jx[i] < 0 || fd.Iy[i] < 0 || fd.Iz[i] < 0 {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, in frame element property data: section property < 0"+
				"Frame element number: %d", i))
		}
		if fd.Ax[i] == 0 {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, in frame element property data: "+
				"cross section area is zero Frame element number: %d", i))
		}

		if fd.Jx[i] == 0 {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, in frame element property data: "+
				"torsional moment of inertia is zero Frame element number: %d", i))
		}
		if fd.Iy[i] == 0 || fd.Iz[i] == 0 {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, cross section bending moment of "+
				"inertia is zero Frame element number: %d", i))
		}
	}
	fd.L = make([]float64, fd.NumElems)  /* length of each element		*/
	fd.Le = make([]float64, fd.NumElems) /* effective length of each element	*/
	for i := 0; i < fd.NumElems; i++ {
		fd.L[i] = femath.ElemLength(fd.Xyz[fd.N1[i]], fd.Xyz[fd.N2[i]])
		fd.Le[i] = fd.L[i] - fd.NodeRadius[fd.N1[i]] - fd.NodeRadius[fd.N2[i]]
		if fd.N1[i] == fd.N2[i] || fd.L[i] == float64(0.0) {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, Frame elements must start and stop "+
				"at different nodes, frame element %d  N1= %d N2= %d L= %e", i, fd.N1[i], fd.N2[i], fd.L[i]))
		}
		if fd.Le[i] <= 0.0 {
			return elemMap, elemMatMap, elemTempMap, errcat.AppendErrStr(err, fmt.Sprintf("ProcessFrameData, Node  radii are too large."+
				"frame element %d  N1= %d N2= %d L= %e r1= %e r2= %e Le= %e \n",
				i, fd.N1[i], fd.N2[i], fd.L[i], fd.NodeRadius[fd.N1[i]], fd.NodeRadius[fd.N2[i]], fd.Le[i]))
		}
	}
	return elemMap, elemMatMap, elemTempMap, nil
}

// Process Materials uses material input data to cacluate the Youngs and Shear moduli as well as the density for each elament.
func (fd *FrameData) processMaterials(ml []*Material, El []*Element,
	defaultTemperature float64) (matMap map[int]int, err error) {

	matMap = make(map[int]int)
	for i, mat := range ml {
		matMap[mat.ID] = i
	}

	fd.E = make([]float64, fd.NumElems)
	fd.G = make([]float64, fd.NumElems)
	fd.ElemDensity = make([]float64, fd.NumElems)

	// set element material properties G and density
	for i := 0; i < fd.NumElems; i++ {
		mat := ml[matMap[El[i].Mid]]
		fd.E[i], fd.G[i], fd.ElemDensity[i], err = mat.getElementValues(El[i].Temp, defaultTemperature)

		if fd.E[i] <= 0 || fd.G[i] <= 0 {
			return matMap, errcat.AppendErrStr(err,
				fmt.Sprintf("ProcessFrameData, Frame element: %d, Material elastic modulus E (%s) or G (%s) "+
					"is not positive ", i, csvrec.Flt2Str(fd.E[i]), csvrec.Flt2Str(fd.G[i])))
		}

		if fd.ElemDensity[i] <= 0 {
			return matMap, errcat.AppendErrStr(err,
				fmt.Sprintf("ProcessFrameData, Frame element: %d, Mass density d (%s) is not positive ",
					i, csvrec.Flt2Str(fd.ElemDensity[i])))
		}

		if (fd.Asy[i] == 0 || fd.Asz[i] == 0) && fd.G[i] == 0 {
			return matMap, errcat.AppendErrStr(nil,
				fmt.Sprintf("ProcessFrameData, Frame element: %d, "+
					"a shear area (Asy: %s, Asz: %s) and shear modulus (G: %s) are zero ",
					i, csvrec.Flt2Str(fd.Asy[i]), csvrec.Flt2Str(fd.Asz[i]), csvrec.Flt2Str(fd.G[i])))
		}
	}

	return matMap, nil
}
