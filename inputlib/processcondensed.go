/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

// processDynamicCondensationNodes adds DynamicCondensationModes data to a FrameData struct.
func (fd *FrameData) processCondensationParams(cp *CondensationParam) {
	fd.PerformCondensation = false
	if cp != nil {
		if cp.CondensationMethod > 0 && cp.CondensationMethod < 4 {
			fd.PerformCondensation = true
			fd.CondensationMethod = cp.CondensationMethod
		}
	}
}

// processCondensedNodes adds CondensedNode data to a FrameData struct.
func (fd *FrameData) processCondensedNodes(cnl []*CondensedNode) {
	if fd.PerformCondensation {
		fd.Cdof = 0
		fd.CondensationData = make([]int, fd.DoF())
		for _, cn := range cnl {
			id := fd.NodeMap[cn.Nid]
			dofs := []bool{cn.X, cn.Y, cn.Z, cn.Xx, cn.Yy, cn.Zz}
			for j := 0; j < 6; j++ {
				if dofs[j] {
					fd.CondensationData[fd.Cdof] = id*6 + j
					fd.Cdof++
				}
			}
		}
	}
}

// processDynamicCondensationNodes adds DynamicCondensationModes data to a FrameData struct.
func (fd *FrameData) processDynamicCondensationModes(dcm *CondensedMode) {
	if fd.PerformCondensation {
		if fd.CondensationMethod > 1 {
			fd.CondensedModeNums = dcm.Modes
		}
	}
}
