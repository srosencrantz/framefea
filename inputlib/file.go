/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

const fltFormat string = "%0.20g"

// FileSuffix specifies the filename suffix to be used for files of this type
const FileSuffix = ".ffin"

// Description contains the framefea input data as read (or to be written) from the file.
type Description struct {
	Filename           string
	Docs               []*Doc               `csvrecRec:"Doc"`
	StaticParams       []*StaticParam       `csvrecRec:"StaticParams"` /*Sp*/
	Nodes              []*Node              `csvrecRec:"Node"`         /*Nl*/
	Reactions          []*Reaction          `csvrecRec:"Reaction"`     /*Rl*/
	Elements           []*Element           `csvrecRec:"Element"`      /*El*/
	Materials          []*Material          `csvrecRec:"Material"`     /*Ml*/
	Curves             []*Curve             `csvrecRec:"Curve"`
	BodyLoads          []*BodyLoad          `csvrecRec:"BodyLoad"`          /*Bll*/
	ElementPointLoads  []*ElementPointLoad  `csvrecRec:"ElementPointLoad"`  /*Epll*/
	NodalDisplacements []*NodalDisplacement `csvrecRec:"NodalDisplacement"` /*Ndl*/
	NodalVelocities    []*NodalVelocity     `csvrecRec:"NodalVelocity"`
	NodalAccelerations []*NodalAcceleration `csvrecRec:"NodalAcceleration"`
	NodalLoads         []*NodalLoad         `csvrecRec:"NodalLoad"`          /*Nll*/
	TemperatureLoads   []*TemperatureLoad   `csvrecRec:"TemperatureLoad"`    /*Templl*/
	TrapezoidLoads     []*TrapezoidLoad     `csvrecRec:"TrapezoidLoad"`      /*Trapll*/
	UniformLoads       []*UniformLoad       `csvrecRec:"UniformLoad"`        /*Ull*/
	ModalParams        []*ModalParam        `csvrecRec:"ModalParams"`        /*Mp*/
	NodalMasses        []*NodalMass         `csvrecRec:"NodalMass"`          /*Nml*/
	ElementMasses      []*ElementMass       `csvrecRec:"ElementMass"`        /*Eml*/
	CondensationParams []*CondensationParam `csvrecRec:"CondensationParams"` /*Cp*/
	CondensedNodes     []*CondensedNode     `csvrecRec:"CondensedNode"`      /*Cnl*/
	CondensedModes     []*CondensedMode     `csvrecRec:"CondensedModes"`     /*Dcm*/
	NewmarkBetaParams  []*NewmarkBetaParam  `csvrecRec:"NewmarkBetaParams"`
}

//Doc describes a Documentation record.
type Doc struct {
	Text string `csvrecField:"cstr"` // Doc name
}

// StaticParam contains parameters necessary for a static analysis.
type StaticParam struct {
	Shear bool    `csvrecField:"bool"` // true=Do, false=Don't include shear deformation effects
	Geom  bool    `csvrecField:"bool"` // true=Do, false=Don't include geometric stiffness effects
	Tol   float64 `csvrecField:"flt"`  // Convergence tolerance for the static analysis when Geom == true
	Temp  float64 `csvrecField:"flt"`  // this is the default temperature of elements that do not have a temperature specified by a load  (for material property purposes)
}

//Node describes a node in 3d space.
type Node struct {
	ID  int        `csvrecField:"int"`   // node id
	Loc [3]float64 `csvrecField:"f3vec"` // node x, y, z
	Rad float64    `csvrecField:"flt"`   // nodal radius
}

// Reaction descripes the constrained degrees of freedom of a node.
type Reaction struct {
	Nid        int  `csvrecField:"int"`  // node id
	X, Y, Z    bool `csvrecField:"bool"` // Position reactions 0 = free, 1 = fixed.
	Xx, Yy, Zz bool `csvrecField:"bool"` // Rotation reactions 0 = free, 1 = fixed.
}

// Element defines the properties and orientation of a frame element.
type Element struct {
	ID       int     `csvrecField:"int"` // element id
	Nid1     int     `csvrecField:"int"` // element nodes
	Nid2     int     `csvrecField:"int"` // element nodes
	Nid3     int     `csvrecField:"int"` // beam orientation node as in lsdyna
	Mid      int     `csvrecField:"int"` // material id
	Ax       float64 `csvrecField:"flt"` // Cross sectional area
	Asy, Asz float64 `csvrecField:"flt"` // Shear area in the y and z directions
	Jx       float64 `csvrecField:"flt"` // Torsional moment of inertia of a frame element
	Iy, Iz   float64 `csvrecField:"flt"` // Moment of inertia for bending about the local y and z axis
	Temp     float64 `csvrecField:"flt"` // Temperature of the Element
}

// A Material defines temperature dependent material properties and failure criteria.
type Material struct {
	ID            int     `csvrecField:"int"` // id
	Name          string  `csvrecField:"str"` // name
	Rho           float64 `csvrecField:"flt"` // density
	YoungsStr     string  `csvrecField:"str"`
	ThermCoefStr  string  `csvrecField:"str"`
	PoissonStr    string  `csvrecField:"str"`
	Youngs        float64 // Youngs modulous
	ThermCoef     float64 // Coefficient of thermal expansion
	Poisson       float64 // Poisson's Ratio
	YoungsCurv    *Curve  // pointer to Youngs Modulus vs. temperature curve
	ThermCoefCurv *Curve  // pointer to thermal coefficient vs. temperature curve
	PoissonCurv   *Curve  // pointer to Poisson's ratio vs. temperature curve
}

// Curve is a slice of CurvePt
type Curve struct {
	ID     int          `csvrecField:"int"`       // curve id
	Points [][2]float64 `csvrecField:"fpairlist"` // curve point list Points[0][0] is the x of point zero Points[0][1] is the y of point zero.
}

// BodyLoad describes a body load (like gravity) that will be applied to the whole model.
type BodyLoad struct {
	ID         int     `csvrecField:"int"` // bodyload id
	Gx, Gy, Gz float64 `csvrecField:"flt"` // Acceleration in the x, y, and z directions
}

// ElementPointLoad describes a point load somewhere on and element.
type ElementPointLoad struct {
	ID      int     `csvrecField:"int"` // load id
	Eid     int     `csvrecField:"int"` // node id
	X, Y, Z float64 `csvrecField:"flt"` // node x, y, z
	Pos     float64 `csvrecField:"flt"` // nodal radius
}

// NodalDisplacement describes a prescribed displacement of a node.
type NodalDisplacement struct {
	ID            int     `csvrecField:"int"` // load id
	Nid           int     `csvrecField:"int"` // node id
	Dx, Dy, Dz    float64 `csvrecField:"flt"` // Displacements in the x, y, and z directions
	Dxx, Dyy, Dzz float64 `csvrecField:"flt"` // Rotations about the x, y, and z axis
}

// NodalLoad describes a load/moment applied at a particular node.
type NodalLoad struct {
	ID         int     `csvrecField:"int"` // load id
	Nid        int     `csvrecField:"int"` // node id
	X, Y, Z    float64 `csvrecField:"flt"` // Applied loads in the x, y, z directions
	Xx, Yy, Zz float64 `csvrecField:"flt"` // Applied moments about the x, y, z axis
}

// TemperatureLoad describes a temperature gradiant applied to an element.
type TemperatureLoad struct {
	ID             int     `csvrecField:"int"` // load id
	Eid            int     `csvrecField:"int"` // node id
	Hy, Hz         float64 `csvrecField:"flt"` // y and z depth
	DtyPos, DtyNeg float64 `csvrecField:"flt"` // the temperature change on the local +/-y face of the element.
	DtzPos, DtzNeg float64 `csvrecField:"flt"` // the temperature change on the local +/-z face of the element.
}

// TrapezoidLoad describes a trapezoidal load applied to an element.
type TrapezoidLoad struct {
	ID       int     `csvrecField:"int"` // load id
	Eid      int     `csvrecField:"int"` // element id
	X1, X2   float64 `csvrecField:"flt"` // start and stop location in the local x-direction
	Wx1, Wx2 float64 `csvrecField:"flt"` // start and stop load in the local x-direction
	Y1, Y2   float64 `csvrecField:"flt"` // start and stop location in the local y-direction
	Wy1, Wy2 float64 `csvrecField:"flt"` // start and stop load in the local y-direction
	Z1, Z2   float64 `csvrecField:"flt"` // start and stop location in the local z-direction
	Wz1, Wz2 float64 `csvrecField:"flt"` // start and stop load in the local z-direction
}

// UniformLoad describes a uniform load applied to an element.
type UniformLoad struct {
	ID         int     `csvrecField:"int"` // load id
	Eid        int     `csvrecField:"int"` // Element id
	Ux, Uy, Uz float64 `csvrecField:"flt"` // Uniformly distributed load in the x, y, and z dirs
}

// ModalParam contains parameters necessary for a modal analysis.
type ModalParam struct {
	NumModes       int     `csvrecField:"int"`  // number of desired modes
	ModalMethod    int     `csvrecField:"int"`  // modal analysis method: 1=Jacobi-Subspace or 2=Stodola
	LumpFlag       bool    `csvrecField:"bool"` // true: lumped mass matrix or false: consistent mass matrix
	ModalTol       float64 `csvrecField:"flt"`  // convergence tolerance for modal analysis
	ModalFreqShift float64 `csvrecField:"flt"`  // modal frequency shift for unrestrained structures, shift-factor for rigid-body-modes
}

// NodalMass describes extra mass and/or inertia applied to a node.
type NodalMass struct {
	ID       int     `csvrecField:"int"` // node id
	Mass     float64 `csvrecField:"flt"` // node mass for each node
	InertiaX float64 `csvrecField:"flt"` // node inertia about global X axis
	InertiaY float64 `csvrecField:"flt"` // node inertia about global Y axis
	InertiaZ float64 `csvrecField:"flt"` // node inertia about global Z axis
}

// ElementMass describes a mass to be added to an element for use in modal analysis.
type ElementMass struct {
	ID   int     `csvrecField:"int"` // Element ID
	Mass float64 `csvrecField:"flt"` // Extra frame element mass
}

// CondensationParam contains the parameters necessary for matrix condensation.
type CondensationParam struct {
	CondensationMethod int `csvrecField:"int"` // matrix condensation method: 0, 1, 2, or 3
}

// CondensedNode contains the condensed degrees of freedom for a node.
type CondensedNode struct {
	Nid        int  `csvrecField:"int"`  // node id
	X, Y, Z    bool `csvrecField:"bool"` // Position reactions 0 = free, 1 = fixed.
	Xx, Yy, Zz bool `csvrecField:"bool"` // Rotation reactions 0 = free, 1 = fixed.
}

// CondensedMode contains a vector of the condensed mode numbers
type CondensedMode struct {
	Modes []int `csvrecField:"intlist"` // condensation mode numbers
}

// NewmarkBetaParam performs implicit time integration on the model
type NewmarkBetaParam struct {
	Dt              float64 `csvrecField:"flt"` // desired time step.
	Alpha           float64 `csvrecField:"flt"` // parameter chosen to control the stability of the solution
	Beta            float64 `csvrecField:"flt"` // parameter chosen to control the stability of the solution
	TerminationTime float64 `csvrecField:"flt"` // parameter that specifies a termination time for a time integration analysis with multiple steps. If 0 is specified then only one time step of durration Dt will be run. I TerminationTime is > 0 then multiple timesteps will be run, where the resulting accelerations, velocities, and displacements will be fed into the next simulation, until the simulation time exceeds the TerminationTime.
}

// NodalVelocity describes an initial velocity of a node.
type NodalVelocity struct {
	ID            int     `csvrecField:"int"` // load id
	Nid           int     `csvrecField:"int"` // node id
	Vx, Vy, Vz    float64 `csvrecField:"flt"` // Velocities in the x, y, and z directions
	Vxx, Vyy, Vzz float64 `csvrecField:"flt"` // rotational velocities about the x, y, and z axis
}

// NodalAcceleration describes an initial acceleration of a node.
type NodalAcceleration struct {
	ID            int     `csvrecField:"int"` // load id
	Nid           int     `csvrecField:"int"` // node id
	Ax, Ay, Az    float64 `csvrecField:"flt"` // Accelerations in the x, y, and z directions
	Axx, Ayy, Azz float64 `csvrecField:"flt"` // rotational acceleration about the x, y, and z axis
}
