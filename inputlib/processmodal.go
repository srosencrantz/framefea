/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

// processModalParams adds ModalParams data to a FrameData struct.
func (fd *FrameData) processModalParams(mp *ModalParam) {
	//	fd.PerformModalAnalysis = false
	if mp.NumModes > 0 {
		//		fd.PerformModalAnalysis = true
		fd.NumModes = mp.NumModes
		fd.ModalMethod = mp.ModalMethod
		fd.LumpFlag = mp.LumpFlag
		fd.ModalTol = mp.ModalTol
		fd.ModalFreqShift = mp.ModalFreqShift
	}
}

// processNodalMasss adds NodalMass data to a FrameData struct.
func (fd *FrameData) processNodalMasss(nml []*NodalMass) {
	//	if fd.PerformModalAnalysis {
	fd.NMs = make([]float64, fd.NumNodes)
	fd.NMx = make([]float64, fd.NumNodes)
	fd.NMy = make([]float64, fd.NumNodes)
	fd.NMz = make([]float64, fd.NumNodes)
	for _, nm := range nml {
		ID := fd.NodeMap[nm.ID]
		fd.NMs[ID] = nm.Mass
		fd.NMx[ID] = nm.InertiaX
		fd.NMy[ID] = nm.InertiaY
		fd.NMz[ID] = nm.InertiaZ
	}
	//	}
}

// processElementMasss adds ElementMass data to a FrameData struct.
func (fd *FrameData) processElementMasss(eml []*ElementMass) {
	//	if fd.PerformModalAnalysis {
	fd.EMs = make([]float64, fd.NumElems)
	for _, em := range eml {
		ID := fd.ElemMap[em.ID]
		fd.EMs[ID] = em.Mass
	}
	//	}
}
