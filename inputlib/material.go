/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"fmt"
	"strings"

	"bitbucket.org/srosencrantz/csvrec"
	"bitbucket.org/srosencrantz/errcat"
)

func readCF(str string, curveMap map[int]*Curve, err error) (val float64, curve *Curve) {
	cstr := strings.ToUpper(strings.TrimSpace(str))
	if strings.HasPrefix(cstr, "T") {
		curveID := csvrec.ReadInt(cstr[1:], &err)
		if _, ok := curveMap[curveID]; ok {
			return float64(-1), curveMap[curveID]
		}
		err = errcat.AppendErrStr(err, fmt.Sprintf("Material record refers to curve %d that doesn't exist, %v", curveID, str))
		return float64(-1), curve
	}
	return csvrec.ReadFlt(cstr, &err), curve
}

func writeCF(val float64, curve *Curve) (str string) {
	if val >= 0 {
		return csvrec.Flt2Str(val)
	}
	return fmt.Sprintf("T%d", curve.ID)
}

// GetYoungs returns the Young's Modulus at the temperature given.
func (mat Material) GetYoungs(temp float64) (val float64, err error) {
	if mat.YoungsCurv == nil {
		return mat.Youngs, nil
	}
	val, err = mat.YoungsCurv.GetYVal(temp)
	return val, err
}

// GetThermCoef returns the Thermal Coefficient at the temperature given.
func (mat Material) GetThermCoef(temp float64) (val float64, err error) {
	if mat.ThermCoefCurv == nil {
		return mat.ThermCoef, nil
	}
	val, err = mat.ThermCoefCurv.GetYVal(temp)
	return val, err
}

// GetPoisson returns the Poisson's Ratio at the temperature given.
func (mat Material) GetPoisson(temp float64) (val float64, err error) {
	if mat.PoissonCurv == nil {
		return mat.Poisson, nil
	}
	val, err = mat.PoissonCurv.GetYVal(temp)
	return val, err
}

func (mat Material) getElementValues(elemTemp, defTemp float64) (E, G, rho float64, err error) {
	// temperature
	temp := elemTemp
	if temp <= 0 {
		temp = defTemp
	}

	//young's modulus
	E, err = mat.GetYoungs(temp)
	if err != nil {
		return 0, 0, 0, err
	}

	//poisson's ratio
	var nu float64
	nu, err = mat.GetPoisson(temp)
	if err != nil {
		return 0, 0, 0, err
	}

	//shear modulus
	G = E / (float64(2.0) * (float64(1.0) + nu))

	//density
	rho = mat.Rho

	return E, G, rho, nil
}
