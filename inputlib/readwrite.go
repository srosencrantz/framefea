/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package inputlib

import (
	"fmt"
	"sort"
	"time"

	"bitbucket.org/srosencrantz/csvrec"
	"bitbucket.org/srosencrantz/framefea/doc"
)

// ReadFile reads an framefea file into a Description and converts it to FrameData.
func ReadFile(fileName string) (frameData *FrameData, inDataPtr *Description, err error) {
	inData := csvrec.ParseFile(fileName, Description{}, &err).(Description)
	if err != nil {
		return nil, &inData, err
	}
	inData.Filename = fileName

	if len(inData.StaticParams) != 1 {
		return frameData, &inData, fmt.Errorf("Exactly one StaticParams record is required")
	}

	if len(inData.BodyLoads) > 1 {
		return frameData, &inData, fmt.Errorf("Only one BodyLoad record is allowed")
	}

	if len(inData.ModalParams) > 1 {
		return frameData, &inData, fmt.Errorf("Only one ModalParams record is allowed")
	}

	if len(inData.CondensationParams) > 1 {
		return frameData, &inData, fmt.Errorf("Only one CondensationParams record is allowed")
	}

	if len(inData.CondensedModes) > 1 {
		return frameData, &inData, fmt.Errorf("Only one CondensedMode record is allowed")
	}

	if len(inData.CondensationParams) == 1 && len(inData.CondensedModes) != 1 {
		return frameData, &inData, fmt.Errorf("When performing a Dynamic Condensation exactly one CondensedMode record is required")
	}

	if len(inData.NewmarkBetaParams) > 1 {
		return frameData, &inData, fmt.Errorf("Only one NewMarkBetaParam record is allowed")
	}

	// make a curve map and make sure the points are sorted by the x vals
	curveMap := make(map[int]*Curve)
	for _, curve := range inData.Curves {
		sort.Sort(pointList(curve.Points))
		curveMap[curve.ID] = curve
	}

	for _, mat := range inData.Materials {
		mat.Youngs, mat.YoungsCurv = readCF(mat.YoungsStr, curveMap, err)
		mat.ThermCoef, mat.ThermCoefCurv = readCF(mat.ThermCoefStr, curveMap, err)
		mat.Poisson, mat.PoissonCurv = readCF(mat.PoissonStr, curveMap, err)
	}
	frameData, err = CreateFrameData(&inData)

	return frameData, &inData, err
}

// WriteFile writes a Description struct to a file.
func (inData *Description) WriteFile(fileName, toolName string) error {

	inData.Docs = append(inData.Docs,
		&Doc{Text: fmt.Sprintf("Written by: %s on %s from %s",
			toolName, time.Now().Format("2 Jan 2006, 15:04:05 -MST"), doc.GetVersion())})

	for _, mat := range inData.Materials {
		mat.YoungsStr = writeCF(mat.Youngs, mat.YoungsCurv)
		mat.ThermCoefStr = writeCF(mat.ThermCoef, mat.ThermCoefCurv)
		mat.PoissonStr = writeCF(mat.Poisson, mat.PoissonCurv)
	}

	return csvrec.CreateFile(fileName, FileSuffix, *inData)
}
