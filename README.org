Framefea: 3D frame finite element analysis software 

* Project Summary 
Framefea is a standalone program and library for the static and dynamic structural analysis of
three-dimensional frames and trusses with elastic and geometric stiffness. The standalone program
reads an input file containing nodes, elements, materials, displacements, loads, and mass information (if a modal analysis is to be carried out). It then performs an analysis and writes results to an output file that contains the resulting forces, displacements, reactions, and element forces.

** Current Version
Current Version: Tue Jun 26 12:37:17 EDT 2018

** Contact Information
Stephen Rosencrantz
mailto:srosencrantz@gmail.com

*** Frame3dd Contact Info 
[[http://frame3dd.sourceforge.net/][Frame3dd Website]]
(c) 1992-2008 Henri Gavin - Associate Professor
Department of Civil and Environmental Engineering
Edmund T. Pratt School of Engineering
Duke University - Box 90287, Durham, NC 27708-0287
mailto:Henri.Gavin@Duke.edu - tel: 919-660-5201 - fax: 919-660-5219 

** History and Frame3dd 
Framefea began as modifications to the frame3dd that was created by Henri Gavin that ended in a complete rewrite that included:

- Conversion of floats to 64 bit 
- Rewrite is in the GO programming language.
- Tests were added for nearly every function of the code. 
- The input file was changed to a csv record format with labels, using my csvrec library, and is more compatible with LS-DYNA's input format.
- I have also added temperature dependent materials that are specified independent of the elements.
- The standalone framefea code will run a single load case. 
- There is a single output file that contains forces, displacements, reactions, and element forces.
- There is a tool to convert (mostly) a fram3dd input file with a single load case to a framefea input file.
  
** Required Software
- git [[https://git-scm.com][Official Website]]
- golang: [[https://golang.org/doc/install][Official Website]], 
[[https://golang.org/doc/install][Installation Instructions]],
[[https://golang.org/doc/code.html][Introduction to GO]]

** Download
To download framefea type `go get bitbucket.org/srosencrantz/framefea`. 
This will clone framefea to $GOPATH/src/bitbucket/srosencrantz/framefea (where $GOPATH is usually yourHomeDir/go).

** FrameFEA Executable
*** Compiling
Go to the framefea directory and run the install.sh script there. The script will create a framefea/bin directory with executables for Linux, Windows, and Mac OS.  It will also create an executable in the $GOBIN/ folder.

*** Command Line Options
- "-in=<name.ffea>"  (required)
- "-out=<name>"  (required)
- "-V" if set additional debug infromation will be printed (optional)
 -  -V=0, off;  -V=1, include mode shapes;  -V=2, Stiffness & Mass matricies as Goland literal [][]float64
- "-cpuprofile <name.prof>" write cpu profiling data to file (optional)

**** Usage
#+begin_example
framefea -in <name.ffea> -out <name>
framefea -in <name.ffea> -out <name> -V <num> -cpuprofile <name>
#+end_example

** File Formats
*** Input File [[file:./inputlib/README.org][Link]]
    
    The input file describes the problem to be simulated with FrameFEA.
*** Output File [[file:./outputlib/README.org][Link]]

    The FFEAOutput file describes the results of a FrameFEA simulation.

** Programs
*** Condensation [[file:./condensation/README.org][Link]]

    Package condensation creates reduce order stiffness and mass matrices.

*** Modal [[file:./modal/README.org][Link]]

    Package modal performs dynamic analysis. FrameFEA analysis will optionally include a dynamic analysis for natural modes of vibration.

*** Newmark-Beta [[file:./newmarkbeta/README.org][Link]]

    Package Newmark-Beta allows for the implementation of time integration in FrameFEA.

*** Static [[file:./static/README.org][Link]]

    Package static allows for a static analysis of a model with loads applied.

* References
#+begin_export latex
The frame3dd software on which framefea is based was developed using methods described in the following texts. The two primary sources are the texts by A. Kassimali and J.S. Przemieniecki. Other relevant books and articles are:

\bibliographystyle{plain}
\bibliography{$HOME/doc/references/references}

\nocite{Allen1980}

\nocite{Bathe1995}

\nocite{Boresi1993}

\nocite{Chen1997}

\nocite{Clough1993}

\nocite{Cowper1966}

\nocite{Hartog1987}

\nocite{Dong2010}

\nocite{Hughes2000}

\nocite{Kaneko1978}

\nocite{Kassimali1999}

\nocite{Mackerle1997}

\nocite{Mackerle2000}

\nocite{McGuire1999}

\nocite{Paz1984}

\nocite{Pilkey1995}

\nocite{Press1993}

\nocite{Przemieniecki1985}

\nocite{Rosinger1977}

\nocite{Sennett2000}

\nocite{Timoshenko1951}

\nocite{Ugural1995}

#+end_export

