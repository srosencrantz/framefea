#!/bin/bash

grep -E 'Nodal.*, 37,' *.ffout | sed 's/exO_/,/g' | sed 's/.ffout/ /g' | sed 's/:/,/g' | sort -n | awk -v RS="NodalAcceleration" 'BEGIN { OFS="," ; FS="," } {print  $10, $15, $25, $5}'
