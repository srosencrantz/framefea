#!/bin/bash

examples="A1 A B1 B2 B C D E F G I K L M N O"

for i in $examples;
do
    echo
    echo Running example ${i}
    echo ------------------------------------
    echo 
    framefea -in ex${i}.ffin -out ex${i} -V 1
    echo
done

