#!/bin/bash

grep -E 'Nodal.*, 2,' *.ffout | sed 's/exM2_/,/g' | sed 's/.ffout/ /g' | sed 's/:/,/g' | sort -n | awk -v RS="NodalAcceleration" 'BEGIN { OFS="," ; FS="," } {print  $10, $16, $26, $6}'
