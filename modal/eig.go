/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package modal

import (
	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
	"fmt"
	"math"
)

// subsace finds the lowest m eigen-values, w, and eigen-vectors, V, of the
// general eigen-problem, K V = w M V using sub-space / Jacobi iteration
// where
// K is an n by n  symmetric real (stiffness) matrix
// M is an n by n  symmetric positive definate real (mass) matrix
// w is a diagonal matrix of eigen-values
// V is a  rectangular matrix of eigen-vectors
// K is an n by n  symmetric real (stiffness) matrix -input
// M is an n by n  symmetric positive definate real (mass) matrix -input
// w is a diagonal matrix of eigen-values -output
// V is a  rectangular matrix of eigen-vectors -output
// K, stiffness and mass matrices
// n, int m, DoF and number of required modes	-output
// w, V, modal frequencies and mode shapes	must guess an answer for V first
// tol,	covergence tolerence
// shift,	frequency shift for unrestrained frames or zero frequencies
// iter, number of sub-space iterations
// ok, sturm check result
func subspace(K, M [][]float64, n, m int, w []float64, V [][]float64, tol, shift float64) (iter, ok int, finalErr error) {

	var modes int
	idx := make([]int, m)

	//added since C code was using 0.0 as an indication that it had not been changed
	for i := 0; i < m; i++ {
		idx[i] = -1
	}

	var km, kmOld, wOld float64

	d := make([]float64, n)
	u := make([]float64, n)
	v := make([]float64, n)
	Kb := femath.Float2d(m, m)
	Mb := femath.Float2d(m, m)
	Qb := femath.Float2d(m, m)
	Xb := femath.Float2d(n, m)

	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			V[i][j] = 0.0
		}
	}

	if m > n {
		finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Subspace, Number of eigen-values must be less than the problem dimension. \nDesired number of eigen-values=%d \n Dimension of the problem= %d", m, n))
		return iter, ok, finalErr
	}

	if 0.5*float64(m) > float64(m)-8.0 { // 0.5*2> 2-8.0
		modes = (int)(float64(m) / 2.0) // 2/2=1.0 mode
	} else {
		modes = m - 8
	}
	modes = modes - 1 // to get the same mode as the c version

	// 	shift eigen-values by this much
	// K MODIFIED
	for i := 0; i < n; i++ {
		for j := i; j < n; j++ {
			K[i][j] += shift * M[i][j]
		}
	}

	var err error

	ok, err = femath.LdlDcmp(K, n, u, v, v, true, false) // use L D L' decomp
	for i := 0; i < n; i++ {
		if M[i][i] <= 0.0 {
			finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Subspace, M[%d][%d] = %e", i, i, M[i][i]))
			return iter, ok, finalErr
		}
		d[i] = K[i][i] / M[i][i]
	}
	//this next section fills idx to determine which case to use in the switch logic
	// Also this section defines the d vector which is used for the initial Xb guess.
	kmOld = 0.0
	for k := 0; k < m; k++ {
		km = d[0] // CHANGE was d[1] C code says d[1]
		for i := 0; i < n; i++ {

			//first time through kmold=0,km=d0, second time true, third time kmOld=1.5
			if kmOld <= d[i] && d[i] <= km {
				ok = 1

				//First time through k=0 J is not -1, when k=1 and j 0 works, k=2 and j= 0 and 1
				for j := 0; j <= k-1; j++ {
					if i == idx[j] {
						ok = 0
					}
				}
				if ok == 1 {
					km = d[i]
					idx[k] = i
				}
			}
		}
		if idx[k] == -1 { //CHANGED to -1 from 0 in the C code
			//Logic is if idx[k] is unset set i = idx0, then if i<any idx set i = to the max idx
			i := idx[0] // CHANGED idx[1] to idx[0]

			// This increases i to the highest value of idx
			for j := 0; j < k; j++ { // CHANGED j < k to j < k-1 because the c was j < k
				if i < idx[j] {
					i = idx[j]
				}
			}
			idx[k] = i + 1
			km = d[i+1]
		}
		kmOld = km
	}

	for k := 0; k < m; k++ { //initializing eigenvector columns
		V[idx[k]][k] = 1.0
		ok = idx[k] % 6 //first loop in test case ok=1 so i=0 and j=1

		var i, j int
		switch ok {
		case 0: //1
			i = 1
			j = 2
		case 1: //2
			i = -1
			j = 1
		case 2: //3
			i = -1
			j = -2
		case 3: //4
			i = 1
			j = 2
		case 4: //5
			i = -1
			j = 1
		case 5: //0
			i = -1
			j = -2
		}
		V[idx[k]+i][k] = 0.2
		V[idx[k]+j][k] = 0.2
	}

	//	for (i=1; i<=n; i++)	V[i][1] = M[i][i];	// diag(M)

	iter = 0
	theErr := float64(1.0)

	for theErr > tol { /* Begin sub-space iterations */
		for k := 0; k < m; k++ { /* K Xb = M V	(12.10 Bathe) */
			femath.ProdABj(M, V, n, k, v)
			ok, err = femath.LdlDcmp(K, n, u, v, d, false, true) //  LDL bk-sub
			if err != nil {
				finalErr = errcat.AppendErr(finalErr, err)
				return iter, ok, finalErr
			}
			/* improve the solution iteratively */
			theErr = 1
			itsOk := true //ok = 1
			for itsOk /*ok == 1*/ {
				itsOk, err = femath.LdlMprove(K, n, u, v, d, &theErr)
				if err != nil {
					if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
						return iter, ok, finalErr
					}
				}
			}
			for i := 0; i < n; i++ {
				Xb[i][k] = d[i]
			}
		}
		/* Kb = Xb' K Xb (12.11) -Finds projections of operator K onto Xb the starting vectors*/
		femath.XtAx(K, Xb, Kb, n, m)

		/* Mb = Xb' M Xb (12.12) -Finds projections of operator K onto Xb the starting vectors*/
		femath.XtAx(M, Xb, Mb, n, m)

		jacobi(Kb, Mb, w, Qb, m)          /* (12.13 Bathe) Solve for Qbthe eigensystem of the projected operators*/
		femath.ProdAB(Xb, Qb, V, n, m, m) // V = Xb Qb (12.14 Bathe)  Find an improved approximation of the Eigenvectors
		eigSort(w, V, n, m)

		if w[modes] == 0.0 {
			finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Subspace, Zero frequency found! \n w[%d] = %e", modes, w[modes]))
			return iter, ok, finalErr
		}
		theErr = math.Abs(w[modes]-wOld) / w[modes]

		iter++
		wOld = w[modes]

		if iter > 1000 {
			finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Subspace, Iteration limit exceeded\n rel. error = %e > %e", theErr, tol))
			return iter, ok, finalErr
		}
	} /* End   sub-space iterations */
	for k := 0; k < m; k++ { /* shift eigen-values */
		if w[k] > shift {
			w[k] = w[k] - shift
		} else {
			w[k] = shift - w[k]
		}
	}

	ok, err = sturm(K, M, n, m, shift, w[modes]+tol)
	if err != nil {

		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return iter, ok, finalErr
		}
	}

	for i := 0; i < n; i++ {
		for j := i; j < n; j++ {
			K[i][j] -= shift * M[i][j]
		}
	}
	return iter, ok, finalErr
}

//  jacobi Finds all eigen-values, E, and eigen-vectors, V,
//  of the general eigen-problem  K V = E M V
//  using Jacobi iteration, with efficient matrix rotations.
//  K is a symmetric real (stiffness) matrix -Given
//  M is a symmetric positive definate real (mass) matrix -Given
//  E is a diagonal matrix of eigen-values
//  V is a  square  matrix of eigen-vectors -Created inside this function
//  If s is zero and KKii and KKjj are equal you get undefined terms.  Need to check for that condition
func jacobi(K, M [][]float64, E []float64, V [][]float64, n int) {
	var iter, dg, i, j, k int
	var KKii, KKjj, KKij, MMjj, MMij, Vki, Vkj, alpha, beta, gamma, s, tol float64

	for i = 0; i < n; i++ {
		for j = i; j < n; j++ {
			V[i][j], V[j][i] = 0.0, 0.0
		}
	}
	for dg = 0; dg < n; dg++ {
		V[dg][dg] = 1.0
	}

	for iter = 1; iter <= 2*n; iter++ { /* Begin Sweep Iteration */
		//tol = 0.01 ^ (2.0 * iter) this was uncommented in the original code but didn't make sense given the next line
		tol = 0.0
		for dg = 0; dg < n-1; dg++ { /* sweep along upper diagonals of K and M matricies*/
			for i = 0; i < (n - dg - 1); i++ { /* row */
				j = i + dg + 1 /* column */
				KKij = K[i][j]
				MMij = M[i][j]

				if KKij*KKij/(K[i][i]*K[j][j]) > tol || MMij*MMij/(M[i][i]*M[j][j]) > tol { /* do a rotation */
					//measure of coupling factor between dof's i and j.  Trying to decouple
					// above line always comes out to 1 when i=j, if i!=j then checks for off diag > diags Basically looking for off diagonal terms not equal to zero.
					KKii = K[i][i]*MMij - KKij*M[i][i] //3*100-1*3000=0
					KKjj = K[j][j]*MMij - KKij*M[j][j]

					s = K[i][i]*M[j][j] - K[j][j]*M[i][i] //1*75-1*75 Convergence Tolerance of measure to assum off
					// diagonal elements are small

					//http://www4.hcmut.edu.vn/~vinhbd/Documents/Finite%20Element%20Procedures%20in%20Engineering%20analysis-Bathe.pdf
					// page 638
					if s >= 0.0 {
						gamma = 0.5*s + math.Sqrt(0.25*s*s+KKii*KKjj) //=0
					} else {
						gamma = 0.5*s - math.Sqrt(0.25*s*s+KKii*KKjj)
					}
					if gamma != 0 { // if M is positive Definite full or banded mass matrix this will be true
						//						fmt.Println("gamma", gamma)
						alpha = KKjj / gamma // These are selected to reduce to zero simultaneously elements i,j in K and M
						beta = -KKii / gamma //

						rotate(K, n, alpha, beta, i, j) /* make Kij zero  Operates on K only */
						rotate(M, n, alpha, beta, i, j) /* make Mij zero   Operates on M only */

						for k = 0; k < n; k++ { /*  update eigen-vectors  V = V * P */
							Vki = V[k][i]
							Vkj = V[k][j]
							V[k][i] = Vki + beta*Vkj
							V[k][j] = Vkj + alpha*Vki
						}
					} /* gamma not equal to zero*-bjb */
				} /* rotations complete */
			} /* row */
		} /* diagonal */
	} /* End Sweep Iteration */

	for j = 0; j < n; j++ { /* scale eigen-vectors, Only uses the diagonal term of the mass matrix*/
		MMjj = math.Sqrt(M[j][j])
		for i = 0; i < n; i++ {
			V[i][j] /= MMjj
		}
	}

	for j = 0; j < n; j++ {
		E[j] = K[j][j] / M[j][j] /* eigen-values uses both K and M to get Eigen values */
	}

	return
}

// rotate, rotates an n by n symmetric matrix A such that A[i][j] = A[j][i] = 0
// This only works on one value of i and one value of j.
//      A = P' * A * P  where diag(P) = 1 and P[i][j] = alpha and P[j][i] = beta.
//      Since P is sparse, this matrix multiplcation can be done efficiently.
func rotate(A [][]float64, n int, alpha, beta float64, i, j int) {

	Ai := make([]float64, n)
	Aj := make([]float64, n)

	for k := 0; k < n; k++ { // saving all off-diagonal elements in columns
		Ai[k] = A[i][k]
		Aj[k] = A[j][k]
	}

	Aii := A[i][i]
	Ajj := A[j][j]
	Aij := A[i][j]

	A[i][i] = Aii + float64(2.0)*beta*Aij + beta*beta*Ajj // Redefining diagonal elements.
	A[j][j] = Ajj + float64(2.0)*alpha*Aij + alpha*alpha*Aii

	for k := 0; k < n; k++ { // Redefines off-diagonal elements since i&j are not equal to k
		if k != i && k != j {
			A[i][k] = Ai[k] + beta*Aj[k]
			A[k][i] = A[i][k]
			A[j][k] = Aj[k] + alpha*Ai[k]
			A[k][j] = A[j][k]
		}
	}
	A[j][i] = float64(0.0)
	A[i][j] = float64(0.0)
	return
}

// stodola, calculate the lowest m eigen-values and eigen-vectors of the
// generalized eigen-problem, K v = w M v, using a matrix iteration approach
// with shifting. 								15oct98
// 	@param n number of degrees of freedom
// 	@param m number of required modes
// 	double **K, double **M,	**< stiffness and mass matrices
// 	int n, int m,		*< DoF and number of required modes
// 	double *w, double **V,	*< modal frequencies and mode shapes
// 	double tol,		*< covergence tolerence
// 	double shift,		/**< frequency shift for unrestrained frames */
// 	int *iter,		/**< number of sub-space iterations	*/
// 	int *ok,		/**< sturm check result			*/
func stodola(K, M [][]float64, n, m int, w []float64, V [][]float64, tol, shift float64) (iter, retOk int, finalErr error) {

	D := femath.Float2d(n, n)                      // the dynamics matrix, D = K^(-1) M
	dMin := float64(0.0)                           // minimum value of D[i][i]
	dMax := float64(0.0)                           // maximum value of D[i][i]
	dOld := float64(0.0)                           // previous extreme value of D[i][i]
	d := make([]float64, n)                        // columns of the D, M, and V matrices
	u, v := make([]float64, n), make([]float64, n) // trial eigen-vector vectors
	c := make([]float64, m)                        // coefficients for lower mode purge
	var vMv float64                                // factor for mass normalization
	var RQ, RQold float64                          // Raliegh quotient

	iEx := int(9999) // location of minimum value of D[i][i]
	var modes int    // number of desired modes
	var i, j, k int
	theErr, ok := float64(1.0), true

	if 0.5*float64(m) > float64(m)-8 {
		modes = int(float64(m) / 2.0)
	} else {
		modes = m - 8
	}
	modes = modes - 1 // to get the same mode as c

	// shift eigen-values by this much
	for i = 0; i < n; i++ {
		for j = 0; j < n; j++ {
			K[i][j] += shift * M[i][j]
		}
	}
	var pd int
	var err error
	pd, err = femath.LdlDcmp(K, n, u, v, v, true, false) // use L D L' decomp
	if pd < 0 {
		finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Stodola, make sure that all six rigid body translation are restrained."))
		return iter, retOk, finalErr
	}
	// calculate  D = K^(-1) M
	for j = 0; j < n; j++ {
		for i = 0; i < n; i++ {
			v[i] = M[i][j]
		}

		pd, err = femath.LdlDcmp(K, n, u, v, d, false, true) // L D L' bk-sub
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return iter, retOk, finalErr
			}
		}

		// improve the solution iteratively
		theErr, ok = float64(1.0), true
		for ok { // this was supposed to be "for ok" in c treating an int as a bool not sure if this conversion is always correct
			ok, err = femath.LdlMprove(K, n, u, v, d, &theErr)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return iter, retOk, finalErr
				}
			}
		}

		for i = 0; i < n; i++ {
			D[i][j] = d[i]
		}
	}

	iter = 0
	for i = 0; i < n; i++ {
		if D[i][i] > dMax {
			dMax = D[i][i]
		}
	}
	dOld = dMax
	dMin = dMax
	for i = 0; i < n; i++ {
		if D[i][i] < dMin {
			dMin = D[i][i]
		}
	}

	for k = 0; k < m; k++ { // loop over lowest m modes
		dMax = dMin
		for i = 0; i < n; i++ { /* initial guess */
			u[i] = 0.0
			if D[i][i] < dOld && D[i][i] > dMax {
				dMax = D[i][i]
				iEx = i
			}
		}
		u[iEx] = 1.0
		if iEx+1 < n {
			u[iEx+1] = 1.e-4
		}
		dOld = dMax

		vMv = femath.XtAy(u, M, u, n, d) // mass-normalize */
		for i = 0; i < n; i++ {
			u[i] /= math.Sqrt(vMv)
		}

		for j = 0; j < k; j++ { /* purge lower modes */
			for i = 0; i < n; i++ {
				v[i] = V[i][j]
			}
			c[j] = femath.XtAy(v, M, u, n, d)
		}
		for j = 0; j < k; j++ {
			for i = 0; i < n; i++ {
				u[i] -= c[j] * V[i][j]
			}
		}

		vMv = femath.XtAy(u, M, u, n, d) /* mass-normalize */
		for i = 0; i < n; i++ {
			u[i] /= math.Sqrt(vMv)
		}
		RQ = femath.XtAy(u, K, u, n, d) /* Raleigh quotient */

		for math.Abs(RQ-RQold)/RQ > tol { /* iterate	*/
			for i = 0; i < n; i++ { /* v = D u	*/
				v[i] = 0.0
				for j = 0; j < n; j++ {
					v[i] += D[i][j] * u[j]
				}
			}

			vMv = femath.XtAy(v, M, v, n, d) /* mass-normalize */
			for i = 0; i < n; i++ {
				v[i] /= math.Sqrt(vMv)
			}

			for j = 0; j < k; j++ { /* purge lower modes */
				for i = 0; i < n; i++ {
					u[i] = V[i][j]
				}
				c[j] = femath.XtAy(u, M, v, n, d)
			}
			for j = 0; j < k; j++ {
				for i = 0; i < n; i++ {
					v[i] -= c[j] * V[i][j]
				}
			}

			vMv = femath.XtAy(v, M, v, n, d) /* mass-normalize */
			for i = 0; i < n; i++ {
				u[i] = v[i] / math.Sqrt(vMv)
			}

			RQold = RQ
			RQ = femath.XtAy(u, K, u, n, d) /* Raleigh quotient */
			iter++

			if iter > 1000 {
				finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("Stodola, iteration limit exceeded\n  rel. error = %e > %e", math.Abs(RQ-RQold)/RQ, tol))
				return iter, retOk, finalErr
			}
		}

		for i = 0; i < n; i++ {

			V[i][k] = v[i]
		}
		w[k] = femath.XtAy(u, K, u, n, d)
		if w[k] > shift {
			w[k] = w[k] - shift
		} else {
			w[k] = shift - w[k]
		}
	}
	eigSort(w, V, n, m)
	retOk, err = sturm(K, M, n, m, shift, w[modes]+tol)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return iter, retOk, finalErr
		}
	}
	return iter, retOk, finalErr
}

// eigSort, Given the eigenvallues e[1..m] and eigenvectors v[1..n][1..m],
// this routine sorts the eigenvalues into ascending order, and rearranges
// the columns of v correspondingly.  The method is straight insertion.
// Adapted from Numerical Recipes in C, Ch 11
func eigSort(e []float64, v [][]float64, n, m int) {
	var k, j, i int
	var p float64

	for i = 0; i < m-1; i++ {
		k = i
		p = e[k]
		for j = i + 1; j < m; j++ {
			if e[j] <= p {
				k = j
				p = e[k] // find smallest eigen-value
			}
		}
		if k != i {
			e[k] = e[i] // swap eigen-values
			e[i] = p
			for j = 0; j < n; j++ { // swap eigen-vectors
				v[j][i], v[j][k] = v[j][k], v[j][i]
			}
		}
	}
}

// sturm Determine the number of eigenvalues, w, of the general eigen-problem
//   K V = w M V which are below the value ws,
//   K is an n by n  symmetric real (stiffness) matrix
//   M is an n by n  symmetric positive definate real (mass) matrix
//   w is a diagonal matrix of eigen-values
//   ws is the limit
//   n is the number of DoF
//   m is the number of required modes
func sturm(K, M [][]float64, n, m int, shift, ws float64) (ok int, finalErr error) {
	d := make([]float64, n)
	wsShift := ws + shift
	modes := m - 8
	var i, j int

	if float64(m)*0.5 > float64(m)-8.0 {
		modes = int(float64(m) / 2.0)
	}

	for i = 0; i < n; i++ {
		for j = i; j < n; j++ {
			K[i][j] -= wsShift * M[i][j]
		}
	}
	var err error
	ok, err = femath.LdlDcmp(K, n, d, d, d, true, false)
	if err != nil {
		finalErr = errcat.AppendErr(finalErr, err)
	}

	if -ok > modes {
		finalErr = errcat.AppendWarnStr(finalErr, fmt.Sprintf("%d modes were not found. Try increasing the number of modes in order to get the missing modes below %f Hz.", -ok-modes, math.Sqrt(ws)/(2.0*math.Pi)))
	}

	for i = 0; i < n; i++ {
		for j = i; j < n; j++ {
			K[i][j] += wsShift * M[i][j]
		}
	}
	return ok, finalErr
}
