/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package modal

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/framefea/transform"
)

// Input contains the necessary input data to run a modal analysis with the exception of the stiffness matrix.
type Input struct {

	// Node Info
	NumNodes   int           // number of Nodes
	Xyz        []femath.Vec3 // {NumNodes} X,Y,Z node coordinates (global)
	NodeRadius []float64     // {NumNodes} node size radius, for finite sizes

	// Reaction Info
	ReactedDof []bool // {Dof} Dof's with reactions formally "r"

	// Element Info
	NumElems    int       // Number of Frame Elements
	N1, N2      []int     // {NumElems} begin and end node numbers
	Ax          []float64 // {NumElems} cross section area
	Jx, Iy, Iz  []float64 // {NumElems} section inertias
	ElemRoll    []float64 // {NumElems} roll of each member, radians
	ElemDensity []float64 // {NumElems} member densities

	L []float64 //  node-to-node length of each element (calculated)

	// Dynamic Analysis Data
	NumModes       int       // number of desired modes
	ModalMethod    int       // modal analysis method: 1=Jacobi-Subspace or 2=Stodola
	LumpFlag       bool      // true: lumped mass matrix or false: consistent mass matrix
	ModalTol       float64   // convergence tolerance for modal analysis
	ModalFreqShift float64   // modal frequency shift for unrestrained structures, shift-factor for rigid-body-modes
	NMs            []float64 // {NumNodes} node mass for each node
	NMx            []float64 // {NumNodes} node inertia about global X axis
	NMy            []float64 // {NumNodes} node inertia about global Y axis
	NMz            []float64 // {NumNodes} node inertia about global Z axis
	EMs            []float64 // {NumElems} lumped mass for each frame element

	// internal variables
	dof int // degrees of freedom
}

// DoF returns the degrees of freedom of the model described by the ModalInput structure (6*NumNodes).
func (inData *Input) DoF() int {
	if inData.dof == 0 {
		inData.dof = inData.NumNodes * 6
	}
	return inData.dof
}

// Run performs a modal analysis of the model, and returns the dynamic stiffness and mass matricies,
// and the resonant mode-shapes and frequencies.
func (inData *Input) Run(K [][]float64) (Kd, Md, M, V [][]float64, f []float64, iter int, finalErr error) {
	// global mass matrix
	// M global mass matrix
	// V resonant mode-shapes
	// f resonant frequencies
	var err error

	var nMCalc int                                     // number of modes to calculate
	if (inData.NumModes + 8) < (2 * inData.NumModes) { /* Bathe */
		nMCalc = inData.NumModes + 8
	} else {
		nMCalc = 2 * inData.NumModes
	}

	M = femath.Float2d(inData.DoF(), inData.DoF())
	V = femath.Float2d(inData.DoF(), nMCalc)
	f = make([]float64, nMCalc)

	err = AssembleM(M, inData.DoF(), inData.NumNodes, inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L,
		inData.N1, inData.N2, inData.Ax, inData.Jx, inData.Iy, inData.Iz, inData.ElemRoll,
		inData.ElemDensity, inData.EMs, inData.NMs, inData.NMx, inData.NMy, inData.NMz, inData.LumpFlag)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return nil, nil, nil, nil, nil, 0, finalErr
		}
	}

	traceK := float64(0.0)              // trace of the global stiffness matrix
	traceM := float64(0.0)              // trace of the global mass matrix
	for j := 0; j < inData.DoF(); j++ { /*  compute traceK and traceM */
		if !inData.ReactedDof[j] {
			traceK += K[j][j]
			traceM += M[j][j]
		}
	}
	for i := 0; i < inData.DoF(); i++ { /*  modify K and M for reactions    */
		if inData.ReactedDof[i] { /* apply reactions to upper triangle */
			K[i][i] = traceK * 1e4
			M[i][i] = traceM
			for j := i + 1; j < inData.DoF(); j++ {
				K[j][i], K[i][j], M[j][i], M[i][j] = 0.0, 0.0, 0.0, 0.0
			}
		}
	}

	Kd = femath.Float2d(inData.DoF(), inData.DoF())
	Md = femath.Float2d(inData.DoF(), inData.DoF())
	for i := 0; i < inData.DoF(); i++ {
		for j := 0; j < inData.DoF(); j++ {
			Kd[i][j] = K[i][j]
			Md[i][j] = M[i][j]
		}
	}

	switch inData.ModalMethod {
	case 1:
		iter, _, err = subspace(K, M, inData.DoF(), nMCalc, f, V, inData.ModalTol, inData.ModalFreqShift)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Kd, Md, nil, nil, nil, 0, finalErr
			}
		}
	case 2:
		iter, _, err = stodola(K, M, inData.DoF(), nMCalc, f, V, inData.ModalTol, inData.ModalFreqShift)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Kd, Md, nil, nil, nil, 0, finalErr
			}
		}
	}

	for j := 0; j < nMCalc; j++ {
		f[j] = math.Sqrt(f[j]) / (2.0 * math.Pi)
	}

	return Kd, Md, M, V, f, iter, finalErr

}

// AssembleM assembles the global mass matrix from element mass & inertia  24nov98
// 	double **M,	/**< mass matrix				*/
// 	int DoF,	/**< number of degrees of freedom		*/
// 	int nN, int nE,	/**< number of nodes, number of frame elements	*/
// 	vec3 *xyz,	/** XYZ locations of each node			*/
// 	float *r,	/**< rigid radius of every node		*/
// 	double *L,	/**< length of each frame element, effective	*/
// 	int *N1, int *N2, /**< node connectivity			*/
// 	float *Ax,	/**< node connectivity				*/
// 	float *Jx, float *Iy, float *Iz,	/**< section area inertias*/
// 	float *p,	/**< roll angle, radians			*/
// 	float *d,	/**< frame element density			*/
// 	float *EMs,	/**< extra frame element mass			*/
// 	float *NMs,	/**< node mass					*/
// 	float *NMx, float *NMy, float *NMz,	/**< node inertias	*/
// 	int lump,	/**< 1: lumped mass matrix, 0: consistent mass	*/
func AssembleM(M [][]float64, DoF, nN, nE int, xyz []femath.Vec3, r, L []float64, N1, N2 []int, Ax, Jx, Iy, Iz, p, d, EMs, NMs, NMx, NMy, NMz []float64, lump bool) (finalErr error) {

	var i, j, ii, jj, l, ll int
	m := femath.Float2d(12, 12)
	ind := femath.Int2d(12, nE)

	for i = 0; i < DoF; i++ {
		for j = 0; j < DoF; j++ {
			M[i][j] = 0.0
		}
	}

	for i = 0; i < nE; i++ {
		ind[0][i] = 6 * N1[i]
		ind[6][i] = 6 * N2[i]
		ind[1][i] = ind[0][i] + 1
		ind[7][i] = ind[6][i] + 1
		ind[2][i] = ind[0][i] + 2
		ind[8][i] = ind[6][i] + 2
		ind[3][i] = ind[0][i] + 3
		ind[9][i] = ind[6][i] + 3
		ind[4][i] = ind[0][i] + 4
		ind[10][i] = ind[6][i] + 4
		ind[5][i] = ind[0][i] + 5
		ind[11][i] = ind[6][i] + 5
	}
	for i = 0; i < nE; i++ {

		if lump {
			err := lumpedM(m, xyz, L[i], N1[i], N2[i], Ax[i], Jx[i], Iy[i], Iz[i], p[i], d[i], EMs[i])
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}
		} else {
			err := consistentM(m, xyz, r, L[i], N1[i], N2[i], Ax[i], Jx[i], Iy[i], Iz[i], p[i], d[i], EMs[i])
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}
		}

		for l = 0; l < 12; l++ {
			ii = ind[l][i]
			for ll = 0; ll < 12; ll++ {
				jj = ind[ll][i]
				M[ii][jj] += m[l][ll]
			}
		}

	}

	for j = 0; j < nN; j++ { // add extra node mass
		i = 6 * j
		M[i+0][i+0] += NMs[j]
		M[i+1][i+1] += NMs[j]
		M[i+2][i+2] += NMs[j]
		M[i+3][i+3] += NMx[j]
		M[i+4][i+4] += NMy[j]
		M[i+5][i+5] += NMz[j]
	}

	for i = 0; i < DoF; i++ {
		if M[i][i] <= 0.0 {
			finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("assembleM, Non pos-def mass matrix\n M[%d][%d] = %v", i, i, M[i][i]))
			return finalErr
		}
	}
	return nil
}

//  lumpedM, space frame element lumped mass matrix in global coordnates 7apr94
func lumpedM(m [][]float64, xyz []femath.Vec3, L float64, n1, n2 int, Ax, J, Iy, Iz, p, d, EMs float64) (finalErr error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* coord Xformn */
	var t, ry, rz, po float64                      /* translational, rotational & polar inertia */

	transform.CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)

	/* rotatory inertia of extra mass is neglected */
	t = (d*Ax*L + EMs) / 2.0
	ry = d * Iy * L / 2.0
	rz = d * Iz * L / 2.0
	po = d * L * J / 2.0 /* assumes simple cross-section	*/

	for i := 0; i < 12; i++ {
		for j := 0; j < 12; j++ {
			m[i][j] = 0.0
		}
	}

	m[0][0], m[1][1], m[2][2], m[6][6], m[7][7], m[8][8] = t, t, t, t, t, t

	m[9][9] = po*t1*t1 + ry*t4*t4 + rz*t7*t7
	m[10][10] = po*t2*t2 + ry*t5*t5 + rz*t8*t8
	m[11][11] = po*t3*t3 + ry*t6*t6 + rz*t9*t9
	m[3][3] = m[9][9]
	m[4][4] = m[10][10]
	m[5][5] = m[11][11]

	m[10][9] = po*t1*t2 + ry*t4*t5 + rz*t7*t8
	m[3][4], m[4][3], m[9][10] = m[10][9], m[10][9], m[10][9]
	m[11][9] = po*t1*t3 + ry*t4*t6 + rz*t7*t9
	m[3][5], m[5][3], m[9][11] = m[11][9], m[11][9], m[11][9]
	m[11][10] = po*t2*t3 + ry*t5*t6 + rz*t8*t9
	m[4][5], m[5][4], m[10][11] = m[11][10], m[11][10], m[11][10]

	return finalErr
}

// consistentM, space frame consistent mass matrix in global coordnates 2oct97
// does not include shear deformations
func consistentM(m [][]float64, xyz []femath.Vec3, r []float64, L float64, n1, n2 int, Ax, J, Iy, Iz, p, d, EMs float64) (finalErr error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* coord Xformn */
	var t, ry, rz, po float64                      /* translational, rotational & polar inertia */
	var i, j int

	transform.CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)

	t = d * Ax * L
	ry = d * Iy
	rz = d * Iz
	po = d * J * L

	for i = 0; i < 12; i++ {
		for j = 0; j < 12; j++ {
			m[i][j] = 0.0
		}
	}

	m[6][6] = t / 3.
	m[7][7] = 13.*t/35. + 6.*rz/(5.*L)
	m[8][8] = 13.*t/35. + 6.*ry/(5.*L)
	m[9][9] = po / 3.

	m[10][10] = t*L*L/105. + 2.*L*ry/15.
	m[11][11] = t*L*L/105. + 2.*L*rz/15.

	m[2][4] = -11.*t*L/210. - ry/10.
	m[1][5] = 11.*t*L/210. + rz/10.
	m[0][6] = t / 6.

	m[5][7] = 13.*t*L/420. - rz/10.
	m[4][8] = -13.*t*L/420. + ry/10.
	m[3][9] = po / 6.
	m[2][10] = 13.*t*L/420. - ry/10.
	m[1][11] = -13.*t*L/420. + rz/10.

	m[8][10] = 11.*t*L/210. + ry/10.
	m[7][11] = -11.*t*L/210. - rz/10.

	m[1][7] = 9.*t/70. - 6.*rz/(5.*L)
	m[2][8] = 9.*t/70. - 6.*ry/(5.*L)
	m[4][10] = -L*L*t/140. - ry*L/30.
	m[5][11] = -L*L*t/140. - rz*L/30.

	m[0][0] = m[6][6]
	m[1][1] = m[7][7]
	m[2][2] = m[8][8]
	m[3][3] = m[9][9]
	m[4][4] = m[10][10]
	m[5][5] = m[11][11]

	m[4][2] = m[2][4]
	m[5][1] = m[1][5]
	m[6][0] = m[0][6]

	m[7][5] = m[5][7]
	m[8][4] = m[4][8]
	m[9][3] = m[3][9]
	m[10][2] = m[2][10]
	m[11][1] = m[1][11]

	m[10][8] = m[8][10]
	m[11][7] = m[7][11]

	m[7][1] = m[1][7]
	m[8][2] = m[2][8]
	m[10][4] = m[4][10]
	m[11][5] = m[5][11]

	/* rotatory inertia of extra beam mass is neglected */

	for i = 0; i < 3; i++ {
		m[i][i] += 0.5 * EMs
	}
	for i = 6; i < 9; i++ {
		m[i][i] += 0.5 * EMs
	}

	transform.Atma(t1, t2, t3, t4, t5, t6, t7, t8, t9, m, r[n1], r[n2]) /* globalize */

	/* check and enforce symmetry of consistent element mass matrix */

	for i = 0; i < 12; i++ {
		for j = i + 1; j < 12; j++ {
			if m[i][j] != m[j][i] {
				if math.Abs(m[i][j]/m[j][i]-1.0) > 1.0e-6 && (math.Abs(m[i][j]/m[i][i]) > 1e-6 || math.Abs(m[j][i]/m[i][i]) > 1e-6) {
					finalErr = errcat.AppendWarnStr(finalErr, fmt.Sprintf("ConsistentM, element mass matrix not symetric\n"+
						" m[%d][%d] = %15.6e \n"+
						" m[%d][%d] = %15.6e \n"+
						" m[%d][%d] = %15.6e \n"+
						" relative error(i,j vs j,i) = %e %v \n"+
						" relative error(i,j vs i,i) = %e \n"+
						" relative error(j,i vs i,i) = %e \n",
						i, j, m[i][j],
						j, i, m[j][i],
						i, i, m[i][i],
						math.Abs(m[i][j]/m[j][i]-1.0), math.Abs(m[i][j]/m[j][i]-1.0) > 1.0e-6,
						math.Abs(m[i][j]/m[i][i]),
						math.Abs(m[j][i]/m[i][i])))
				}
				m[j][i] = 0.5 * (m[i][j] + m[j][i])
				m[i][j] = m[j][i]
			}
		}
	}
	return finalErr
}
