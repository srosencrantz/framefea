/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package newmarkbeta

import (
	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/framefea/modal"
	"bitbucket.org/srosencrantz/framefea/static"
	"bitbucket.org/srosencrantz/vectnd"
)

// Input contains the necessary input data to run a modal analysis with the exception of the stiffness matrix.
type Input struct {
	// Run Info
	Shear bool // indicates shear deformation
	Geom  bool // indicates  geometric nonlinearity

	// Node Info
	NumNodes   int           // number of Nodes
	Xyz        []femath.Vec3 // {NumNodes} X,Y,Z node coordinates (global)
	NodeRadius []float64     // {NumNodes} node size radius, for finite sizes

	// Reaction Info
	ReactedDof   []bool // {Dof} Dof's with reactions formally "r"
	UnreactedDof []bool // {Dof} Dof's without reactions formally "q" (calculated)

	// Element Info
	NumElems     int       // Number of Frame Elements
	N1, N2       []int     // {NumElems} begin and end node numbers
	Ax, Asy, Asz []float64 // {NumElems} cross section areas, incl. shear
	Jx, Iy, Iz   []float64 // {NumElems} section inertias
	ElemRoll     []float64 // {NumElems} roll of each member, radians
	E, G         []float64 // {NumElems} elastic modulus, shear modulus
	ElemDensity  []float64 // {NumElems} member densities

	L  []float64 //  node-to-node length of each element (calculated)
	Le []float64 //  effective length, accounts for node size (calculated)

	// Load Info
	StaticTol       float64   // Convergence tolerance for the static analysis when Geom == true
	AnalyzeLoadCase bool      //  Are there mechanical loads in Mechanical Loads
	MechanicalLoads []float64 // {Dof} mechanical load vectors, all load cases

	TempLoads    bool      // Are there temperature loads in the load case
	ThermalLoads []float64 // {Dof} thermal load vectors, all load cases

	EqFMech [][]float64 //  {numElems}{12} equivalent end forces from mech loads global (calculated)
	EqFTemp [][]float64 //  {numElems}{12} equivalent end forces from temp loads global (calculated)

	// Mass Info
	LumpFlag bool      // true: lumped mass matrix or false: consistent mass matrix
	NMs      []float64 // {NumNodes} node mass for each node
	NMx      []float64 // {NumNodes} node inertia about global X axis
	NMy      []float64 // {NumNodes} node inertia about global Y axis
	NMz      []float64 // {NumNodes} node inertia about global Z axis
	EMs      []float64 // {NumElems} lumped mass for each frame element

	// Time integration parameters
	Dt    float64
	Alpha float64
	Beta  float64

	// Initial Conditions
	U0 []float64 // {Dof} prescribed node displacements
	V0 []float64 // {Dof} initial nodal velocities
	A0 []float64 // {Dof} initial nodal accelerations

	// internal variables
	dof int // degrees of freedom
}

// DoF returns the degrees of freedom of the model described by the ModalInput structure (6*NumNodes).
func (inData *Input) DoF() int {
	if inData.dof == 0 {
		inData.dof = inData.NumNodes * 6
	}
	return inData.dof
}

// Run performs a modal analysis of the model, and returns the dynamic stiffness and mass matricies,
// and the resonant mode-shapes and frequencies.
func (inData *Input) Run(calcAccel bool) (Khat, M, Q1 [][]float64, Fhat, U1, V1, A1, R1 []float64, eqErr, rmsResid float64, ok int, finalErr error) {

	// initialization
	rmsResid = 1                             // root mean square of residual displ. error
	ok = 1                                   // number of (-ve) diag. terms of L D L'
	axialStrainWarning := false              // false: "ok", true: strain > 0.001
	iter := int(0)                           // number of iterations
	U1 = make([]float64, inData.DoF())       // displacement vector
	dU1 := make([]float64, inData.DoF())     // incremental displacement vector
	R1 = make([]float64, inData.DoF())       // total reaction force vector
	dR := make([]float64, inData.DoF())      // incremental reaction force vector
	Q1 = femath.Float2d(inData.NumElems, 12) // end forces for each member

	// base K matrix - elastic stiffness matrix  [K({D}^(i))], {D}^(0)={0} (i=0) */
	K := femath.Float2d(inData.DoF(), inData.DoF()) // basic global stiffness matrix
	err := static.AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L, inData.Le, inData.N1,
		inData.N2, inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy,
		inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q1)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
		}
	}

	// base M matrix
	M = femath.Float2d(inData.DoF(), inData.DoF()) // basic mass matrix
	modal.AssembleM(M, inData.DoF(), inData.NumNodes, inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L,
		inData.N1, inData.N2, inData.Ax, inData.Jx, inData.Iy, inData.Iz, inData.ElemRoll,
		inData.ElemDensity, inData.EMs, inData.NMs, inData.NMx, inData.NMy, inData.NMz, inData.LumpFlag)

	if calcAccel {
		inData.A0 = calcInitialAccel(M, K, vectnd.Add1D(inData.ThermalLoads, inData.MechanicalLoads), inData.U0)
	}

	// calculate a0, a1, a2
	a0 := float64(1) / (inData.Beta * inData.Dt * inData.Dt)
	a1 := a0 * inData.Dt
	a2 := (float64(1) / (float64(2) * inData.Beta)) - float64(1)
	a3 := (float64(1) - inData.Alpha) * inData.Dt
	a4 := inData.Alpha * inData.Dt

	// create Khat
	Khat = vectnd.Add2D(K, vectnd.ScalerMult2D(M, a0))

	// create Fadd the term to be added to the force vector
	Fadd := vectnd.MultAy(M, vectnd.Add1D(vectnd.ScalerMult1D(inData.U0, a0), vectnd.Add1D(vectnd.ScalerMult1D(inData.V0, a1), vectnd.ScalerMult1D(inData.A0, a2))))

	// Temperature loads only
	if inData.TempLoads {

		/*  solve {FT} = [K({D=0})] * {DT} */
		err = static.SolveSystem(Khat, dU1, vectnd.Add1D(inData.ThermalLoads, Fadd), dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof, &ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
			}
		}

		// update displacements and reactions
		for i := 0; i < inData.DoF(); i++ {
			// increment {DT} = {0} + {DT} temp.-induced displ */
			if inData.UnreactedDof[i] {
				U1[i] += dU1[i]
			}
			// increment {RT} = {0} + {RT} temp.-induced react */
			if inData.ReactedDof[i] {
				R1[i] += dR[i]
			}
		}

		if inData.Geom { // assemble K = Ke + Kg */
			// compute   {Q}={QT} ... temp.-induced forces     */
			err = static.ElementEndForces(Q1, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
				inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz,
				inData.E, inData.G, inData.ElemRoll, inData.EqFTemp, inData.EqFMech, U1,
				inData.Shear, inData.Geom, &axialStrainWarning)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
				}
			}

			/* assemble temp.-stressed stiffness [K({DT})]     */
			err = static.AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L,
				inData.Le, inData.N1, inData.N2, inData.Ax, inData.Asy, inData.Asz, inData.Jx,
				inData.Iy, inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q1)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
				}
			}
			Khat = vectnd.Add2D(K, vectnd.ScalerMult2D(M, a0))
		}
	}

	// Mechanical loads only
	// ... then apply mechanical loads only, if there are any ... */
	if inData.AnalyzeLoadCase {
		/* incremental displ  =  initial displacements */
		for i := 0; i < inData.DoF(); i++ {
			dU1[i] = inData.U0[i]
		}

		/*  solve {FM} = [K({DT})] * {DM}	*/
		err = static.SolveSystem(Khat, dU1, vectnd.Add1D(inData.MechanicalLoads, Fadd), dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof, &ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
			}
		}

		for i := 0; i < inData.DoF(); i++ {
			/* combine {D} = {DT} + {DM}	*/
			if inData.UnreactedDof[i] {
				U1[i] += dU1[i]
			} else {
				dU1[i] = 0.0
			}
			// combine {R} = {RT} + {RM} --- for linear systems
			if inData.ReactedDof[i] {
				R1[i] += dR[i]
			}
		}
	}

	// combine thermal and mechanical {F} = {FT} + {FM}
	Fhat = vectnd.Add1D(inData.ThermalLoads, vectnd.Add1D(inData.MechanicalLoads, Fadd))
	// fmt.Println("Thermal", inData.ThermalLoads)
	// fmt.Println("Mechanical", inData.MechanicalLoads)
	// fmt.Println("Fadd", Fadd)
	//  element forces {Q} for displacements {D}
	err = static.ElementEndForces(Q1, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
		inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz,
		inData.E, inData.G, inData.ElemRoll, inData.EqFTemp, inData.EqFMech, U1, inData.Shear, inData.Geom,
		&axialStrainWarning)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
		}
	}

	// check the equilibrium error
	dFhat := make([]float64, inData.DoF())
	// equilibrium error  {dF} = {F} - [K]{D} in nonlinear anlys
	eqErr = static.EquilibriumError(dFhat, Fhat, Khat, U1, inData.DoF(), inData.UnreactedDof, inData.ReactedDof)

	A1, V1 = calcVandA(inData.U0, inData.V0, inData.A0, U1, a0, a1, a2, a3, a4)
	if !inData.Geom {
		//Done if Geom is false
		return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
	}
	// quasi Newton-Raphson iteration for geometric nonlinearity
	//	fmt.Printf("U1 = %10.5g, eqErr = %10.5g, rmsResid = %10.5g, iter = %d, satic tolerance = %10.5g, ok = %d\n", U1[9], eqErr, rmsResid, iter, inData.StaticTol, ok)
	eqErr = 1.0
	ok = 0
	iter = 0

	for eqErr > inData.StaticTol && iter < 500 && ok >= 0 {
		iter++
		// assemble stiffness matrix [K({D}^(i))]
		err = static.AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L, inData.Le, inData.N1, inData.N2, inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q1)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
			}
		}
		Khat = vectnd.Add2D(K, vectnd.ScalerMult2D(M, a0))

		//  compute equilibrium error, {dF}, at iteration i
		//  {dF}^(i) = {F} - [K({D}^(i))]*{D}^(i)
		//  convergence criteria = || {dF}^(i) ||  /  || F ||
		eqErr = static.EquilibriumError(dFhat, Fhat, Khat, U1, inData.DoF(), inData.UnreactedDof, inData.ReactedDof)
		//  Powell-Symmetric-Broyden secant stiffness update
		// 0PSB_update ( Ks, dF, dU1, inData.DoF() );  /* not helpful?
		//  solve {dF}^(i) = [K({D}^(i))] * {dD}^(i)
		err = static.SolveSystem(Khat, dU1, dFhat, dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof, &ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
			}
		}

		if ok < 0 {
			// K is not positive definite
			finalErr = errcat.AppendErrStr(finalErr, "RunNewmarkBetaAnalysis, The stiffness matrix is not pos-def. "+
				"Reduce loads and re-run the analysis.")
			break
		}

		// increment {D}^(i+1) = {D}^(i) + {dD}^(i)
		for i := 0; i < inData.DoF(); i++ {
			if inData.UnreactedDof[i] {
				U1[i] += dU1[i]
			}
		}

		// element forces {Q} for displacements {D}^(i)
		err = static.ElementEndForces(Q1, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
			inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz, inData.E, inData.G,
			inData.ElemRoll, inData.EqFTemp, inData.EqFMech, U1, inData.Shear, inData.Geom,
			&axialStrainWarning)
		finalErr = errcat.AppendErr(finalErr, err)
		if errcat.IsErr(finalErr) {
			return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr
		}

		A1, V1 = calcVandA(inData.U0, inData.V0, inData.A0, U1, a0, a1, a2, a3, a4)
	}
	// end quasi Newton-Raphson iteration

	static.ComputeReactionForces(R1, Fhat, Khat, U1, inData.DoF(), inData.ReactedDof)
	// calcualte V1 and A1

	return Khat, M, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, finalErr

}

func calcVandA(U0, V0, A0, U1 []float64, a0, a1, a2, a3, a4 float64) (A1, V1 []float64) {
	A1 = vectnd.Subtract1D(vectnd.Subtract1D(vectnd.ScalerMult1D(vectnd.Subtract1D(U1, U0), a0), vectnd.ScalerMult1D(V0, a1)), vectnd.ScalerMult1D(A0, a2))
	V1 = vectnd.Add1D(vectnd.Add1D(V0, vectnd.ScalerMult1D(A0, a3)), vectnd.ScalerMult1D(A1, a4))
	return A1, V1
}

func calcInitialAccel(M, K [][]float64, F0, U0 []float64) (A0 []float64) {
	dim := len(M)
	b := vectnd.Subtract1D(F0, vectnd.MultAy(K, U0))
	B := vectnd.Make2D(dim, 1)
	for i, num := range b {
		B[i][0] = num
	}
	temp := vectnd.Make2D(dim, 1)
	ok := false
	femath.InvAB(M, B, dim, 1, temp, &ok)
	A0 = vectnd.Get2DColAs1D(temp, 0)
	return A0
}
