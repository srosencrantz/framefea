/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package transform

import (
	"bitbucket.org/srosencrantz/framefea/femath"
	"math"
)

// CoordTrans calculates the 9 elements of the block-diagonal 12-by-12
// coordinate transformation matrix, t1, t2, ..., t9.
// These coordinate transformation factors are used to:
// * transform frame element end forces from the element (local) coordinate system
// to the structral (global) coordinate system.
// * transfrom end displacements from the structural (global) coordinate system
// to the element (local) coordinate system,
// * transform the frame element stiffness and mass matrices
// from element (local) coordinates to structral (global) coordinates.
// Element matrix coordinate transformations are carried out by function ATMA
// in framefea.c
// Currently coordinate transformations do not consider the effect of
// finite node sizes ... this needs work, and could require a substantial
// re-write of much of the code.
// Currently the effect of finite node sizes is used only in the calculation
// of the element stiffness matrices.
// 	COORDTRANS -  evaluate the 3D coordinate transformation coefficients 1dec04
// 	Default order of coordinate rotations...  typical for Y as the vertical axis
// 	1. rotate about the global Z axis
// 	2. rotate about the global Y axis
// 	3. rotate about the local  x axis --- element 'roll'
// 	If Zvert is defined as 1, then the order of coordinate rotations is typical
// 	for Z as the vertical axis
// 	1. rotate about the global Y axis
// 	2. rotate about the global Z axis
// 	3. rotate about the local  x axis --- element 'roll'
// 	Q=TF;   U=TD;   T'T=I;   Q=kU;   TF=kTD;   T'TF=T'kTD;   T'kT = K;   F=KD
// 	vec3 *xyz, 			// XYZ coordinate of all nodes
// 	double L, 			// length of all beam elements
// 	int n1, int n2, 		// node connectivity
// 	double *t1, double *t2, double *t3, double *t4, double *t5,
// 	double *t6, double *t7, double *t8, double *t9, // coord transformation
// 	float p				// the roll angle (radians)
func CoordTrans(xyz []femath.Vec3, L float64, n1, n2 int, t1, t2, t3, t4, t5, t6, t7, t8, t9 *float64, p float64) {
	// Cx, Cy, Cz, den,		/* direction cosines	*/
	Cx := (xyz[n2].X - xyz[n1].X) / L
	Cy := (xyz[n2].Y - xyz[n1].Y) / L
	Cz := (xyz[n2].Z - xyz[n1].Z) / L

	*t1 = 0.0
	*t2 = 0.0
	*t3 = 0.0
	*t4 = 0.0
	*t5 = 0.0
	*t6 = 0.0
	*t7 = 0.0
	*t8 = 0.0
	*t9 = 0.0

	// Cp, Sp;			/* cosine and sine of roll angle */
	Cp := math.Cos(p)
	Sp := math.Sin(p)

	if math.Abs(Cz) == 1.0 {
		*t3 = Cz
		*t4 = -Cz * Sp
		*t5 = Cp
		*t7 = -Cz * Cp
		*t8 = -Sp
	} else {
		den := math.Sqrt(1.0 - Cz*Cz)

		*t1 = Cx
		*t2 = Cy
		*t3 = Cz

		*t4 = (-Cx*Cz*Sp - Cy*Cp) / den
		*t5 = (-Cy*Cz*Sp + Cx*Cp) / den
		*t6 = Sp * den

		*t7 = (-Cx*Cz*Cp + Cy*Sp) / den
		*t8 = (-Cy*Cz*Cp - Cx*Sp) / den
		*t9 = Cp * den
	}
}

// Atma performs the coordinate transformation from local to global     6jan96
// includes effects of a finite node radii, r1 and r2.	    9dec04
// a is the transformation matrix based off of t1...t9
// m is a 12x12 matrix that gets transformed.
// This function is only special in that it creates the transition matrix.
// Need to replace the lame matrix multiplication loops with a function call.
func Atma(t1, t2, t3, t4, t5, t6, t7, t8, t9 float64, m [][]float64, r1, r2 float64) {
	a := femath.Float2d(12, 12)
	ma := femath.Float2d(12, 12)

	for i := 0; i <= 3; i++ {
		a[3*i][3*i] = t1
		a[3*i][3*i+1] = t2
		a[3*i][3*i+2] = t3
		a[3*i+1][3*i] = t4
		a[3*i+1][3*i+1] = t5
		a[3*i+1][3*i+2] = t6
		a[3*i+2][3*i] = t7
		a[3*i+2][3*i+1] = t8
		a[3*i+2][3*i+2] = t9
	}
	/*  effect of finite node radius on coordinate transformation  ... */
	/*  this needs work ... */
	/*
		a[5][1] =  r1*t7;
		a[5][2] =  r1*t8;
		a[5][3] =  r1*t9;
		a[6][1] = -r1*t4;
		a[6][2] = -r1*t5;
		a[6][3] = -r1*t6;

		a[11][7] = -r2*t7;
		a[11][8] = -r2*t8;
		a[11][9] = -r2*t9;
		a[12][7] =  r2*t4;
		a[12][8] =  r2*t5;
		a[12][9] =  r2*t6;
	*/

	for j := 0; j < 12; j++ { /*  MT = M T     */
		for i := 0; i < 12; i++ {
			for k := 0; k < 12; k++ {
				ma[i][j] += m[i][k] * a[k][j]
			}
		}
	}

	for i := 0; i < 12; i++ {
		for j := i; j < 12; j++ {
			m[j][i], m[i][j] = 0.0, 0.0
		}
	}

	for j := 0; j < 12; j++ { /*  T'MT = T' MT */
		for i := 0; i < 12; i++ {
			for k := 0; k < 12; k++ {
				m[i][j] += a[k][i] * ma[k][j]
			}
		}
	}
}
