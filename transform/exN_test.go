/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package transform

import (
	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/testhelp"

	"testing"
)

// TestCoordTransexN_0
func TestCoordTransexN_0b(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(2.12199579145933796095e-314)
	t2 := float64(3.18369004628430812593e-314)
	t3 := float64(2.12199054201185089771e-314)
	t4 := float64(2.12198536420388248144e-314)
	t5 := float64(3.18367841449680808546e-314)
	t6 := float64(0.00000000000000000000e+00)
	t7 := float64(0.00000000000000000000e+00)
	t8 := float64(2.27270197086973410321e-322)
	t9 := float64(3.18405342959056177939e-314)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_1
func TestCoordTransexN_1(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(1.27321997432867499943e-313)
	t2 := float64(2.12198534048873148106e-314)
	t3 := float64(1.27321213528551182388e-313)
	t4 := float64(1.27321990911200974839e-313)
	t5 := float64(2.12419890971871305973e-314)
	t6 := float64(1.27321990911200974839e-313)
	t7 := float64(2.12419890971871305973e-314)
	t8 := float64(2.12198531677358048068e-314)
	t9 := float64(4.94065645841246544177e-324)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_2
func TestCoordTransexN_2(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(-0.00000000000000000000e+00)
	t2 := float64(6.12323399573676603587e-17)
	t3 := float64(1.00000000000000000000e+00)
	t4 := float64(0.00000000000000000000e+00)
	t5 := float64(-1.00000000000000000000e+00)
	t6 := float64(6.12323399573676603587e-17)
	t7 := float64(2.12198535629883214798e-314)
	t8 := float64(1.00000000000000000000e+00)
	t9 := float64(1.27321997946695771618e-313)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_3
func TestCoordTransexN_3(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(1.06099789498857051171e-314)
	t2 := float64(2.12198538396650831509e-314)
	t3 := float64(2.12422607987077480741e-314)
	t4 := float64(2.12198526934327847992e-314)
	t5 := float64(3.80261153692946835543e-311)
	t6 := float64(3.18384161475500801186e-314)
	t7 := float64(0.00000000000000000000e+00)
	t8 := float64(0.00000000000000000000e+00)
	t9 := float64(0.00000000000000000000e+00)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_4
func TestCoordTransexN_4(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(1.27321997432867499943e-313)
	t2 := float64(2.12198534048873148106e-314)
	t3 := float64(1.27321213528551182388e-313)
	t4 := float64(1.27321990911200974839e-313)
	t5 := float64(2.12419890971871305973e-314)
	t6 := float64(1.27321990911200974839e-313)
	t7 := float64(2.12419890971871305973e-314)
	t8 := float64(2.12198531677358048068e-314)
	t9 := float64(4.94065645841246544177e-324)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_5
func TestCoordTransexN_5(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(-0.00000000000000000000e+00)
	t2 := float64(6.12323399573676603587e-17)
	t3 := float64(1.00000000000000000000e+00)
	t4 := float64(0.00000000000000000000e+00)
	t5 := float64(-1.00000000000000000000e+00)
	t6 := float64(6.12323399573676603587e-17)
	t7 := float64(2.12198535629883214798e-314)
	t8 := float64(1.00000000000000000000e+00)
	t9 := float64(1.27321997946695771618e-313)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_6
func TestCoordTransexN_6(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(1.06099789498857051171e-314)
	t2 := float64(2.12198538396650831509e-314)
	t3 := float64(2.12422607987077480741e-314)
	t4 := float64(2.12198526934327847992e-314)
	t5 := float64(3.80260557918008416691e-311)
	t6 := float64(3.18384161475500801186e-314)
	t7 := float64(0.00000000000000000000e+00)
	t8 := float64(0.00000000000000000000e+00)
	t9 := float64(0.00000000000000000000e+00)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_7
func TestCoordTransexN_7(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(6.91691904177745161847e-323)
	t2 := float64(0.00000000000000000000e+00)
	t3 := float64(2.12198530096347981376e-314)
	t4 := float64(-7.57306469012171333195e-29)
	t5 := float64(-4.13547528114630536657e+196)
	t6 := float64(3.18323449404369149623e-314)
	t7 := float64(-4.13547459276089968456e+196)
	t8 := float64(1.28457067918724101486e-322)
	t9 := float64(1.17990265643190496035e-307)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}

// TestCoordTransexN_8
func TestCoordTransexN_8(t *testing.T) {
	xyz := []femath.Vec3{femath.Vec3{X: 0.00000000000000000000e+00, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00},
		femath.Vec3{X: 1.00000000000000000000e+01, Y: 0.00000000000000000000e+00, Z: 0.00000000000000000000e+00}}
	L := float64(1.00000000000000000000e+01)
	n1 := int(0)
	n2 := int(1)
	t1 := float64(2.12198747485232151525e-314)
	t2 := float64(1.27321890121809223225e-313)
	t3 := float64(3.18382183286061417419e-314)
	t4 := float64(6.22522713759970645662e-322)
	t5 := float64(1.27321215346712759084e-313)
	t6 := float64(1.27321215346712759084e-313)
	t7 := float64(2.12198798077554285669e-314)
	t8 := float64(3.18390809029952465990e-314)
	t9 := float64(2.12199054201185089771e-314)
	p := float64(1.57079632679489655800e+00)
	t1Out := float64(1.00000000000000000000e+00)
	t2Out := float64(0.00000000000000000000e+00)
	t3Out := float64(0.00000000000000000000e+00)
	t4Out := float64(-0.00000000000000000000e+00)
	t5Out := float64(6.12323399573676603587e-17)
	t6Out := float64(1.00000000000000000000e+00)
	t7Out := float64(0.00000000000000000000e+00)
	t8Out := float64(-1.00000000000000000000e+00)
	t9Out := float64(6.12323399573676603587e-17)
	CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	testhelp.FloatTest(t, "t1 comparsion", t1, t1Out)
	testhelp.FloatTest(t, "t2 comparsion", t2, t2Out)
	testhelp.FloatTest(t, "t3 comparsion", t3, t3Out)
	testhelp.FloatTest(t, "t4 comparsion", t4, t4Out)
	testhelp.FloatTest(t, "t5 comparsion", t5, t5Out)
	testhelp.FloatTest(t, "t6 comparsion", t6, t6Out)
	testhelp.FloatTest(t, "t7 comparsion", t7, t7Out)
	testhelp.FloatTest(t, "t8 comparsion", t8, t8Out)
	testhelp.FloatTest(t, "t9 comparsion", t9, t9Out)
}
