#!/bin/bash

rm -r bin
mkdir bin
mkdir bin/win64
mkdir bin/lin64
mkdir bin/mac64

tools="framefea"

for i in $tools ; do
    echo ${i}...
    cd $i
    go install
    go build -v
    mv ${i} ../bin/lin64/
    env GOOS=windows GOARCH=amd64 go build
    mv ${i}.exe ../bin/win64/
    env GOOS=darwin GOARCH=amd64 go build
    mv ${i} ../bin/mac64/
    cd .. 
done



