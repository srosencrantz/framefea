/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/inputlib"
	"bitbucket.org/srosencrantz/framefea/outputlib"
)

func main() {
	// command line flag stuff
	var inFilename string
	var outFilename string
	var calcAccel bool
	var cpuProfile string
	var toolName = "FrameFEA"
	var verbose int
	flag.StringVar(&inFilename, "in", "not_present", "-in=inputfile.csv is required")
	flag.StringVar(&outFilename, "out", "not_present", "-out=outputFilename is required")
	flag.BoolVar(&calcAccel, "acc", false, "-acc=true causes initial acceleration at each time step to be calculated (Optional) (Default false)")
	flag.StringVar(&cpuProfile, "cpuprofile", "", "-cpuprofile write cpu profile to file")
	flag.IntVar(&verbose, "V", -1, "-V=verbose modal output, 0 off, 1 include mode shapes, 2 Stiffness & Mass matricies as Golang literal [][]float64")
	flag.Parse()

	// check for required flags
	if (inFilename == "not_present") || (outFilename == "not_present") {
		flag.Usage()
	}

	// turn on profiling if requested
	if cpuProfile != "" {
		fp, err := os.Create(cpuProfile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(fp)
		defer pprof.StopCPUProfile()
	}

	//read in the model file
	log.Println("Reading model file")
	var fd *inputlib.FrameData
	var err error
	fd, _, err = inputlib.ReadFile(inFilename)
	if err != nil {
		log.Fatal(err)
	}

	var output *outputlib.Output
	switch {
	case fd.PerformStaticAnalysis:
		output = RunStaticAnalysis(fd)
	case fd.PerformModalAnalysis:
		output = RunModalAnalysis(fd)
	case fd.PerformNewmarkBetaAnalysis:
		RunNewmarkBetaAnalysis(fd, outFilename, toolName, verbose, calcAccel)
	case fd.PerformCondensation:
		output = RunCondensation(fd)
	default:
		log.Println("No analysis was specified")
	}

	if !fd.PerformNewmarkBetaAnalysis {
		WriteOutput(output, outFilename, toolName, verbose)
	}

	log.Println("Done")
}

// WriteOutput writes the output file
func WriteOutput(output *outputlib.Output, outFilename, toolName string, verbose int) {
	if output == nil {
		return
	}
	log.Println("Write Output")
	err := output.WriteFile(outFilename, toolName, verbose)
	if err != nil {
		log.Fatal(err)
	}
}

// RunStaticAnalysis runs a static analysis
func RunStaticAnalysis(fd *inputlib.FrameData) (output *outputlib.Output) {
	log.Println("Running Static Analysis")

	_, Q, F, D, R, eqErr, rmsResid, ok, err := fd.NewStaticInput().Run()
	err = checkErrors(err, "Static Analysis")

	output = &outputlib.Output{}
	output.AddStatic(F, D, R, Q, eqErr, rmsResid, ok, *fd)
	return output
}

// RunModalAnalysis runs a modal analysis
func RunModalAnalysis(fd *inputlib.FrameData) (output *outputlib.Output) {
	log.Println("Running Modal Analysis")

	K, _, _, _, _, _, _, _, err := fd.NewStaticInput().Run()
	err = checkErrors(err, "Static Analysis")

	Kd, Md, M, V, f, iter, err := fd.NewModalInput().Run(K)
	err = checkErrors(err, "Modal Analysis")

	output = &outputlib.Output{}
	output.AddModal(Kd, Md, M, V, f, iter)
	return output
}

// RunCondensation creates reduced order stiffness and mass matrices
func RunCondensation(fd *inputlib.FrameData) (output *outputlib.Output) {
	log.Println("Running Condensation")

	K, _, _, _, _, _, _, _, err := fd.NewStaticInput().Run()
	err = checkErrors(err, "Static Analysis")

	_, _, M, V, f, _, err := fd.NewModalInput().Run(K)
	err = checkErrors(err, "Modal Analysis")

	Kc, Mc, err := fd.NewCondensationInput().Run(K, M, V, f)
	err = checkErrors(err, "Condensation")

	output = &outputlib.Output{}
	output.AddCondensation(Kc, Mc)
	return output
}

// RunNewmarkBetaAnalysis runs a Newmark-Beta analysis
func RunNewmarkBetaAnalysis(fd *inputlib.FrameData, outFilename, toolName string, verbose int, calcAccel bool) {
	log.Println("Running Newmark-Beta Analysis")

	if fd.NBTermTime <= 0 {
		fd.NBTermTime = fd.NBDt // only run one time step
	}

	for time, timeIter := 0.0, 0; time < fd.NBTermTime; time, timeIter = time+fd.NBDt, timeIter+1 {

		_, _, Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, err := fd.NewNewmarkBetaInput().Run(calcAccel)
		err = checkErrors(err, "Newmark Beta Analysis")

		output := &outputlib.Output{}
		output.AddNewmarkBeta(Q1, Fhat, U1, V1, A1, R1, eqErr, rmsResid, ok, fd)
		log.Printf("Writing Output Time %f, Iteration: %d \n", time, timeIter)
		WriteOutput(output, outFilename+fmt.Sprintf("_%d", timeIter), toolName, verbose)

		fd.Dp = U1
		fd.Vi = V1
		fd.Ai = A1
	}
}

func checkErrors(err error, label string) (newError error) {
	if errcat.IsErr(err) {
		log.Fatal(err)
	}
	if err != nil {
		log.Println(label + " warning and info messages:")
		fmt.Println(err)
	}
	return newError
}
