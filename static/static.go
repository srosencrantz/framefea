/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package static

import (
	"fmt"
	"math"

	"bitbucket.org/srosencrantz/errcat"
	"bitbucket.org/srosencrantz/framefea/femath"
	"bitbucket.org/srosencrantz/framefea/transform"
)

// Input is the data required for a static frame structural analysis.
type Input struct {
	// Dof = 6*nN
	// Run Info
	Shear bool // indicates shear deformation
	Geom  bool // indicates  geometric nonlinearity

	// Node Info
	NumNodes   int           // number of Nodes
	Xyz        []femath.Vec3 // {NumNodes} X,Y,Z node coordinates (global)
	NodeRadius []float64     // {NumNodes} node size radius, for finite sizes

	// Reaction Info
	ReactedDof   []bool // {Dof} Dof's with reactions formally "r"
	UnreactedDof []bool // {Dof} Dof's without reactions formally "q" (calculated)

	// Element Info
	NumElems     int       // Number of Frame Elements
	N1, N2       []int     // {NumElems} begin and end node numbers
	Ax, Asy, Asz []float64 // {NumElems} cross section areas, incl. shear
	Jx, Iy, Iz   []float64 // {NumElems} section inertias
	E, G         []float64 // {NumElems} elastic modulus, shear modulus
	ElemRoll     []float64 // {NumElems} roll of each member, radians

	L  []float64 //  node-to-node length of each element (calculated)
	Le []float64 //  effective length, accounts for node size (calculated)

	// Load Info
	StaticTol       float64   // Convergence tolerance for the static analysis when Geom == true
	AnalyzeLoadCase bool      //  Are there mechanical loads in Mechanical Loads
	MechanicalLoads []float64 // {Dof} mechanical load vectors, all load cases
	Dp              []float64 // {Dof} prescribed node displacements

	TempLoads    bool      // Are there temperature loads in the load case
	ThermalLoads []float64 // {Dof} thermal load vectors, all load cases

	EqFMech [][]float64 //  {numElems}{12} equivalent end forces from mech loads global (calculated)
	EqFTemp [][]float64 //  {numElems}{12} equivalent end forces from temp loads global (calculated)

	// internal variables
	dof int // degrees of freedom

}

// DoF returns the number of degrees of freedom represented by the StaticInput structure.
func (inData *Input) DoF() int {
	if inData.dof == 0 {
		inData.dof = inData.NumNodes * 6
	}
	return inData.dof
}

// Run performs a static analysis on the input data.
func (inData *Input) Run() (K, Q [][]float64, F, D, R []float64, eqErr, rmsResid float64, ok int, finalErr error) {

	K = femath.Float2d(inData.DoF(), inData.DoF()) /* global stiffness matrix	*/

	// initialize internal element end forces Q = {0}	*/
	Q = femath.Float2d(inData.NumElems, 12) /* end forces for each member	*/

	// elastic stiffness matrix  [K({D}^(i))], {D}^(0)={0} (i=0) */
	err := AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L, inData.Le, inData.N1,
		inData.N2, inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy,
		inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return K, Q, nil, nil, nil, eqErr, rmsResid, ok, finalErr
		}
	}

	if !inData.TempLoads && !inData.AnalyzeLoadCase {
		return K, nil, nil, nil, nil, 0.0, 0.0, 0, finalErr
	}

	rmsResid = float64(1.0)     // root mean square of residual displ. error
	ok = int(1)                 // number of (-ve) diag. terms of L D L'
	axialStrainWarning := false // false: "ok", true: strain > 0.001
	iter := int(0)              // number of iterations

	// initialize displacements and displ. increment to {0}
	D = make([]float64, inData.DoF())   // displacement vector
	dD := make([]float64, inData.DoF()) // incremental displacement vector

	// initialize reactions and react. increment to {0}
	R = make([]float64, inData.DoF())   // total reaction force vector
	dR := make([]float64, inData.DoF()) // incremental reaction force vector

	/* first apply temperature loads only, if there are any ... */
	if inData.TempLoads {

		/*  solve {FT} = [K({D=0})] * {DT} */
		err = SolveSystem(K, dD, inData.ThermalLoads, dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof,
			&ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return K, Q, nil, D, R, eqErr, rmsResid, ok, finalErr
			}
		}

		// increment {DT} = {0} + {DT} temp.-induced displ */
		for i := 0; i < inData.DoF(); i++ {
			if inData.UnreactedDof[i] {
				D[i] += dD[i]
			}
		}

		// increment {RT} = {0} + {RT} temp.-induced react */
		for i := 0; i < inData.DoF(); i++ {
			if inData.ReactedDof[i] {
				R[i] += dR[i]
			}
		}

		if inData.Geom { // assemble K = Ke + Kg */
			// compute   {Q}={QT} ... temp.-induced forces     */
			err = ElementEndForces(Q, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
				inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz,
				inData.E, inData.G, inData.ElemRoll, inData.EqFTemp, inData.EqFMech, D,
				inData.Shear, inData.Geom, &axialStrainWarning)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return K, Q, nil, D, R, eqErr, rmsResid, ok, finalErr
				}
			}

			/* assemble temp.-stressed stiffness [K({DT})]     */
			err = AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L,
				inData.Le, inData.N1, inData.N2, inData.Ax, inData.Asy, inData.Asz, inData.Jx,
				inData.Iy, inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return K, Q, nil, D, R, eqErr, rmsResid, ok, finalErr
				}
			}
		}
	}

	// ... then apply mechanical loads only, if there are any ... */
	if inData.AnalyzeLoadCase {

		/* incremental displ at react'ns = prescribed displ */
		for i := 0; i < inData.DoF(); i++ {
			if inData.ReactedDof[i] {
				dD[i] = inData.Dp[i]
			}
		}

		/*  solve {FM} = [K({DT})] * {DM}	*/
		err = SolveSystem(K, dD, inData.MechanicalLoads, dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof, &ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return K, Q, nil, D, R, eqErr, rmsResid, ok, finalErr
			}
		}

		/* combine {D} = {DT} + {DM}	*/
		for i := 0; i < inData.DoF(); i++ {
			if inData.UnreactedDof[i] {
				D[i] += dD[i]
			} else {
				D[i] = inData.Dp[i]
				dD[i] = 0.0
			}
		}

		// combine {R} = {RT} + {RM} --- for linear systems
		for i := 0; i < inData.DoF(); i++ {
			if inData.ReactedDof[i] {
				R[i] += dR[i]
			}
		}
	}

	//  combine {F} = {FT} + {FM}
	F = make([]float64, inData.DoF())

	for i := 0; i < inData.DoF(); i++ {
		F[i] = inData.ThermalLoads[i] + inData.MechanicalLoads[i]
	}

	//  element forces {Q} for displacements {D}
	err = ElementEndForces(Q, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
		inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz,
		inData.E, inData.G, inData.ElemRoll, inData.EqFTemp, inData.EqFMech, D, inData.Shear, inData.Geom,
		&axialStrainWarning)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return K, Q, F, D, R, eqErr, rmsResid, ok, finalErr
		}
	}

	/*  check the equilibrium error	*/
	dF := make([]float64, inData.DoF()) // equilibrium error  {dF} = {F} - [K]{D} in nonlinear anlys
	eqErr = EquilibriumError(dF, F, K, D, inData.DoF(), inData.UnreactedDof, inData.ReactedDof)

	/* quasi Newton-Raphson iteration for geometric nonlinearity  */
	if inData.Geom { /* re-initialize */
		eqErr = 1.0
		ok = 0
		iter = 0
	}

	posDefErr := false
	for inData.Geom && eqErr > inData.StaticTol && iter < 500 && ok >= 0 {
		iter++
		/*  assemble stiffness matrix [K({D}^(i))]	      */
		err = AssembleK(K, inData.DoF(), inData.NumElems, inData.Xyz, inData.NodeRadius, inData.L, inData.Le, inData.N1, inData.N2,
			inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy,
			inData.Iz, inData.E, inData.G, inData.ElemRoll, inData.Shear, inData.Geom, Q)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return K, Q, F, D, R, eqErr, rmsResid, ok, finalErr
			}
		}

		//  compute equilibrium error, {dF}, at iteration i
		//  {dF}^(i) = {F} - [K({D}^(i))]*{D}^(i)
		//  convergence criteria = || {dF}^(i) ||  /  || F ||
		eqErr = EquilibriumError(dF, F, K, D, inData.DoF(), inData.UnreactedDof, inData.ReactedDof)

		//  Powell-Symmetric-Broyden secant stiffness update
		// 0PSB_update ( Ks, dF, dD, inData.DoF() );  /* not helpful?
		//  solve {dF}^(i) = [K({D}^(i))] * {dD}^(i)
		err = SolveSystem(K, dD, dF, dR, inData.DoF(), inData.UnreactedDof, inData.ReactedDof, &ok, &rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return K, Q, F, D, R, eqErr, rmsResid, ok, finalErr
			}
		}

		if ok < 0 { /*  K is not positive definite	      */
			posDefErr = true
			finalErr = errcat.AppendErrStr(finalErr, "RunStaticAnalysis, The stiffness matrix is not pos-def. "+
				"Reduce loads and re-run the analysis.")
			break
		}

		/*  increment {D}^(i+1) = {D}^(i) + {dD}^(i)	      */
		for i := 0; i < inData.DoF(); i++ {
			if inData.UnreactedDof[i] {
				D[i] += dD[i]
			}
		}

		/*  element forces {Q} for displacements {D}^(i)      */
		err = ElementEndForces(Q, inData.NumElems, inData.Xyz, inData.L, inData.Le, inData.N1, inData.N2,
			inData.Ax, inData.Asy, inData.Asz, inData.Jx, inData.Iy, inData.Iz, inData.E, inData.G,
			inData.ElemRoll, inData.EqFTemp, inData.EqFMech, D, inData.Shear, inData.Geom,
			&axialStrainWarning)

		finalErr = errcat.AppendErr(finalErr, err)

		if errcat.IsErr(finalErr) {
			return K, Q, F, D, R, eqErr, rmsResid, ok, finalErr
		}
	} /* end quasi Newton-Raphson iteration */

	//TODO - logical checks below don't make any sense.
	if axialStrainWarning && !posDefErr {
		if posDefErr {
			finalErr = errcat.AppendWarnStr(finalErr, "Strain limit and buckling failure.")
		} else {
			finalErr = errcat.AppendWarnStr(finalErr, "Strain limit failure.")
		}
	}

	if inData.Geom {
		ComputeReactionForces(R, F, K, D, inData.DoF(), inData.ReactedDof)
	}

	return K, Q, F, D, R, eqErr, rmsResid, ok, finalErr

}

// AssembleK assembles the global stiffness matrix from individual elements 23feb94
// 	K, stiffness matrix
// 	DoF, number of degrees of freedom
// 	nE,	number of frame elements
// 	xyz, XYZ locations of every node
// 	r, rigid radius of every node
// 	L, double *Le,	 length of each frame element, effective
// 	N1, int *N2,	 node connectivity
// 	Ax, float *Asy, float *Asz,	 section areas
// 	Jx, float *Iy, float *Iz,	 section inertias
// 	E, float *G,	 elastic and shear moduli
// 	p, roll angle, radians
// 	shear, true: include shear deformation, false: don't
// 	geom, true: include goemetric stiffness, false: don't
// 	Q, frame element end forces
func AssembleK(K [][]float64, DoF, nE int, xyz []femath.Vec3, r, L, Le []float64, N1, N2 []int, Ax, Asy, Asz, Jx, Iy, Iz, E, G, p []float64, shear, geom bool, Q [][]float64) (finalErr error) {
	//var res int
	var i, j, ii, jj, l, ll int
	k := femath.Float2d(12, 12) /* element stiffness matrix in global coord */
	ind := femath.Int2d(12, nE) /* member-structure DoF index table	*/

	for i = 0; i < DoF; i++ {
		for j = 0; j < DoF; j++ {
			K[i][j] = 0.0
		}
	}

	for i = 0; i < nE; i++ {
		ind[0][i] = 6 * N1[i]
		ind[6][i] = 6 * N2[i]
		ind[1][i] = ind[0][i] + 1
		ind[7][i] = ind[6][i] + 1
		ind[2][i] = ind[0][i] + 2
		ind[8][i] = ind[6][i] + 2
		ind[3][i] = ind[0][i] + 3
		ind[9][i] = ind[6][i] + 3
		ind[4][i] = ind[0][i] + 4
		ind[10][i] = ind[6][i] + 4
		ind[5][i] = ind[0][i] + 5
		ind[11][i] = ind[6][i] + 5
	}

	for i = 0; i < nE; i++ {
		err := elasticK(k, xyz, r, L[i], Le[i], N1[i], N2[i], Ax[i], Asy[i], Asz[i], Jx[i], Iy[i], Iz[i], E[i], G[i], p[i], shear)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return finalErr
			}
		}
		if geom {
			err := geometricK(k, xyz, r, L[i], Le[i], N1[i], N2[i], Ax[i], Asy[i], Asz[i], Jx[i], Iy[i], Iz[i], E[i],
				G[i], p[i], -Q[i][0], shear)
			if err != nil {
				if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
					return finalErr
				}
			}
		}

		for l = 0; l < 12; l++ {
			ii = ind[l][i]
			for ll = 0; ll < 12; ll++ {
				jj = ind[ll][i]
				K[ii][jj] += k[l][ll]
			}
		}
	}
	return finalErr
}

//  elasticK, space frame elastic stiffness matrix in global coordnates	22oct02
func elasticK(k [][]float64, xyz []femath.Vec3, r []float64, L, Le float64, n1, n2 int, Ax, Asy, Asz, J, Iy, Iz, E, G, p float64, shear bool) (finalErr error) {
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* coord Xformn */
	var Ksy, Ksz float64                           /* shear deformatn coefficients	*/
	var i, j int

	transform.CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)

	for i = 0; i < 12; i++ {
		for j = 0; j < 12; j++ {
			k[i][j] = 0.0
		}
	}

	if shear {
		Ksy = 12. * E * Iz / (G * Asy * Le * Le)
		Ksz = 12. * E * Iy / (G * Asz * Le * Le)
	} else {
		Ksy, Ksz = 0.0, 0.0
	}

	k[6][6] = E * Ax / Le
	k[0][0] = k[6][6]
	k[7][7] = 12. * E * Iz / (Le * Le * Le * (1. + Ksy))
	k[1][1] = k[7][7]
	k[8][8] = 12. * E * Iy / (Le * Le * Le * (1. + Ksz))
	k[2][2] = k[8][8]
	k[9][9] = G * J / Le
	k[3][3] = k[9][9]
	k[10][10] = (4. + Ksz) * E * Iy / (Le * (1. + Ksz))
	k[4][4] = k[10][10]
	k[11][11] = (4. + Ksy) * E * Iz / (Le * (1. + Ksy))
	k[5][5] = k[11][11]
	k[2][4] = -6. * E * Iy / (Le * Le * (1. + Ksz))
	k[4][2] = k[2][4]
	k[1][5] = 6. * E * Iz / (Le * Le * (1. + Ksy))
	k[5][1] = k[1][5]
	k[6][0], k[0][6] = -k[0][0], -k[0][0]
	k[11][7], k[7][11], k[7][5], k[5][7] = -k[5][1], -k[5][1], -k[5][1], -k[5][1]
	k[10][8], k[8][10], k[8][4], k[4][8] = -k[4][2], -k[4][2], -k[4][2], -k[4][2]
	k[9][3], k[3][9] = -k[3][3], -k[3][3]
	k[10][2], k[2][10] = k[4][2], k[4][2]
	k[11][1], k[1][11] = k[5][1], k[5][1]
	k[7][1], k[1][7] = -k[1][1], -k[1][1]
	k[8][2], k[2][8] = -k[2][2], -k[2][2]
	k[4][10] = (2. - Ksz) * E * Iy / (Le * (1. + Ksz))
	k[10][4] = k[4][10]
	k[5][11] = (2. - Ksy) * E * Iz / (Le * (1. + Ksy))
	k[11][5] = k[5][11]

	transform.Atma(t1, t2, t3, t4, t5, t6, t7, t8, t9, k, r[n1], r[n2]) /* globalize */

	/* check and enforce symmetry of elastic element stiffness matrix */

	for i = 0; i < 12; i++ {
		for j = i + 1; j < 12; j++ {
			if k[i][j] != k[j][i] {
				if math.Abs(k[i][j]/k[j][i]-1.0) > 1.0e-6 &&
					(math.Abs(k[i][j]/k[i][i]) > 1e-6 ||
						math.Abs(k[j][i]/k[i][i]) > 1e-6) {
					finalErr = errcat.AppendWarnStr(finalErr, fmt.Sprintf("elasticK, element stiffness matrix not symetric\n"+
						" ... k[%d][%d] = %15.6e \n"+" ... k[%d][%d] = %15.6e   "+" ... relative error = %e \n"+
						" ... element matrix saved in file 'kt'\n", i, j, k[i][j], j, i, k[j][i], math.Abs(k[i][j]/k[j][i]-1.0)))
				}
				k[j][i] = 0.5 * (k[i][j] + k[j][i])
				k[i][j] = k[j][i]
			}
		}
	}
	return finalErr
}

// geometricK, space frame geometric stiffness matrix, global coordnates 20dec07
func geometricK(k [][]float64, xyz []femath.Vec3, r []float64, L, Le float64, n1, n2 int, Ax, Asy, Asz, J, Iy, Iz, E, G, p, T float64, shear bool) (finalErr error) {
	var i, j int
	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* coord Xformn */
	var Ksy, Ksz, Dsy, Dsz float64                 /* shear deformation coefficients	*/
	kg := femath.Float2d(12, 12)

	transform.CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)
	if shear {
		Ksy = 12. * E * Iz / (G * Asy * Le * Le)
		Ksz = 12. * E * Iy / (G * Asz * Le * Le)
		Dsy = (1 + Ksy) * (1 + Ksy)
		Dsz = (1 + Ksz) * (1 + Ksz)
	} else {
		Ksz = 0.0
		Ksy = Ksz
		Dsz = 1.0
		Dsy = Dsz
	}

	kg[0][0], kg[6][6] = 0.0, 0.0 // T/L;

	kg[7][7] = T / L * (1.2 + 2.0*Ksy + Ksy*Ksy) / Dsy
	kg[1][1] = kg[7][7]
	kg[8][8] = T / L * (1.2 + 2.0*Ksz + Ksz*Ksz) / Dsz
	kg[2][2] = kg[8][8]
	kg[9][9] = T / L * J / Ax
	kg[3][3] = kg[9][9]
	kg[10][10] = T * L * (2.0/15.0 + Ksz/6.0 + Ksz*Ksz/12.0) / Dsz
	kg[4][4] = kg[10][10]
	kg[11][11] = T * L * (2.0/15.0 + Ksy/6.0 + Ksy*Ksy/12.0) / Dsy
	kg[5][5] = kg[11][11]

	kg[6][0] = 0.0 // -T/L;
	kg[0][6] = kg[6][0]

	kg[2][10] = -T / 10.0 / Dsz
	kg[4][2], kg[2][4], kg[10][2] = kg[2][10], kg[2][10], kg[2][10]
	kg[8][10] = T / 10.0 / Dsz
	kg[8][4], kg[4][8], kg[10][8] = kg[8][10], kg[8][10], kg[8][10]
	kg[1][11] = T / 10.0 / Dsy
	kg[5][1], kg[1][5], kg[11][1] = kg[1][11], kg[1][11], kg[1][11]
	kg[7][11] = -T / 10.0 / Dsy
	kg[7][5], kg[5][7], kg[11][7] = kg[7][11], kg[7][11], kg[7][11]

	kg[3][9], kg[9][3] = -kg[3][3], -kg[3][3]

	kg[1][7] = -T / L * (1.2 + 2.0*Ksy + Ksy*Ksy) / Dsy
	kg[7][1] = kg[1][7]
	kg[2][8] = -T / L * (1.2 + 2.0*Ksz + Ksz*Ksz) / Dsz
	kg[8][2] = kg[2][8]

	kg[4][10] = -T * L * (1.0/30.0 + Ksz/6.0 + Ksz*Ksz/12.0) / Dsz
	kg[10][4] = kg[4][10]
	kg[5][11] = -T * L * (1.0/30.0 + Ksy/6.0 + Ksy*Ksy/12.0) / Dsy
	kg[11][5] = kg[5][11]

	transform.Atma(t1, t2, t3, t4, t5, t6, t7, t8, t9, kg, r[n1], r[n2]) /* globalize */

	/* check and enforce symmetry of geometric element stiffness matrix */
	for i = 0; i < 12; i++ {
		for j = i + 1; j < 12; j++ {
			if kg[i][j] != kg[j][i] {
				if math.Abs(kg[i][j]/kg[j][i]-1.0) > 1.0e-6 &&
					(math.Abs(kg[i][j]/kg[i][i]) > 1.0e-6 || math.Abs(kg[j][i]/kg[i][i]) > 1.0e-6) {
					finalErr = errcat.AppendErrStr(finalErr, fmt.Sprintf("geometric_K element stiffness matrix not symetric ...\n"+
						" ... kg[%d][%d] = %15.6e \n"+" ... kg[%d][%d] = %15.6e   "+" ... relative error = %e \n"+
						" ... element matrix saved in file 'kg'\n", i, j, kg[i][j], j, i, kg[j][i], math.Abs(kg[i][j]/kg[j][i]-1.0)))
				}
				kg[j][i] = 0.5 * (kg[i][j] + kg[j][i])
				kg[i][j] = kg[j][i]
			}
		}
	}

	/* add geometric stiffness matrix to elastic stiffness matrix ... */
	for i = 0; i < 12; i++ {
		for j = 0; j < 12; j++ {
			k[i][j] += kg[i][j]
		}
	}
	return finalErr
}

// SolveSystem  solve {F} =   [K]{D} via L D L' decomposition        27dec01
// Prescribed displacements are "mechanical loads" not "temperature loads"
// 	K, stiffness matrix for the restrained frame
// 	D, displacement vector to be solved
// 	F, external load vector
// 	R, reaction vector
// 	DoF, number of degrees of freedom
// 	q, true: not a reaction; false: a reaction coordinate
// 	r, false: not a reaction; true: a reaction coordinate
// 	ok, indicates positive definite stiffness matrix
// 	rmsResid /**< the RMS error of the solution residual
func SolveSystem(K [][]float64, D, F, R []float64, DoF int, q, r []bool, ok *int, rmsResid *float64) (finalErr error) {
	diag := make([]float64, DoF) // diagonal vector of the L D L' decomp.
	var err error
	var mproveOk bool

	/* L D L' decomposition of K[q,q] into lower triangle of K[q,q] and diag[q]
	vectors F and D are unchanged */
	err = femath.LdlDcmpPm(K, DoF, diag, F, D, R, q, r, true, false, ok)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	if *ok < 0 {
		return errcat.AppendErrStr(finalErr, fmt.Sprintf("solveSystem, make sure that all six rigid body transations are restrained."))
	}

	/* LDL'  back-substitution for D[q] and R[r] */
	err = femath.LdlDcmpPm(K, DoF, diag, F, D, R, q, r, false, true, ok)
	if err != nil {
		if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
			return finalErr
		}
	}

	*rmsResid, mproveOk = 1, true
	for mproveOk { /* improve solution for D[q] and R[r] */
		mproveOk, err = femath.LdlMprovePm(K, DoF, diag, F, D, R, q, r, rmsResid)
		if err != nil {
			if finalErr = errcat.AppendErr(finalErr, err); errcat.IsErr(finalErr) {
				return finalErr
			}
		}
	}
	return finalErr
}

// EquilibriumError compute {dF_q} =   {F_q} - [K_qq]{D_q} - [K_qr]{DR}
// use only the upper-triangle of [K_qq]
// return ||dF||/||F||
// 2014-05-16
// dF, equilibrium error  {dF} = {F} - [K]{D}
// F,	load vector
// K,	stiffness matrix for the restrained frame
// D,	displacement vector to be solved
// DoF,	number of degrees of freedom
// q, true: not a reaction; false: a reaction coordinate
// r, false: not a reaction; true: a reaction coordinate
func EquilibriumError(dF, F []float64, K [][]float64, D []float64, DoF int, q, r []bool) float64 {
	var ssDF float64 //  sum of squares of dF
	var ssF float64  //  sum of squares of F
	var errF float64
	var i, j int

	// compute equilibrium error at free coord's (q)
	for i = 0; i < DoF; i++ {
		errF = 0.0
		if q[i] {
			errF = F[i]
			for j = 0; j < DoF; j++ {
				if q[j] { // K_qq in upper triangle only
					if i <= j {
						errF -= K[i][j] * D[j]
					} else {
						errF -= K[j][i] * D[j]
					}
				}
			}
			for j = 0; j < DoF; j++ {
				if r[j] {
					errF -= K[i][j] * D[j]
				}
			}
		}
		dF[i] = errF
	}

	for i = 0; i < DoF; i++ {
		if q[i] {
			ssDF += (dF[i] * dF[i])
		}
	}
	for i = 0; i < DoF; i++ {
		if q[i] {
			ssF += (F[i] * F[i])
		}
	}
	return math.Sqrt(ssDF) / math.Sqrt(ssF) // convergence criterion
}

// ElementEndForces evaluates the end forces for all elements
// * 23feb94
// double **Q,	/**< frame element end forces			*/
// int nE,		/**< number of frame elements			*/
// vec3 *xyz,	/** XYZ locations of each node			*/
// double *L, double *Le,	/**< length of each frame element, effective */
// int *N1, int *N2,	/**< node connectivity			*/
// float *Ax, float *Asy, float *Asz,	/**< section areas	*/
// float *Jx, float *Iy, float *Iz,	/**< section area inertias */
// float *E, float *G,	/**< elastic and shear moduli		*/
// float *p,		/**< roll angle, radians		*/
// double **eqFTemp, /**< equivalent temp loads on elements, global */
// double **eqFMech, /**< equivalent mech loads on elements, global */
// double *D,	/**< displacement vector			*/
// int shear,	/**< 1: include shear deformation, 0: don't	*/
// int geom,	/**< 1: include goemetric stiffness, 0: don't	*/
// *bool axialStrainWarning /** false: strains < 0.001         */
func ElementEndForces(Q [][]float64, nE int, xyz []femath.Vec3, L, Le []float64, N1, N2 []int,
	Ax, Asy, Asz, Jx, Iy, Iz, E, G, p []float64, eqFTemp, eqFMech [][]float64, D []float64,
	shear, geom bool, axialStrainWarning *bool) (finalErr error) {
	s := make([]float64, 12)
	var axialStrain float64
	var m, j int

	for m = 0; m < nE; m++ {

		frameElementForce(s, xyz, L[m], Le[m], N1[m], N2[m], Ax[m], Asy[m], Asz[m], Jx[m], Iy[m], Iz[m], E[m], G[m], p[m], eqFTemp[m], eqFMech[m], D, shear, geom, &axialStrain)

		for j = 0; j < 12; j++ {
			Q[m][j] = s[j]
		}

		if math.Abs(axialStrain) > 0.001 {
			finalErr = errcat.AppendWarnStr(finalErr, fmt.Sprintf("elementEndForces, frame element %2d has an average axial strain of %8.6f!",
				m, axialStrain))
			*axialStrainWarning = true
		}
	}
	return finalErr
}

// frameElementForce, evaluate the end forces in local coord's
// 12nov02
func frameElementForce(s []float64, xyz []femath.Vec3, L, Le float64, n1, n2 int, Ax, Asy, Asz, J, Iy, Iz, E, G, p float64, fT, fM, D []float64, shear, geom bool, axialStrain *float64) {

	var t1, t2, t3, t4, t5, t6, t7, t8, t9 float64 /* coord Xformn	*/
	var d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12 float64
	//var x1, y1, z1, x2, y2, z2 float64 /* node coordinates	*/
	//var Ls float64                     /* stretched length of element */
	var delta float64              /* stretch in the frame element */
	var Ksy, Ksz, Dsy, Dsz float64 /* shear deformation coeff's	*/
	var T float64                  /* axial force for geometric stiffness */
	var f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12 float64

	transform.CoordTrans(xyz, L, n1, n2, &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8, &t9, p)

	n1 = 6 * n1
	n2 = 6 * n2

	d1 = D[n1+0]
	d2 = D[n1+1]
	d3 = D[n1+2]
	d4 = D[n1+3]
	d5 = D[n1+4]
	d6 = D[n1+5]
	d7 = D[n2+0]
	d8 = D[n2+1]
	d9 = D[n2+2]
	d10 = D[n2+3]
	d11 = D[n2+4]
	d12 = D[n2+5]

	if shear {
		Ksy = 12. * E * Iz / (G * Asy * Le * Le)
		Ksz = 12. * E * Iy / (G * Asz * Le * Le)
		Dsy = (1 + Ksy) * (1 + Ksy)
		Dsz = (1 + Ksz) * (1 + Ksz)
	} else {
		Ksz = 0.0
		Ksy = Ksz
		Dsz = 1.0
		Dsy = Dsz
	}

	/* finite strain ... (not consistent with 2nd order formulation)
	 *	delta += ( pow(((d7-d1)*t4 + (d8-d2)*t5 + (d9-d3)*t6),2.0) +
	 *		   pow(((d7-d1)*t7 + (d8-d2)*t8 + (d9-d3)*t9),2.0) )/(2.0*L);
	 */

	/* true strain ... (not appropriate for structural materials)
	 *	x1 = xyz[n1].x;	y1 = xyz[n1].y;	z1 = xyz[n1].z;
	 *	x2 = xyz[n2].x;	y2 = xyz[n2].y;	z2 = xyz[n2].z;
	 *
	 *  	Ls =	pow((x2+d7-x1-d1),2.0) +
	 *		pow((y2+d8-y1-d2),2.0) +
	 *		pow((z2+d9-z1-d3),2.0);
	 *	Ls = sqrt(Ls) + Le - L;
	 *
	 *	delta = Le*log(Ls/Le);
	 */

	/* axial element displacement ... */
	delta = (d7-d1)*t1 + (d8-d2)*t2 + (d9-d3)*t3
	*axialStrain = delta / Le // log(Ls/Le);

	s[0] = -(Ax * E / Le) * ((d7-d1)*t1 + (d8-d2)*t2 + (d9-d3)*t3)

	if geom {
		T = -s[0]
	}

	s[1] = -(12.*E*Iz/(Le*Le*Le*(1.+Ksy))+T/L*(1.2+2.0*Ksy+Ksy*Ksy)/Dsy)*
		((d7-d1)*t4+(d8-d2)*t5+(d9-d3)*t6) +
		(6.*E*Iz/(Le*Le*(1.+Ksy))+T/10.0/Dsy)*
			((d4+d10)*t7+(d5+d11)*t8+(d6+d12)*t9)
	s[2] = -(12.*E*Iy/(Le*Le*Le*(1.+Ksz))+
		T/L*(1.2+2.0*Ksz+Ksz*Ksz)/Dsz)*
		((d7-d1)*t7+(d8-d2)*t8+(d9-d3)*t9) -
		(6.*E*Iy/(Le*Le*(1.+Ksz))+T/10.0/Dsz)*
			((d4+d10)*t4+(d5+d11)*t5+(d6+d12)*t6)
	s[3] = -(G * J / Le) * ((d10-d4)*t1 + (d11-d5)*t2 + (d12-d6)*t3)
	s[4] = (6.*E*Iy/(Le*Le*(1.+Ksz))+T/10.0/Dsz)*
		((d7-d1)*t7+(d8-d2)*t8+(d9-d3)*t9) +
		((4.+Ksz)*E*Iy/(Le*(1.+Ksz))+
			T*L*(2.0/15.0+Ksz/6.0+Ksz*Ksz/12.0)/Dsz)*
			(d4*t4+d5*t5+d6*t6) + ((2.-Ksz)*E*Iy/(Le*(1.+Ksz))-
		T*L*(1.0/30.0+Ksz/6.0+Ksz*Ksz/12.0)/Dsz)*
		(d10*t4+d11*t5+d12*t6)
	s[5] = -(6.*E*Iz/(Le*Le*(1.+Ksy))+T/10.0/Dsy)*
		((d7-d1)*t4+(d8-d2)*t5+(d9-d3)*t6) + ((4.+Ksy)*E*Iz/(Le*(1.+Ksy))+
		T*L*(2.0/15.0+Ksy/6.0+Ksy*Ksy/12.0)/Dsy)*
		(d4*t7+d5*t8+d6*t9) + ((2.-Ksy)*E*Iz/(Le*(1.+Ksy))-
		T*L*(1.0/30.0+Ksy/6.0+Ksy*Ksy/12.0)/Dsy)*
		(d10*t7+d11*t8+d12*t9)
	s[6] = -s[0]
	s[7] = -s[1]
	s[8] = -s[2]
	s[9] = -s[3]
	s[10] = (6.*E*Iy/(Le*Le*(1.+Ksz))+T/10.0/Dsz)*
		((d7-d1)*t7+(d8-d2)*t8+(d9-d3)*t9) + ((4.+Ksz)*E*Iy/(Le*(1.+Ksz))+
		T*L*(2.0/15.0+Ksz/6.0+Ksz*Ksz/12.0)/Dsz)*
		(d10*t4+d11*t5+d12*t6) + ((2.-Ksz)*E*Iy/(Le*(1.+Ksz))-
		T*L*(1.0/30.0+Ksz/6.0+Ksz*Ksz/12.0)/Dsz)*
		(d4*t4+d5*t5+d6*t6)
	s[11] = -(6.*E*Iz/(Le*Le*(1.+Ksy))+T/10.0/Dsy)*
		((d7-d1)*t4+(d8-d2)*t5+(d9-d3)*t6) + ((4.+Ksy)*E*Iz/(Le*(1.+Ksy))+
		T*L*(2.0/15.0+Ksy/6.0+Ksy*Ksy/12.0)/Dsy)*
		(d10*t7+d11*t8+d12*t9) + ((2.-Ksy)*E*Iz/(Le*(1.+Ksy))-
		T*L*(1.0/30.0+Ksy/6.0+Ksy*Ksy/12.0)/Dsy)*
		(d4*t7+d5*t8+d6*t9)

	// add fixed end forces to internal element forces
	// 18oct2012, 14may1204, 15may2014

	// add temperature fixed-end-forces to variables f1-f12
	// add mechanical load fixed-end-forces to variables f1-f12
	// f1 ...  f12 are in the global element coordinate system
	f1 = fT[0] + fM[0]
	f2 = fT[1] + fM[1]
	f3 = fT[2] + fM[2]
	f4 = fT[3] + fM[3]
	f5 = fT[4] + fM[4]
	f6 = fT[5] + fM[5]
	f7 = fT[6] + fM[6]
	f8 = fT[7] + fM[7]
	f9 = fT[8] + fM[8]
	f10 = fT[9] + fM[9]
	f11 = fT[10] + fM[10]
	f12 = fT[11] + fM[11]

	// transform f1 ... f12 to local element coordinate system and
	// add local fixed end forces (-equivalent loads) to internal loads
	// {Q} = [T]{f}

	s[0] -= (f1*t1 + f2*t2 + f3*t3)
	s[1] -= (f1*t4 + f2*t5 + f3*t6)
	s[2] -= (f1*t7 + f2*t8 + f3*t9)
	s[3] -= (f4*t1 + f5*t2 + f6*t3)
	s[4] -= (f4*t4 + f5*t5 + f6*t6)
	s[5] -= (f4*t7 + f5*t8 + f6*t9)
	s[6] -= (f7*t1 + f8*t2 + f9*t3)
	s[7] -= (f7*t4 + f8*t5 + f9*t6)
	s[8] -= (f7*t7 + f8*t8 + f9*t9)
	s[9] -= (f10*t1 + f11*t2 + f12*t3)
	s[10] -= (f10*t4 + f11*t5 + f12*t6)
	s[11] -= (f10*t7 + f11*t8 + f12*t9)
}

// ComputeReactionForces : R(r) = [K(r,q)]*{D(q)} + [K(r,r)]*{D(r)} - F(r)
// reaction forces satisfy equilibrium in the solved system
//  2012-10-12  , 2014-05-16
// 	R, computed reaction forces
// 	F, vector of equivalent external loads
// 	K, stiffness matrix for the solved system
// 	D, displacement vector for the solved system
// 	DoF, number of structural coordinates
// 	r, false: not a reaction; true: a reaction coordinate
func ComputeReactionForces(R, F []float64, K [][]float64, D []float64, DoF int, r []bool) {
	var i, j int
	for i = 0; i < DoF; i++ {
		R[i] = 0
		if r[i] { // coordinate "i" is a reaction coord.
			R[i] = -F[i] // negative of equiv loads at coord i
			// reactions are relaxed through system deformations
			for j = 0; j < DoF; j++ {
				R[i] += K[i][j] * D[j]
			}
		}
	}
}
