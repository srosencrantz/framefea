package doc

import (
	"fmt"
)

const version = "Tue Jun 25 15:34:31 EDT 2019"

// GetVersion returns a string indicating the compile version information
func GetVersion() string {
	return fmt.Sprintf("FrameFEA compiled %s", version)
}

// GetAuthor returns a string indicating the compile version information
func GetAuthor() string {
	return "Written by Stephen Rosencrantz (email: srosencrantz@gmail.com)"
}
