#!/bin/bash

stamp=`date`

sed -i "/const version/c\const version = \"${stamp}\"" doc/doc.go

sed -i "/Current Version:/c\Current Version: ${stamp}" README.md
